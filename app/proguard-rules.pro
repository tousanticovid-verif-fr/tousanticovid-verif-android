# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


#implementation 'androidx.legacy:legacy-support-v4:1.0.0'

#    implementation 'com.google.android.material:material:1.0.0'
#    implementation 'androidx.constraintlayout:constraintlayout:1.1.3'
#    testImplementation 'junit:junit:4.12'
#    androidTestImplementation 'androidx.test:runner:1.2.0'
#    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'
#
#    //Lifecycle
#    implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'
#    annotationProcessor 'androidx.lifecycle:lifecycle-compiler:2.2.0'
#

#    //Retrofit
#    implementation 'com.squareup.okhttp3:logging-interceptor:3.12.1'
#    testImplementation 'com.squareup.retrofit2:retrofit-mock:2.5.0'
#

##---------------Begin: proguard configuration for Retrofit  -----------
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

-keepclasseswithmembers interface * {
    @retrofit2.* <methods>;
}
##---------------End: proguard configuration for Retrofit  -------------



##---------------Begin: proguard configuration for OkHttp  -------------
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
##---------------End: proguard configuration for OkHttp  ---------------


##---------------Begin: proguard configuration for Gson  ---------------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature
# For using GSON @Expose annotation
-keepattributes *Annotation*
# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }
# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
##---------------End: proguard configuration for Gson  -----------------



##---------------Begin: proguard configuration for Kotlin  -------------
-dontwarn kotlin.reflect.jvm.internal.**
-keep class kotlin.reflect.jvm.internal.** { *; }
##---------------End: proguard configuration for Kotlin  ---------------




##---------------Begin: proguard configuration for EventBus  -----------
-keepattributes *Annotation*
-keepclassmembers class * {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
# And if you use AsyncExecutor:
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}
##---------------End: proguard configuration for EventBus  -------------




-keepclassmembers,allowobfuscation class * {
    @com.google.gson.annotations.SerializedName <fields>;
  }
-keep,allowobfuscation interface com.google.gson.annotations.SerializedName

#
#    //Google Play Service(Google CheckLocation and Activity Recognition)
#    implementation('com.google.android.gms:play-services-location:17.0.0') {
#        exclude group: 'com.android.support', module:'support-v4'
#    }
#
#    // Anko Commons
#    implementation "org.jetbrains.anko:anko-commons:0.10.5"
#    implementation "org.jetbrains.anko:anko-appcompat-v7-commons:0.10.5"
#
#    // Zxing android embedded
#    implementation 'com.journeyapps:zxing-android-embedded:3.6.0'

-keepclassmembers class * extends java.lang.Enum {
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-dontobfuscate
-keep public class dgca.verifier.app.decoder.model.** { *; }

# Bouncy Castle -- Keep ECDH
-keep class org.bouncycastle.jcajce.provider.asymmetric.EC$* { *; }
-keep class org.bouncycastle.jcajce.provider.asymmetric.ec.KeyPairGeneratorSpi$ECDH { *; }
-keep class org.bouncycastle.jcajce.provider.asymmetric.ec.KeyFactorySpi$ECDH { *; }
-keep class org.bouncycastle.jcajce.provider.asymmetric.ec.KeyFactorySpi$ECDSA { *; }

# json-schema-validator (/!\ KEEP IT /!\)
-keep class com.github.fge.jsonschema.** { *; }
-keep class org.mozilla.javascript.** { *; }
-keep class sun.** { *; }
-keep class javax.** { *; }

-keep class com.blongho.** {*;}
-keep interface com.blongho.**
# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keeppackagenames com.blongho.country_data
-keepclassmembers class com.blongho.country_data.* {
   public *;
}
-keep class com.blongho.country_data.R$*{
    *;
 }