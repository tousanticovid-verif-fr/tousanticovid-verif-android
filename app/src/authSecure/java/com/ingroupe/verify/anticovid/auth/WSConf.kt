package com.ingroupe.verify.anticovid.auth

import com.ingroupe.verify.anticovid.BuildConfig
import com.ingroupe.verify.anticovid.common.FileLogger
import com.ingroupe.verify.anticovid.service.api.configuration.ConfigurationService
import com.scandit.datacapture.core.internal.sdk.AppAndroidEnvironment.applicationContext
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory



object WSConf: IWSConf {
    const val TAG = "WSConf"

    override fun getConfigurationBaseUrl(): String {
        return "/api/client/configuration"
    }

    override fun getConfigurationService(): ConfigurationService {


        return Retrofit.Builder().baseUrl(BuildConfig.ENDPOINT_URL)
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create()).build().create(
                ConfigurationService::class.java
            )
    }

    @Suppress("ConstantConditionIf")
    override fun getOkHttpClient(): OkHttpClient {
        val builder: OkHttpClient.Builder = OkHttpClient.Builder()

        // cas de base : non authentifié sans proxy
        builder.addInterceptor(AcceptLanguageHeaderInterceptor())
            .addInterceptor(AddAuthorizationInterceptor())
            .addInterceptor(AddVersionInterceptor())

        if(BuildConfig.LOG_ENABLED) {
            val logging = HttpLoggingInterceptor {
                FileLogger.write(it, applicationContext)
            }
            logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
            builder.addInterceptor(logging)
        }

        return builder.build()

    }
}
