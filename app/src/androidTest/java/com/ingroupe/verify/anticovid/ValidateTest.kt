package com.ingroupe.verify.anticovid

import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.fasterxml.jackson.databind.ObjectMapper
import dgca.verifier.app.decoder.getJsonDataFromAsset
import dgca.verifier.app.engine.CertLogicEngine
import dgca.verifier.app.engine.DefaultAffectedFieldsDataRetriever
import dgca.verifier.app.engine.DefaultCertLogicEngine
import dgca.verifier.app.engine.DefaultJsonLogicValidator
import dgca.verifier.app.engine.Result
import dgca.verifier.app.engine.data.CertificateType
import dgca.verifier.app.engine.data.ExternalParameter
import dgca.verifier.app.engine.data.source.remote.rules.RuleRemote
import dgca.verifier.app.engine.data.source.remote.rules.toRule
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.BufferedReader
import java.io.InputStream
import java.time.ZonedDateTime

@RunWith(AndroidJUnit4::class)
internal class ValidateTest {

    private lateinit var engine: CertLogicEngine
    private val objectMapper = ObjectMapper().apply { this.findAndRegisterModules() }

    companion object {
        const val SCHEMA_JSON_FILE_NAME = "schema.json"
        const val RULE_JSON_FILE_NAME = "rule.json"

        const val HCERT_JSON = "{\"nam\":{\"gn\":\"John\",\"gnt\":\"JOHN\",\"fnt\":\"DOE\",\"fn\":\"DOE\"},\"t\":[{\"tr\":\"260415000\",\"tg\":\"840539006\",\"sc\":\"2021-07-27T08:10:00Z\",\"is\":\"CNAM\",\"tc\":\"test\",\"co\":\"FR\",\"ci\":\"URN:UVCI:V1:FR:71O714J2U5AKX30BOPF1BUN5TB\",\"ma\":\"autotest\",\"tt\":\"970970\"}],\"ver\":\"1.2.1\",\"dob\":\"1980-01-30\"}"
    }

    private fun fetchRule(fileName: String): RuleRemote {
        val ruleExampleIs: InputStream =
            javaClass.classLoader!!.getResourceAsStream(fileName)
        val ruleJson = ruleExampleIs.bufferedReader().use(BufferedReader::readText)
        return objectMapper.readValue(ruleJson, RuleRemote::class.java)
    }

    @Before
    fun create() {
        val context = InstrumentationRegistry.getTargetContext()

        val schemaIs: InputStream =
            javaClass.classLoader!!.getResourceAsStream(SCHEMA_JSON_FILE_NAME)
        val schemaJson = schemaIs.bufferedReader().use(BufferedReader::readText)

        val jsonSchema = getJsonDataFromAsset(context, schemaJson) ?: ""
        engine = DefaultCertLogicEngine(
            DefaultAffectedFieldsDataRetriever(ObjectMapper().readTree(jsonSchema), ObjectMapper()),
            DefaultJsonLogicValidator()
        )
    }

    @Test
    @Throws(Exception::class)
    fun testValidationWithDateKO() {
        val rule = fetchRule(RULE_JSON_FILE_NAME).toRule()

        val externalParameter = ExternalParameter(
            validationClock = ZonedDateTime.now(),
            valueSets = mapOf(),
            countryCode = "FR",
            exp = ZonedDateTime.now(),
            iat = ZonedDateTime.now(),
            issuerCountryCode = "FR",
            kid = "",
            region = "",
        )

        val actual = engine.validate(
            CertificateType.TEST,
            "1.2.1",
            listOf(rule),
            externalParameter,
            HCERT_JSON
        )

        Assert.assertTrue(actual.size == 1)
        Assert.assertEquals(Result.FAIL, actual[0].result)
    }

    @Test
    @Throws(Exception::class)
    fun testValidationWithDateOK() {
        val rule = fetchRule(RULE_JSON_FILE_NAME).toRule()

        val externalParameter = ExternalParameter(
            validationClock = ZonedDateTime.parse("2021-07-29T08:10:00Z"),
            valueSets = mapOf(),
            countryCode = "FR",
            exp = ZonedDateTime.now(),
            iat = ZonedDateTime.now(),
            issuerCountryCode = "FR",
            kid = "",
            region = "",
        )

        val actual = engine.validate(
            CertificateType.TEST,
            "1.2.1",
            listOf(rule),
            externalParameter,
            HCERT_JSON
        )

        Assert.assertTrue(actual.size == 1)
        Assert.assertEquals(Result.PASSED, actual[0].result)
    }
}