package com.ingroupe.verify.anticovid.service.document

import android.content.Context
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.model.StatsPeriod
import com.ingroupe.verify.anticovid.common.model.TravelConfiguration
import com.ingroupe.verify.anticovid.service.document.database.DocumentLoader
import com.ingroupe.verify.anticovid.service.document.datamatrix.DatamatrixLiteHealthService
import com.ingroupe.verify.anticovid.service.document.datamatrix.DatamatrixLiteVaccineService
import com.ingroupe.verify.anticovid.service.document.datamatrix.DatamatrixPremiumService
import com.ingroupe.verify.anticovid.service.document.dcc.DccLiteHealthService
import com.ingroupe.verify.anticovid.service.document.dcc.DccLiteVaccineService
import com.ingroupe.verify.anticovid.service.document.dcc.DccPremiumService
import com.ingroupe.verify.anticovid.service.document.enums.ErrorDocEnum
import com.ingroupe.verify.anticovid.service.document.model.*
import com.ingroupe.verify.anticovid.synchronization.elements.Analytics
import com.ingroupe.verify.anticovid.synchronization.elements.Blacklist
import org.bouncycastle.util.encoders.Hex
import java.security.MessageDigest
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


abstract class DocOfflineService {

    companion object {
        const val TAG = "DocumentService"

        fun getDocumentBy2DDoc(encoded2DDoc: String, context: Context, travelConfiguration: TravelConfiguration?): DocumentResult? {

            val dynamicDataList: MutableList<Map<String, String>> = ArrayList()
            val errors: ArrayList<DocumentErrorsResult> = arrayListOf()
            val mappedDynamicData: MutableMap<String, String> = LinkedHashMap()

            try {

                val barcode2DDoc = StaticDataService.getBarcode2DDoc(encoded2DDoc) ?: return null
                val modeService = when(JWTUtils.getDisplayMode()) {
                    Constants.DisplayMode.VACCINE -> DatamatrixLiteVaccineService
                    Constants.DisplayMode.HEALTH -> DatamatrixLiteHealthService
                    Constants.DisplayMode.PS -> DatamatrixPremiumService
                    Constants.DisplayMode.OT -> DatamatrixPremiumService
                }
                when(barcode2DDoc.message?.type) {
                    "B2" -> {
                        modeService.get2ddocTest(
                            barcode2DDoc,
                            mappedDynamicData,
                            context,
                            travelConfiguration?.timeUTC
                        )
                    }
                    "L1" -> {
                        modeService.get2ddocVaccin(
                            barcode2DDoc,
                            mappedDynamicData,
                            context,
                            travelConfiguration?.timeUTC
                        )
                    }
                }
                val configBarcode2DDoc = DocumentLoader.findConfigBarcodeByType(barcode2DDoc.message?.type)

                dynamicDataList.add(mappedDynamicData)

                val hash = getHash2ddoc(encoded2DDoc)
                val isHashBlacklisted = Blacklist.isBlacklisted(Blacklist.BlacklistType.BLACKLIST_2DDOC, hash)
                val isHashDuplicate = Blacklist.isHashDuplicate(hash)
                if(isHashBlacklisted) {
                    if(JWTUtils.isDisplayPremium()) {
                        mappedDynamicData["blacklist"] = Blacklist.Message.BLACKLIST_OT.name
                    } else {
                        mappedDynamicData["blacklist"] = Blacklist.Message.BLACKLIST_LITE.name
                    }
                    mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = Constants.GlobalValidity.KO.text
                    mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = context.getString(R.string.result_fraudulent)
                } else if(isHashDuplicate) { // on n'affiche pas l'informaton doublon dans le cas d'un blacklistage
                    mappedDynamicData["duplicate"] = Blacklist.Message.DUPLICATE.name
                }

                writeStat(
                    mappedDynamicData,
                    configBarcode2DDoc?.resourceType?:"",
                    "",
                    isHashBlacklisted,
                    isHashDuplicate
                )

                return DocumentResult(
                    DocumentDataResult(
                        barcode2DDoc,
                        dynamicDataList as ArrayList<Map<String, String>>?
                    ),
                    errors,
                    configBarcode2DDoc?.resourceType,
                    null
                )
            } catch (e: Exception) {
                writeStat(
                    mappedDynamicData,
                    "",
                    ErrorDocEnum.ERR04.message,
                    isHashBlacklisted = false,
                    isHashDuplicate = false
                )

                return null
            }
        }

        @ExperimentalUnsignedTypes
        fun getDocumentByDcc(encodedDcc: String, context: Context, dccFormat: Constants.DccFormat, travelConfiguration: TravelConfiguration?): DocumentResult? {

            val dynamicDataList: MutableList<Map<String, String>> = ArrayList()
            val mappedDynamicData: MutableMap<String, String> = LinkedHashMap()
            val errors: ArrayList<DocumentErrorsResult> = arrayListOf()

            try {

                val staticDcc = StaticDataService.getDcc(encodedDcc, context, mappedDynamicData, dccFormat, travelConfiguration?.timeUTC)
                val infoTestForStat = InfoTestForStat()

                var ci = ""
                var co = ""

                val dccType = DocumentStaticDccResult.getResourceType(staticDcc.dccData!!.greenCertificate)
                val modeService = when(JWTUtils.getDisplayMode()) {
                    Constants.DisplayMode.VACCINE -> DccLiteVaccineService
                    Constants.DisplayMode.HEALTH -> DccLiteHealthService
                    Constants.DisplayMode.PS -> DccPremiumService
                    Constants.DisplayMode.OT -> DccPremiumService
                }
                when (dccType) {

                    DocumentStaticDccResult.DccType.DCC_VACCINATION -> {
                        ci = staticDcc.dccData!!.greenCertificate.vaccinations!![0].certificateIdentifier
                        co = staticDcc.dccData!!.greenCertificate.vaccinations!![0].countryOfVaccination

                        modeService.getDccVaccination(
                            staticDcc,
                            mappedDynamicData,
                            context,
                            travelConfiguration?.timeUTC
                        )
                    }
                    DocumentStaticDccResult.DccType.DCC_TEST -> {
                        ci = staticDcc.dccData!!.greenCertificate.tests!![0].certificateIdentifier
                        co = staticDcc.dccData!!.greenCertificate.tests!![0].countryOfVaccination

                        modeService.getDccTest(
                            staticDcc,
                            mappedDynamicData,
                            context,
                            travelConfiguration?.timeUTC,
                            infoTestForStat
                        )
                    }
                    DocumentStaticDccResult.DccType.DCC_RECOVERY -> {
                        ci = staticDcc.dccData!!.greenCertificate.recoveryStatements!![0].certificateIdentifier
                        co = staticDcc.dccData!!.greenCertificate.recoveryStatements!![0].countryOfVaccination

                        modeService.getDccRecovery(
                            staticDcc,
                            mappedDynamicData,
                            context,
                            travelConfiguration?.timeUTC,
                            infoTestForStat
                        )
                    }
                    DocumentStaticDccResult.DccType.DCC_EXEMPTION -> {
                        ci = staticDcc.dccData!!.greenCertificate.exemption!!.certificateIdentifier
                        co = staticDcc.dccData!!.greenCertificate.exemption!!.countryOfVaccination

                        modeService.getDccExemption(
                            staticDcc,
                            mappedDynamicData,
                            context,
                            travelConfiguration?.timeUTC
                        )
                    }
                    DocumentStaticDccResult.DccType.DCC_ACTIVITY -> {
                        modeService.getDccActivity(
                            staticDcc,
                            mappedDynamicData,
                            context,
                            travelConfiguration?.timeUTC
                        )
                    }
                    null -> {
                        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = Constants.GlobalValidity.KO.text
                        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = context.getString(R.string.result_invalid)
                    }
                }
                dynamicDataList.add(mappedDynamicData)


                val hash = getHashDcc(ci, co)
                val isHashBlacklisted = Blacklist.isBlacklisted(Blacklist.BlacklistType.BLACKLIST_DCC, hash)
                val isHashDuplicate = Blacklist.isHashDuplicate(hash)
                if(isHashBlacklisted) {
                    if(JWTUtils.isDisplayPremium()) {
                        mappedDynamicData["blacklist"] = Blacklist.Message.BLACKLIST_OT.name
                    } else {
                        mappedDynamicData["blacklist"] = Blacklist.Message.BLACKLIST_LITE.name
                    }
                    mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = Constants.GlobalValidity.KO.text
                    mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = context.getString(R.string.result_fraudulent)
                } else if(isHashDuplicate) { // on n'affiche pas l'informaton doublon dans le cas d'un blacklistage
                    mappedDynamicData["duplicate"] = Blacklist.Message.DUPLICATE.name
                }

                val engineDataResult = DocumentEngineDataResult(mutableListOf(), null)

                if(JWTUtils.getDisplayMode() == Constants.DisplayMode.OT
                    && travelConfiguration != null
                    && !isHashBlacklisted) {

                    staticDcc.dccData?.let { gcd ->

                        val rulesForVerification = EngineService().prepareRules(
                            gcd,
                            travelConfiguration
                        )

                        // On trie par nom pour respecter l'ordre des favoris
                        rulesForVerification.listRulesFavorite.sortBy { it.country.name }

                        EngineService().executeRules(
                            engineDataResult.engineResults,
                            rulesForVerification,
                            staticDcc,
                            dccFormat,
                            travelConfiguration.timeUTC!!,
                            mappedDynamicData[Constants.VALIDITY_STATUS_FIELD],
                            context
                        )

                        if(rulesForVerification.showAlert && mappedDynamicData["anomaly"] == null) {
                            engineDataResult.alertMessage = context.getString(R.string.result_alert_not_green_country)
                        }
                    }
                }

                writeStat(
                    mappedDynamicData,
                    dccType?.text?:"",
                    "",
                    isHashBlacklisted,
                    isHashDuplicate,
                    infoTestForStat
                )

                return DocumentResult(
                    DocumentDataResult(
                        staticDcc,
                        dynamicDataList as ArrayList<Map<String, String>>?
                    ),
                    errors,
                    dccType?.text,
                    engineDataResult
                )
            } catch (e: Exception) {
                writeStat(
                    mappedDynamicData,
                    "",
                    ErrorDocEnum.ERR03.message,
                    isHashBlacklisted = false,
                    isHashDuplicate = false
                )

                return null
            }
        }

        private fun writeStat(
            mappedDynamicData: MutableMap<String, String>,
            resourceType: String,
            msgException: String,
            isHashBlacklisted: Boolean,
            isHashDuplicate: Boolean,
            infoTestForStat: InfoTestForStat? = null
        ) {
            val stat = StringBuffer()
            val separator = "|"

            val statsPeriod = StatsPeriod.getStatsPeriod()
            val statsDay = statsPeriod.getStatsDay(LocalDate.now())

            val valGlobal = when (mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD]) {
                Constants.GlobalValidity.KO.text -> "false"
                Constants.GlobalValidity.OK.text -> "true"
                Constants.GlobalValidity.INFO.text -> "null"
                else -> "false"
            }

            if(isHashDuplicate)  {
                statsDay.nbDuplicate++
            } else {
                when (mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD]) {
                    Constants.GlobalValidity.KO.text -> statsDay.nbR++
                    Constants.GlobalValidity.OK.text -> statsDay.nbG++
                    Constants.GlobalValidity.INFO.text -> statsDay.nbB++
                    else -> statsDay.nbR++
                }
                statsDay.nbTotal++
            }

            // Pour être sûr, on efface les logs anciennes
            statsPeriod.cleanOldStatsAndSave()

            stat.append(LocalDate.now().atTime(0, 0, 0).format(DateTimeFormatter.ISO_DATE_TIME))
                .append(separator)
                .append(valGlobal)
                .append(separator)

            if(isHashBlacklisted) {
                stat.append("Blacklist")
            }
            if(isHashDuplicate) {
                stat.append("Doublon")
            }
            stat.append(separator)
                .append(separator)
                .append(JWTUtils.getDisplayMode().controlType)
                .append(separator)
                .append(JWTUtils.getSiren())
                .append(separator)
                .append(separator)
                .append(separator)
                .append(separator)
                .append(resourceType)
                .append(separator)
                .append(msgException)
                .append(separator)


            infoTestForStat?.isTestPositive?.let {
                if(it) {
                    stat.append("TEST POSITIF")
                } else {
                    stat.append("TEST NEGATIF")
                }
            }
            stat.append(separator)
            infoTestForStat?.info?.let {
                stat.append(it)
            }

            Analytics.reportVerifEvent(stat.toString())
        }

        private fun toSha256(input: String): String {
            val hash: ByteArray = MessageDigest.getInstance("SHA-256").digest(
                input.toByteArray()
            )
            return String(Hex.encode(hash))
        }

        private fun getHashDcc(ci: String, co: String): String {
            val toHash = co.trim().uppercase()+ci.trim()
            return toSha256(toHash)
        }

        private fun getHash2ddoc(encoded2DDoc: String): String {
            val split = encoded2DDoc.split("\u001F")
            val message = split.getOrNull(0) ?: return ""
            return toSha256(message)
        }


    }
}