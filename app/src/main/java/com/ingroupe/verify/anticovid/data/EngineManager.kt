package com.ingroupe.verify.anticovid.data

import androidx.room.Room
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.data.local.CertificatesRepository
import com.ingroupe.verify.anticovid.data.local.BlacklistRepository
import com.ingroupe.verify.anticovid.data.local.TacvCountriesRepository
import com.ingroupe.verify.anticovid.data.local.TacvRulesRepository
import com.ingroupe.verify.anticovid.data.local.TacvValueSetsRepository
import com.ingroupe.verify.anticovid.data.local.certificates.CertificatesDataSource
import com.ingroupe.verify.anticovid.data.local.blacklist.BlacklistDataSource
import com.ingroupe.verify.anticovid.data.local.countries.DefaultCountriesLocalDataSource
import com.ingroupe.verify.anticovid.data.local.EngineDatabase
import com.ingroupe.verify.anticovid.data.local.rules.TacvRulesLocalDataSource
import com.ingroupe.verify.anticovid.data.local.valuesets.DefaultValueSetsLocalDataSource
import com.ingroupe.verify.anticovid.data.rules.TacvGetRulesUseCase


object EngineManager {
    var engineDatabase : EngineDatabase =
        Room.databaseBuilder(App.getContext(), EngineDatabase::class.java, "engine-db")
            .fallbackToDestructiveMigration().build()

    val rulesDao = engineDatabase.rulesDao()

    val rulesLocalDataSource = TacvRulesLocalDataSource(rulesDao)

    val rulesRepository = TacvRulesRepository(rulesLocalDataSource)

    val rulesUseCase = TacvGetRulesUseCase(rulesRepository)


    val countriesDao = engineDatabase.countriesDao()

    val countriesLocalDataSource = DefaultCountriesLocalDataSource(countriesDao)

    val countriesRepository = TacvCountriesRepository(countriesLocalDataSource)


    val valueSetsDao = engineDatabase.valueSetsDao()

    val valueSetsLocalDataSource = DefaultValueSetsLocalDataSource(valueSetsDao)

    val valueSetsRepository = TacvValueSetsRepository(valueSetsLocalDataSource)


    val certificatesDao = engineDatabase.certificatesDao()

    val certificatesLocalDataSource = CertificatesDataSource(certificatesDao)

    val certificatesRepository = CertificatesRepository(certificatesLocalDataSource)


    val blacklistDao = engineDatabase.blacklistDao()

    val blacklistDataSource = BlacklistDataSource(blacklistDao)

    val blacklistRepository = BlacklistRepository(blacklistDataSource)


}