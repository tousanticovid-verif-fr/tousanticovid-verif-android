package com.ingroupe.verify.anticovid.dialog

import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import androidx.fragment.app.DialogFragment
import com.ingroupe.verify.anticovid.R

class LoadingAnimDialog : DialogFragment() {
    companion object {
        const val TAG = "LoadingAnimDialog"

        private const val WIDTH_PROPORTION = 1.0
        private const val HEIGHT_PROPORTION = 1.0

        private var inst: LoadingAnimDialog? = null

        fun getInstance(): LoadingAnimDialog? {
            if (inst == null) {
                inst = LoadingAnimDialog()
            }
            return inst
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "on create view")
        return inflater.inflate(R.layout.dialog_loading_anim, container, false)
    }

    override fun onResume() {
        Log.d(TAG, "on resume $this")
        super.onResume()

        val window = dialog?.window
        val wm = window?.windowManager

        isCancelable = false
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))


        wm?.let {
            val width: Int
            val height: Int

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                val windowMetrics = wm.currentWindowMetrics
                val windowInsets: WindowInsets = windowMetrics.windowInsets

                val insets = windowInsets.getInsetsIgnoringVisibility(
                    WindowInsets.Type.navigationBars() or WindowInsets.Type.displayCutout())
                val insetsWidth = insets.right + insets.left
                val insetsHeight = insets.top + insets.bottom

                val b = windowMetrics.bounds
                width = b.width() - insetsWidth
                height = b.height() - insetsHeight
            } else {

                val size = Point()
                @Suppress("DEPRECATION")
                val display = wm.defaultDisplay // deprecated in API 30
                @Suppress("DEPRECATION")
                display?.getSize(size) // deprecated in API 30

                width = size.x
                height = size.y
            }
            window.setLayout((width * WIDTH_PROPORTION).toInt(), (height * HEIGHT_PROPORTION).toInt())
        }
    }
}