package com.ingroupe.verify.anticovid.data.rules

import com.ingroupe.verify.anticovid.data.local.TacvRulesRepository
import dgca.verifier.app.engine.data.CertificateType
import dgca.verifier.app.engine.data.Rule
import dgca.verifier.app.engine.data.Type
import dgca.verifier.app.engine.domain.rules.GetRulesUseCase
import java.time.ZonedDateTime

class TacvGetRulesUseCase(private val rulesRepository: TacvRulesRepository): GetRulesUseCase {

    fun invoke(
        validationClock: ZonedDateTime,
        departureCountryCode: String,
        arrivalCountryCode: String,
        issuanceCountryIsoCode: String,
        certificateType: CertificateType,
        region: String? = null
    ): List<Rule> {
        val filteredAcceptanceRules = mutableMapOf<String, Rule>()
        val selectedRegion: String = region?.trim() ?: ""
        val acceptanceRules = rulesRepository.getRulesBy(
            departureCountryCode,
            arrivalCountryCode,
            validationClock,
            Type.ACCEPTANCE,
            certificateType.toRuleCertificateType()
        )
        for (i in acceptanceRules.indices) {
            val rule = acceptanceRules[i]
            val ruleRegion: String = rule.region?.trim() ?: ""
            if (selectedRegion.equals(
                    ruleRegion,
                    ignoreCase = true
                ) && (filteredAcceptanceRules[rule.identifier]?.version?.toVersion() ?: -1 < rule.version.toVersion() ?: 0)
            ) {
                filteredAcceptanceRules[rule.identifier] = rule
            }
        }

        val filteredInvalidationRules = mutableMapOf<String, Rule>()
        if (issuanceCountryIsoCode.isNotBlank()) {
            val invalidationRules = rulesRepository.getRulesBy(
                issuanceCountryIsoCode,
                validationClock,
                Type.INVALIDATION,
                certificateType.toRuleCertificateType()
            )
            for (i in invalidationRules.indices) {
                val rule = invalidationRules[i]
                if (filteredInvalidationRules[rule.identifier]?.version?.toVersion() ?: -1 < rule.version.toVersion() ?: 0) {
                    filteredInvalidationRules[rule.identifier] = rule
                }
            }
        }
        return filteredAcceptanceRules.values + filteredInvalidationRules.values
    }

    override fun invoke(
        validationClock: ZonedDateTime,
        acceptanceCountryIsoCode: String,
        issuanceCountryIsoCode: String,
        certificateType: CertificateType,
        region: String?
    ): List<Rule> {
        val filteredAcceptanceRules = mutableMapOf<String, Rule>()
        val selectedRegion: String = region?.trim() ?: ""
        val acceptanceRules = rulesRepository.getRulesBy(
            acceptanceCountryIsoCode,
            validationClock,
            Type.ACCEPTANCE,
            certificateType.toRuleCertificateType()
        )
        for (i in acceptanceRules.indices) {
            val rule = acceptanceRules[i]
            val ruleRegion: String = rule.region?.trim() ?: ""
            if (selectedRegion.equals(
                    ruleRegion,
                    ignoreCase = true
                ) && (filteredAcceptanceRules[rule.identifier]?.version?.toVersion() ?: -1 < rule.version.toVersion() ?: 0)
            ) {
                filteredAcceptanceRules[rule.identifier] = rule
            }
        }

        val filteredInvalidationRules = mutableMapOf<String, Rule>()
        if (issuanceCountryIsoCode.isNotBlank()) {
            val invalidationRules = rulesRepository.getRulesBy(
                issuanceCountryIsoCode,
                validationClock,
                Type.INVALIDATION,
                certificateType.toRuleCertificateType()
            )
            for (i in invalidationRules.indices) {
                val rule = invalidationRules[i]
                if (filteredInvalidationRules[rule.identifier]?.version?.toVersion() ?: -1 < rule.version.toVersion() ?: 0) {
                    filteredInvalidationRules[rule.identifier] = rule
                }
            }
        }
        return filteredAcceptanceRules.values + filteredInvalidationRules.values
    }

    private fun String.toVersion(): Int? = try {
        val versionParts = this.split('.')
        var version = 0
        var multiplier = 1
        versionParts.reversed().forEach {
            version += multiplier * it.toInt()
            multiplier *= 100
        }
        version
    } catch (error: Throwable) {
        null
    }
}