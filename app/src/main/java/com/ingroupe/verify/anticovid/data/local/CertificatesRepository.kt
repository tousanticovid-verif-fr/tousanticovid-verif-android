
package com.ingroupe.verify.anticovid.data.local

import android.content.Context
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.data.local.certificates.CertificateDcc
import com.ingroupe.verify.anticovid.data.local.certificates.CertificatesDataSource
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncResult
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils
import java.text.SimpleDateFormat
import java.util.*

class CertificatesRepository(private val certificatesDataSource: CertificatesDataSource) {

    companion object {
        const val TAG = "CertificatesRepository"
    }

    fun saveCertificates(certificatesMap: Map<String, String>) {
        certificatesDataSource.cleanAndInsertAll(certificatesMap)
    }

    fun getCertificateWithKey(key: String) : CertificateDcc? {
        return certificatesDataSource.getCertificateWithKey(key)
    }

    fun initDatas() : Boolean {
        val syncResult : SyncResult = Utils.loadFromAsset("sync/sync_certs.json")
            ?: return false

        syncResult.certificatesDCC?.let { listCertifs ->
            saveCertificates(listCertifs)
        }

        var lastSyncDate : Date? = null
        syncResult.currentDate?.let { strDate ->
            lastSyncDate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(strDate)
            val syncPrefs = App.getContext().getSharedPreferences(Constants.SYNC_KEY, Context.MODE_PRIVATE)
            with(syncPrefs.edit()) {
                putLong(Constants.KEY_DATE_UPDATE_CONF, lastSyncDate?.time?:0)
                apply()
            }
        }

        SynchronisationUtils.saveSpecificValues(syncResult)
        SynchronisationUtils.resetPrefs(syncResult, lastSyncDate?.time ?: 0)

        return true
    }
}
