package com.ingroupe.verify.anticovid

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ingroupe.verify.anticovid.common.FeatureFragment
import com.ingroupe.verify.anticovid.ui.actionchoice.ActionChoiceChildFragment
import com.ingroupe.verify.anticovid.ui.init.InitChildFragment
import com.ingroupe.verify.anticovid.ui.result.ResultScanChildFragment
import com.ingroupe.verify.anticovid.ui.scan.ScanChildFragment
import com.ingroupe.verify.anticovid.ui.statistic.StatisticChildFragment
import com.ingroupe.verify.anticovid.ui.tutorialresult.lite.TutorialResultLiteChildFragment
import com.ingroupe.verify.anticovid.ui.tutorialresult.lite.TutorialResultLiteHelpChildFragment
import com.ingroupe.verify.anticovid.ui.tutorialresult.premium.TutorialResultPremiumChildFragment
import com.ingroupe.verify.anticovid.ui.tutorialresult.premium.TutorialResultPremiumHelpChildFragment
import java.io.Serializable

class NavigationFragment : FeatureFragment() {

    companion object {
        const val TAG = "NavigationFragment"
        fun newInstance() = NavigationFragment()
    }

    override fun createFragment(tag: String, args: Array<out Serializable>): Fragment {
        return when (tag) {
            InitChildFragment.TAG -> InitChildFragment.newInstance()
            ScanChildFragment.TAG -> ScanChildFragment.newInstance()
            ResultScanChildFragment.TAG -> ResultScanChildFragment.newInstance()
            ActionChoiceChildFragment.TAG -> ActionChoiceChildFragment.newInstance()
            TutorialResultLiteChildFragment.TAG -> TutorialResultLiteChildFragment.newInstance()
            SettingsChildFragment.TAG -> SettingsChildFragment.newInstance()
            InformationChildFragment.TAG -> InformationChildFragment.newInstance()
            TutorialResultLiteHelpChildFragment.TAG -> TutorialResultLiteHelpChildFragment()
            TutorialResultPremiumChildFragment.TAG -> TutorialResultPremiumChildFragment()
            TutorialResultPremiumHelpChildFragment.TAG -> TutorialResultPremiumHelpChildFragment()
            StatisticChildFragment.TAG -> StatisticChildFragment()
            else -> {
                Log.w(TAG, "TAG not found")
                InitChildFragment.newInstance()
            }
        }
    }

    override fun getLayoutId(): Int {
        return R.id.container_main
    }

    override fun getFirstTag(): String {
        return InitChildFragment.TAG
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.navigation, container, false)
    }
}