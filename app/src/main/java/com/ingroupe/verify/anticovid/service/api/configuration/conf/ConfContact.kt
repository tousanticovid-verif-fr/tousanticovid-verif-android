package com.ingroupe.verify.anticovid.service.api.configuration.conf

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConfContact(
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("tel")
    var tel: String? = null,
    @SerializedName("mail")
    var mail: String? = null,
    @SerializedName("details")
    var details: String? = null
) : Serializable