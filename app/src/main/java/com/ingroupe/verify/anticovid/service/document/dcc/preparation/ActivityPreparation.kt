package com.ingroupe.verify.anticovid.service.document.dcc.preparation

import dgca.verifier.app.decoder.model.ActivityDCC
import java.time.ZonedDateTime

class ActivityPreparation(
    val expirationTime: ZonedDateTime,
    val issuedAt: ZonedDateTime
) {
    companion object {

        fun getPreparation(
            activity: ActivityDCC
        ) : ActivityPreparation? {
            val expirationTime = activity.expirationTime
            val issuedAt = activity.issuedAt

            return if(expirationTime == null || issuedAt == null) {
                null
            } else {
                ActivityPreparation(expirationTime, issuedAt)
            }
        }
    }
}
