/*-
 * ---license-start
 * eu-digital-green-certificates / dgc-certlogic-android
 * ---
 * Copyright (C) 2021 T-Systems International GmbH and all other contributors
 * ---
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ---license-end
 *
 * Created by osarapulov on 25.06.21 15:49
 */

package com.ingroupe.verify.anticovid.data.local

import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.service.api.configuration.engine.toValueSetIdentifiers
import com.ingroupe.verify.anticovid.service.api.configuration.engine.toValueSets
import com.ingroupe.verify.anticovid.service.api.configuration.engine.valuesets.ValueSetIdentifierResult
import com.ingroupe.verify.anticovid.service.api.configuration.engine.valuesets.ValueSetResult
import com.ingroupe.verify.anticovid.synchronization.ConfServiceUtils
import dgca.verifier.app.engine.data.ValueSet
import dgca.verifier.app.engine.data.ValueSetIdentifier
import dgca.verifier.app.engine.data.source.local.valuesets.ValueSetsLocalDataSource
import dgca.verifier.app.engine.data.source.valuesets.ValueSetsRepository

class TacvValueSetsRepository(
    private val localDataSource: ValueSetsLocalDataSource
) : ValueSetsRepository {

    companion object {
        const val TAG = "TacvValueSetsRepository"
    }

    override suspend fun preLoad(url: String) {
        val valueSetsIdentifiersRemote = ConfServiceUtils.getValueSetIdentifiers().toValueSetIdentifiers()

        if(valueSetsIdentifiersRemote.isNotEmpty()) {
            val valueSetsIdentifiersLocal = localDataSource.getValueSetIdentifiers()

            val added = valueSetsIdentifiersRemote - valueSetsIdentifiersLocal
            val removed = valueSetsIdentifiersLocal - valueSetsIdentifiersRemote

            localDataSource.removeValueSetsBy(removed.map { it.id })

            val valueSets = mutableListOf<ValueSet>()
            val valueSetToAddHashes = mutableListOf<String>()
            added.forEach {
                valueSetToAddHashes.add(it.hash)
            }

            if(valueSetToAddHashes.isNotEmpty()) {
                val listValueSetResult = ConfServiceUtils.getValueSet(valueSetToAddHashes)
                listValueSetResult.toValueSets().forEach { vs ->
                    valueSets.add(vs)
                }
            }

            if (valueSets.isNotEmpty()) {
                localDataSource.addValueSets(added, valueSets)
            }
        } else {
            throw Exception("ValueSets Identifiers is Empty")
        }
    }

    override suspend fun initDatas() : Boolean {
        val vSetResult : List<ValueSetResult>? = Utils.loadFromAsset("sync/sync_valueset.json")
        val vSetIdentifierResult : List<ValueSetIdentifierResult>? = Utils.loadFromAsset("sync/sync_valueset_id.json")

        if (vSetResult.isNullOrEmpty() || vSetIdentifierResult.isNullOrEmpty()) {
            return false
        }

        val vSets = vSetResult.toValueSets()
        val vSetIdentifiers = vSetIdentifierResult.toValueSetIdentifiers()

        if (vSets.isNullOrEmpty() || vSetIdentifiers.isNullOrEmpty()) {
            return false
        }

        localDataSource.removeAllValueSetsDatas()
        localDataSource.addValueSets(vSetIdentifiers, vSets)

        return true
    }


    override suspend fun getValueSets(): List<ValueSet> = localDataSource.getValueSets()

    override suspend fun getValueSetIdentifiers(): List<ValueSetIdentifier> = localDataSource.getValueSetIdentifiers()
}