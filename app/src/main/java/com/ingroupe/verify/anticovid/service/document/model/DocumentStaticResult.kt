package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

abstract class DocumentStaticResult(
    @SerializedName("signature")
    var signature: DocumentSignatureResult? = null

) : Serializable {

    var hasValidSignature = false
}