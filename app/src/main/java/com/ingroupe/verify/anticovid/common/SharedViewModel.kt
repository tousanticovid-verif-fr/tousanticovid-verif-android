package com.ingroupe.verify.anticovid.common

import androidx.lifecycle.ViewModel
import com.ingroupe.verify.anticovid.common.model.TravelConfiguration
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentResult
import java.util.*

class SharedViewModel : ViewModel() {
    var configuration: ConfResult? = null

    var currentResponse: DocumentResult? = null

    var currentResponseDate: Date? = null

    var resultAlreadyViewed: Boolean? = null

    var reloadConfiguration: Boolean = false

    var travelConfiguration : TravelConfiguration? = null
}