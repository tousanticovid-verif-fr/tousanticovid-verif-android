package com.ingroupe.verify.anticovid.observer

import androidx.lifecycle.*

class BackgroundObserver(private val listener: BackgroundObserverListener) : LifecycleEventObserver {

    private var wasInBackground = false

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        if (event == Lifecycle.Event.ON_PAUSE) {
            wasInBackground = true
        } else if (event == Lifecycle.Event.ON_RESUME) {
            if(wasInBackground) {
                wasInBackground = false
                listener.hasComeBackFromBackground()
            }
        }
    }
}