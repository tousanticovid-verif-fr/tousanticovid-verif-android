package com.ingroupe.verify.anticovid.ui.tutorialscan2ddoc

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.google.android.material.tabs.TabLayoutMediator
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.databinding.ActivityScreenSlideBinding


class TutorialScanActivity : AppCompatActivity() {

    companion object {
        /**
         * The number of pages to show in this tuto
         */
        private const val NUM_PAGES = 2
    }

    private lateinit var binding: ActivityScreenSlideBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityScreenSlideBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        initToolbar()
        initView()
    }

    private fun initToolbar() {
        title = getString(R.string.title_tutorial_scan)
        binding.toolbar.setNavigationOnClickListener(null)

        supportActionBar?.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.show()

        binding.toolbar.setNavigationOnClickListener { finish() }
    }

    private fun initView() {
        val pagerAdapter = ScreenSlidePagerAdapter(this)
        binding.contentTuto.pager.adapter = pagerAdapter
        TabLayoutMediator(binding.contentTuto.tabLayout, binding.contentTuto.pager) { _, _ -> }.attach()
        binding.contentTuto.pager.registerOnPageChangeCallback(pageChangeCallback)

        binding.contentTuto.buttonTutoNext.setOnClickListener {
            loadNextTutoPage()
        }

        binding.contentTuto.buttonTutoPrevious.setOnClickListener {
            loadPreviousTutoPage()
        }

        if (intent.hasExtra(Constants.CAN_SKIP_TUTO)) {
            binding.contentTuto.checkBoxShowTutoScan.visibility = View.VISIBLE
            binding.contentTuto.buttonTutoPrevious.visibility = View.GONE

            binding.contentTuto.checkBoxShowTutoScan.setOnCheckedChangeListener { _, _ ->
                saveTutoPreferences()
            }
        }
    }

    private fun saveTutoPreferences() {
        val sharedPref = getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)

        sharedPref?.let { sp ->
            with(sp.edit()) {
                putBoolean(
                    Constants.SavedItems.SHOW_SCAN_TUTO.text,
                    !binding.contentTuto.checkBoxShowTutoScan.isChecked
                )
                apply()
            }
        }
    }

    private fun loadPreviousTutoPage() {
        val current: Int = getItem(-1)
        if (current >= 0) {
            binding.contentTuto.pager.currentItem = current
        } else {
            finish()
        }
    }

    private fun loadNextTutoPage() {
        val current: Int = getItem(+1)
        if (current < NUM_PAGES) {
            binding.contentTuto.pager.currentItem = current
        } else {
            finish()
        }
    }

    private fun getItem(i: Int): Int {
        return binding.contentTuto.pager.currentItem + i
    }

    override fun onBackPressed() {
        if (binding.contentTuto.pager.currentItem == 0) {
            super.onBackPressed()
        } else {
            binding.contentTuto.pager.currentItem = binding.contentTuto.pager.currentItem - 1
        }
    }

    private var pageChangeCallback: OnPageChangeCallback = object : OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)

            when (position) {
                NUM_PAGES - 1 -> {
                    binding.contentTuto.buttonTutoNext.text = getString(R.string.action_close)
                    binding.contentTuto.buttonTutoPrevious.text = getString(R.string.action_previous)
                }
                0 -> {
                    binding.contentTuto.buttonTutoNext.text = getString(R.string.action_next)
                    binding.contentTuto.buttonTutoPrevious.text = getString(R.string.action_close)
                }
                else -> {
                    binding.contentTuto.buttonTutoNext.text = getString(R.string.action_next)
                    binding.contentTuto.buttonTutoPrevious.text = getString(R.string.action_previous)
                }
            }
        }
    }

    private inner class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = NUM_PAGES

        override fun createFragment(position: Int): Fragment {
            return when(position) {
                1 -> GestureTutorialFragment()
                else -> GenericTutorialFragment.newInstance(getString(R.string.action_scan_2ddoc), getString(R.string.tuto_scan_instructions), R.drawable.ic_icon_qr_code)
            }
        }
    }
}