package com.ingroupe.verify.anticovid.service.api.configuration.conf

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConfGroup(
    @SerializedName("label")
    var labelGroup: String? = null,
    @SerializedName("order")
    var orderGroup: Int? = null,
    @SerializedName("fields")
    var fields: ArrayList<ConfField>? = null,
    @SerializedName("showGroupName")
    var showGroupName: Boolean? = null

) : Serializable