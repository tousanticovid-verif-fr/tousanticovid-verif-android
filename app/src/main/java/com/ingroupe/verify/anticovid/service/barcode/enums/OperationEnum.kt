package com.ingroupe.verify.anticovid.service.barcode.enums

enum class OperationEnum {
    CONVERT_HEX_TO_DATE,
    CONVERT_STRING_TO_DATE,
    DECODE_BASE16,
    DECODE_BASE32,
    DECODE_BASE36,
    GET_COUNTRY,
    REMOVE_SLASH,
    TRANSLATE_BOOLEAN
}