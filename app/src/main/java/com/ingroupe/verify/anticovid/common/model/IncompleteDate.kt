package com.ingroupe.verify.anticovid.common.model

class IncompleteDate(var month: String? = null, var year: String? = null)