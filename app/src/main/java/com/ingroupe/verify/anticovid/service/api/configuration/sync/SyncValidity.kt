package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName

class SyncValidity (
    @SerializedName("vaccinePassStartDate")
    var vaccinePassStartDate: String? = null,
    @SerializedName("health")
    var health: SyncValidityMode? = null,
    @SerializedName("vaccine")
    var vaccine: SyncValidityMode? = null,
    @SerializedName("plus")
    var plus: SyncValidityMode? = null
)
