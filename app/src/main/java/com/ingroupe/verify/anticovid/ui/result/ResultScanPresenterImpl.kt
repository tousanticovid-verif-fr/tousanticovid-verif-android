package com.ingroupe.verify.anticovid.ui.result

import android.content.Context
import android.util.Log
import androidx.annotation.VisibleForTesting
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.common.model.ResultData
import com.ingroupe.verify.anticovid.common.model.ResultScan
import com.ingroupe.verify.anticovid.common.model.TravelConfiguration
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfResult
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfStatic
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfType
import com.ingroupe.verify.anticovid.service.document.model.*
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter


class ResultScanPresenterImpl(
    private val view: ResultScanView?
) : ResultScanPresenter {

    companion object {
        const val TAG = "ResultScanP"
    }

    override fun reloadConf(model: SharedViewModel) {
        JWTUtils.loadConf(model, false)
    }

    override fun prepareResult(
        resultScan: ResultScan,
        resultType: String?,
        documentDataResult: DocumentDataResult,
        configuration: ConfResult?,
        travelConfiguration: TravelConfiguration?,
        context: Context,
        displayMode: Constants.DisplayMode,
        engineData: DocumentEngineDataResult?
    ) {

        // 1 - faire une liste temporaire des résultats avec le même format pour chaque champ
        val tempResultScan = prepareDocumentResult(documentDataResult)

        // 2 - trouver la conf du bon type
        val confType = configuration?.types?.find { type -> type.resourceType.equals(resultType) }


        if (confType == null) {
            prepareUnknownType(tempResultScan, resultScan, configuration?.confStatic ?: arrayListOf(), context)
        } else {
            prepareKnownType(tempResultScan, resultScan, confType, travelConfiguration, context, displayMode, engineData)
        }


        // 3 - gestion fond et des groupes vides
        var position = 0
        var previousHeader: ResultData? = null
        val listEmptyGroup = mutableListOf<ResultData>()
        resultScan.datas.forEach {
            if (it.isGroupHeader) {
                previousHeader?.let { previous -> listEmptyGroup.add(previous) }
                position = 0
                previousHeader = it
            } else {
                if(it.format != Constants.CODE_HIDDEN) {
                    if (position % 2 == 1) {
                        it.withBackground = true
                    }
                    position++
                    previousHeader = null
                }
            }
        }
        // on vérifie si le dernier groupe est vide
        previousHeader?.let { previous -> listEmptyGroup.add(previous) }

        listEmptyGroup.forEach { resultScan.datas.remove(it) }
    }

    // cas des formats des types de 2D-Doc connus du service Document
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun prepareKnownType(
        tempResultScan: ResultScan,
        resultScan: ResultScan,
        confType: ConfType,
        travelConfiguration: TravelConfiguration?,
        context: Context,
        displayMode: Constants.DisplayMode,
        engineData: DocumentEngineDataResult?
    ) {
        // 2-1 - validité
        val currentValidityField = tempResultScan.datas.find { it.name == Constants.DYNAMIC_VALIDITY_FIELD }
        currentValidityField?.let { data ->
            // on l'enlève de la liste temporaire
            tempResultScan.datas.remove(data)

            data.isValidity = true
            data.format = Constants.CODE_VALIDITY_KO
            confType.information?.validityValuesFormat?.let { valuesFormat ->
                valuesFormat[data.value]?.let { data.format = it }
            }

            var isGlobalInfo = true
            if(confType.resourceType in listOf(
                    "2D-DOC_B2",
                    "2D-DOC_L1",
                    DocumentStaticDccResult.DccType.DCC_VACCINATION.text,
                    DocumentStaticDccResult.DccType.DCC_RECOVERY.text,
                    DocumentStaticDccResult.DccType.DCC_TEST.text,
                    DocumentStaticDccResult.DccType.DCC_ACTIVITY.text,
                    DocumentStaticDccResult.DccType.DCC_EXEMPTION.text)) {
                val validityGlobal =
                    tempResultScan.datas.find { resultData -> resultData.name == Constants.DYNAMIC_VALIDITY_GLOBAL_FIELD }?.value

                when (validityGlobal) {
                    Constants.GlobalValidity.OK.text -> {
                        data.format = Constants.CODE_VALIDITY_OK
                    }
                    Constants.GlobalValidity.INFO.text -> {
                        data.format = Constants.CODE_VALIDITY_INFO
                        isGlobalInfo = false
                    }
                    Constants.GlobalValidity.KO.text -> {
                        data.format = Constants.CODE_VALIDITY_KO
                    }
                    Constants.GlobalValidity.KO_RULE.text -> {
                        data.format = Constants.CODE_VALIDITY_KO
                        isGlobalInfo = false
                    }
                }
            }

            if(!isGlobalInfo && engineData != null) {
                if(displayMode == Constants.DisplayMode.OT) {

                    // on affiche l'alerte si, en sortie, le pays de départ n'est pas vert
                    engineData.alertMessage?.let {
                        val alertData = ResultData()
                        alertData.value = it
                        alertData.format = Constants.CODE_WARN_TEXT_1
                        resultScan.datas.add(alertData)
                    }

                    showControlDate(resultScan, travelConfiguration, displayMode, context)

                    engineData.engineResults.let {
                        if (it.isNotEmpty()) {
                            val resultCountriesData = ResultData()
                            resultCountriesData.format = Constants.CODE_COUNTRIES
                            resultCountriesData.engineResultDatas = it
                            resultScan.datas.add(resultCountriesData)
                        }
                    }
                }else {
                    showControlDate(resultScan, travelConfiguration, displayMode, context)
                    resultScan.datas.add(data)
                }
            } else {
                showControlDate(resultScan, travelConfiguration, displayMode, context)
                resultScan.datas.add(data)
            }
        }

        // 2-2 - parcourir groupe
        confType.groups?.forEach { group ->
            val resultData = ResultData()
            group.labelGroup?.let { label -> resultData.label = label }
            resultData.isGroupHeader = true
            if(group.showGroupName != false) {
                resultScan.datas.add(resultData)
            }

            // 2-3 - parcourir les champs et rajouter les infos
            group.fields?.forEach { field ->

                val data = tempResultScan.datas.find { it.name == field.name }
                if (data == null || data.value.isBlank()) {
                    Log.w(TAG, "Données de contrôle absente ou vide : " + field.name)
                } else {
                    // on l'enlève de la liste temporaire
                    tempResultScan.datas.remove(data)

                    field.label?.let { label -> data.label = label }
                    field.defaultFormat?.let { format -> data.format = format }
                    field.valuesFormat?.let { valuesFormat ->
                        valuesFormat[data.value]?.let { value -> data.format = value }
                    }
                    resultScan.datas.add(data)
                }
            }
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun showControlDate(
        resultScan: ResultScan,
        travelConfiguration: TravelConfiguration?,
        displayMode: Constants.DisplayMode,
        context: Context
    ) {

        if(displayMode == Constants.DisplayMode.OT) {

            // on affiche la date effective du controle si ce n'est pas la date du jour
            val resultDateData = ResultData()

            travelConfiguration?.let {
                if (it.isCustomDate) {
                    val dateTimePickedByOT =
                        it.timeUTC!!.withZoneSameInstant(ZoneId.systemDefault())
                    resultDateData.label =
                        context.getString(R.string.result_label_control_date)
                    resultDateData.value =
                        dateTimePickedByOT.format(DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMAT))
                    resultDateData.format =
                        Constants.CODE_FORMAT_2 // en orange pour que ce soit visible
                    resultScan.datas.add(resultDateData)
                }
            }
        }
    }

    // cas des types de 2D-Doc non traités par le service Document
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun prepareUnknownType(
        tempResultScan: ResultScan,
        resultScan: ResultScan,
        confStatic: ArrayList<ConfStatic>,
        context: Context
    ) {

        // 2-1 - validité
        val currentValidityField = tempResultScan.datas.find { it.name == Constants.SIGNATURE_FIELD }
        currentValidityField?.let { data ->

            val valData = ResultData()
            valData.label = data.label
            valData.value = data.value
            valData.position = data.position
            valData.withBackground = data.withBackground
            valData.isGroupHeader = data.isGroupHeader
            valData.name = data.name
            valData.isStatic = data.isStatic
            valData.isSignatureValid = data.isSignatureValid

            valData.isValidity = true
            valData.format = Constants.CODE_VALIDITY_KO
            valData.isSignatureValid?.let {
                if (it) {
                    valData.format = Constants.CODE_VALIDITY_OK
                }
            }
            resultScan.datas.add(valData)
        }

        // 2-2 - champs statiques par groupe
        confStatic.forEach { conf ->
            val resultData = ResultData()
            conf.label?.let { label -> resultData.label = label }
            resultData.isGroupHeader = true
            resultScan.datas.add(resultData)

            tempResultScan.datas.forEach { data ->
                if (data.name.startsWith("static." + conf.name)) {

                    // pas besoin d'enlever de la liste temporaire

                    if (data.name == "static.signature.status") {
                        data.label = context.getString(R.string.result_label_signature)
                        data.format = Constants.CODE_CHECK_3
                        data.isSignatureValid?.let {
                            if (it) {
                                data.format = Constants.CODE_CHECK_1
                            }
                        }

                    } else {
                        data.format = Constants.CODE_FORMAT_1
                    }
                    resultScan.datas.add(data)
                }
            }
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun prepareDocumentResult(documentDataResult: DocumentDataResult): ResultScan {
        val tempResultScan = ResultScan()

        documentDataResult.static?.let { static ->

            if(static is DocumentStatic2ddocResult) {

                static.header?.fields?.let {
                    prepareStaticResultDatas(it, "static.header", tempResultScan)
                }

                static.message?.fields?.let {
                    prepareStaticResultDatas(it, "static.message", tempResultScan)
                }

                static.annexe?.fields?.let {
                    prepareStaticResultDatas(it, "static.annexe", tempResultScan)
                }
            } else if(static is DocumentStaticDccResult) {

                static.dccData?.greenCertificate?.let {
                    val firstName = it.person.givenName ?: it.person.standardisedGivenName
                    val lastName = it.person.familyName ?: it.person.standardisedFamilyName
                    addResultByValue(it.dateOfBirth, "dateOfBirth", tempResultScan, true)
                    addResultByValue(firstName?.uppercase(), "givenName", tempResultScan)
                    addResultByValue(lastName.uppercase(), "familyName", tempResultScan)
                }

                static.dccData?.greenCertificate?.tests?.get(0)?.let {
                    addResultByValue(it.dateTimeOfCollection, "dateTimeOfCollection", tempResultScan,
                        isDate = true,
                        withTime = true
                    )
                    addResultByValue(it.disease, "disease", tempResultScan)
                    addResultByValue(it.testName, "testName", tempResultScan)
                    addResultByValue(it.testNameAndManufacturer, "testNameAndManufacturer", tempResultScan)
                    addResultByValue(it.testingCentre, "testingCentre", tempResultScan)
                    addResultByValue(it.countryOfVaccination, "countryOfVaccination", tempResultScan)
                    addResultByValue(it.certificateIssuer, "certificateIssuer", tempResultScan)
                    addResultByValue(it.certificateIdentifier, "certificateIdentifier", tempResultScan)
                }

                static.dccData?.greenCertificate?.vaccinations?.get(0)?.let {
                    addResultByValue(it.dateOfVaccination, "dateOfVaccination", tempResultScan, true)
                    addResultByValue(it.medicinalProduct, "medicinalProduct", tempResultScan)
                    addResultByValue(it.disease, "disease", tempResultScan)
                    addResultByValue(it.vaccine, "vaccine", tempResultScan)
                    addResultByValue(it.manufacturer, "manufacturer", tempResultScan)
                    addResultByValue(it.countryOfVaccination, "countryOfVaccination", tempResultScan)
                    addResultByValue(it.certificateIssuer, "certificateIssuer", tempResultScan)
                    addResultByValue(it.certificateIdentifier, "certificateIdentifier", tempResultScan)
                }

                static.dccData?.greenCertificate?.recoveryStatements?.get(0)?.let {
                    addResultByValue(it.dateOfFirstPositiveTest, "dateOfFirstPositiveTest", tempResultScan, true)
                    addResultByValue(it.certificateValidFrom, "certificateValidFrom", tempResultScan, true)
                    addResultByValue(it.certificateValidUntil, "certificateValidUntil", tempResultScan, true)
                    addResultByValue(it.disease, "disease", tempResultScan)
                    addResultByValue(it.countryOfVaccination, "countryOfVaccination", tempResultScan)
                    addResultByValue(it.certificateIssuer, "certificateIssuer", tempResultScan)
                    addResultByValue(it.certificateIdentifier, "certificateIdentifier", tempResultScan)
                }

                static.dccData?.greenCertificate?.exemption?.let {
                    addResultByValue(it.disease, "disease", tempResultScan)
                    addResultByValue(it.exemptionStatus, "exemptionStatus", tempResultScan)
                    addResultByValue(it.dateValidFrom, "dateValidFrom", tempResultScan, true)
                    addResultByValue(it.dateValidUntil, "dateValidUntil", tempResultScan, true)
                    addResultByValue(it.countryOfVaccination, "countryOfVaccination", tempResultScan)
                    addResultByValue(it.certificateIssuer, "certificateIssuer", tempResultScan)
                    addResultByValue(it.certificateIdentifier, "certificateIdentifier", tempResultScan)
                }
            }

            static.signature?.status?.let { status ->
                val resultData = ResultData()
                resultData.name = "static.signature.status"
                resultData.label = "static.signature.status"
                resultData.value = status
                resultData.isStatic = true
                resultData.isSignatureValid = static.signature?.isValid
                tempResultScan.datas.add(resultData)
            }

        }
        documentDataResult.dynamic?.let {
            if (it.size > 0) {
                prepareDynamicResultDatas(it[0], tempResultScan)
            }
        }

        return tempResultScan
    }

    private fun addResultByValue(value: String?, fieldName: String, tempResultScan : ResultScan, isDate: Boolean = false, withTime: Boolean = false) {
        if(value != null) {
            val resultData = ResultData()
            resultData.name = "static.$fieldName"
            resultData.label = "fieldName"
            resultData.value = value
            if (isDate && !withTime) {
                try {
                    val date = LocalDate.parse(value, DateTimeFormatter.ISO_DATE)
                    resultData.value =
                        date.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT))
                } catch (e: Exception) {
                    try {
                        val date = LocalDate.parse(value, DateTimeFormatter.ISO_DATE_TIME)
                        resultData.value =
                            date.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT))
                    } catch (e: Exception) {
                        Utils.splitDateFromString(resultData.value)?.let {
                            if (it.month != null) {
                                resultData.value = "XX/" + it.month + "/" + it.year
                            } else {
                                resultData.value = "XX/XX/" + it.year
                            }
                        }
                    }
                }
            } else if (isDate && withTime) {
                try {
                    val date =
                        OffsetDateTime.parse(resultData.value, DateTimeFormatter.ISO_DATE_TIME)
                    resultData.value = date
                        .atZoneSameInstant(ZoneId.systemDefault())
                        .format(DateTimeFormatter.ofPattern(Constants.DATE_TIME_FORMAT))
                } catch (e: Exception) {
                    Log.e(TAG, "error parsing date time $fieldName")
                }
            }

            resultData.isStatic = true
            tempResultScan.datas.add(resultData)
        }
    }

    private fun prepareDynamicResultDatas(datas: Map<String, String>, resultScan: ResultScan) {
        datas.keys.forEach { k ->

            val v = datas[k]

            if (v != null && "" != v) {
                val resultData = ResultData()
                resultData.name = "dynamic.$k"
                resultData.label = k
                resultData.value = v

                if (v == "") {
                    return
                }

                resultScan.datas.add(resultData)
            }
        }
    }


    private fun prepareStaticResultDatas(datas: ArrayList<DocumentFieldResult>, zone: String, resultScan: ResultScan) {
        datas.forEach { data ->
            val resultData = ResultData()
            resultData.name = zone + "." + data.name!!
            resultData.label = data.label!!
            resultData.value = data.value!!
            resultData.isStatic = true
            resultScan.datas.add(resultData)
        }
    }

    override fun prepareErrorResult(resultScan: ResultScan, errors: ArrayList<DocumentErrorsResult>, context: Context) {

        var data = ResultData()
        data.isValidity = true
        data.format = Constants.CODE_VALIDITY_KO
        data.value = context.getString(R.string.result_invalid)
        resultScan.datas.add(data)

        errors.forEach { error ->
            data = ResultData()
            data.isGroupHeader = true
            data.label = context.getString(R.string.result_anomaly)
            resultScan.datas.add(data)

            if (error.documentNumber?.isNotEmpty() == true) {
                data = ResultData()
                data.label = context.getString(R.string.result_parameter)
                data.value = error.documentNumber!!
                data.format = Constants.CODE_FORMAT_3
                resultScan.datas.add(data)
            }

            data = ResultData()
            data.label = context.getString(R.string.result_message)
            data.value = error.message!!
            data.format = Constants.CODE_CHECK_3
            resultScan.datas.add(data)
        }
    }
}