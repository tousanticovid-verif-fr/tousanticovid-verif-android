package com.ingroupe.verify.anticovid.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.model.StatsDay
import com.ingroupe.verify.anticovid.databinding.StatisticFirstViewBinding
import com.ingroupe.verify.anticovid.databinding.StatisticViewBinding
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class StatisticAdapter(private val stats: Map<Long, StatsDay>, val context: Context)
    : RecyclerView.Adapter<StatisticAdapter.MyViewHolder>()
    {
    open class MyViewHolder(open val binding: androidx.viewbinding.ViewBinding):
            RecyclerView.ViewHolder(binding.root)


        override fun getItemViewType(position: Int): Int {
            return if(position == 0) {
                R.layout.statistic_first_view
            } else {
                R.layout.statistic_view
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            return when(viewType) {
                R.layout.statistic_first_view -> MyViewHolder(
                    StatisticFirstViewBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
                else -> MyViewHolder(
                    StatisticViewBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val date = LocalDate.now().minusDays(position.toLong())
            val epochDay = date.toEpochDay()

            when(holder.binding) {
                is StatisticFirstViewBinding -> {
                    drawChart((holder.binding as StatisticFirstViewBinding).chart1)
                    (holder.binding as StatisticFirstViewBinding).textViewDate.text =
                        context.getString(R.string.stats_current_day_count, date.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT)))
                    (holder.binding as StatisticFirstViewBinding).textViewCount.text = stats[epochDay]!!.nbTotal.toString()
                    (holder.binding as StatisticFirstViewBinding).textViewCountDuplicate.text = stats[epochDay]!!.nbDuplicate.toString()
                }
                is StatisticViewBinding -> {
                    (holder.binding as StatisticViewBinding).textViewDate.text =
                        context.getString(R.string.stats_other_day_count, date.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT)))
                    (holder.binding as StatisticViewBinding).textViewCount.text = stats[epochDay]!!.nbTotal.toString()
                    (holder.binding as StatisticViewBinding).textViewCountDuplicate.text = stats[epochDay]!!.nbDuplicate.toString()
                }
            }

            holder.binding.root.tag = position
        }

        override fun getItemCount() = stats.size

        private fun drawChart(chart: BarChart) {

            chart.description.isEnabled = false
            chart.legend.isEnabled = true

            val entries: MutableList<BarEntry> = ArrayList()
            for (data in stats) {
                // turn your data into Entry objects
                entries.add(BarEntry(data.key.toFloat(), floatArrayOf(data.value.nbTotal.toFloat(), data.value.nbDuplicate.toFloat())))
            }


            val barDataSet = BarDataSet(entries, "")
            barDataSet.setDrawIcons(false)
            barDataSet.colors = listOf(
                ContextCompat.getColor(context, R.color.in_blue),
                ContextCompat.getColor(context, R.color.in_grey_input)
            )
            barDataSet.stackLabels = arrayOf(
                context.getString(R.string.stats_graph_scan),
                context.getString(R.string.stats_graph_duplicate)
            )

            val xFormatter: ValueFormatter = object : ValueFormatter() {
                override fun getAxisLabel(value: Float, axis: AxisBase): String {
                    val date = LocalDate.ofEpochDay(value.toLong())
                    return date.format(DateTimeFormatter.ofPattern("dd/MM"))
                }
            }

            val xAxis = chart.xAxis
            xAxis.position = XAxisPosition.BOTTOM
            xAxis.setDrawGridLines(false)
            xAxis.valueFormatter = xFormatter


            val yFormatter: ValueFormatter = object : ValueFormatter() {
                override fun getAxisLabel(value: Float, axis: AxisBase): String {
                    return value.toInt().toString()
                }

                override fun getBarLabel(barEntry: BarEntry?): String {
                    return barEntry?.y?.toInt().toString()
                }

                override fun getFormattedValue(value: Float): String {
                    return value.toInt().toString()
                }
            }

            val leftAxis = chart.axisLeft
            leftAxis.axisMinimum = 0f
            leftAxis.granularity = 1F
            leftAxis.valueFormatter = yFormatter

            chart.axisRight.isEnabled = false

            val barData = BarData(barDataSet)
            barData.setValueFormatter(yFormatter)
            chart.data = barData
            chart.invalidate()
        }
}