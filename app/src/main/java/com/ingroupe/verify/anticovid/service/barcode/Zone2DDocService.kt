package com.ingroupe.verify.anticovid.service.barcode

import com.ingroupe.verify.anticovid.service.document.model.DocumentFieldResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentStaticFieldsResult
import com.ingroupe.verify.anticovid.service.barcode.database.BarcodeLoader
import com.ingroupe.verify.anticovid.service.barcode.database.entity.*
import com.ingroupe.verify.anticovid.service.barcode.enums.CharacterEnum
import com.ingroupe.verify.anticovid.service.barcode.enums.ErrorBarcodeEnum
import com.ingroupe.verify.anticovid.service.barcode.enums.OperationEnum
import java.util.*
import kotlin.collections.HashMap


abstract class Zone2DDocService {

    companion object {

        private const val ID_FIELD_SIZE = 2

        fun get2DDocHeaderZone(encodedHeader: String, header: Header): DocumentStaticFieldsResult {
            val headerZone = DocumentStaticFieldsResult()
            headerZone.fields = arrayListOf()
            headerZone.version = header.version
            header.fields.forEach { strHeaderField ->
                val headerField = BarcodeLoader.findHeaderFieldByName(strHeaderField)

                if (HeaderField.isValidHeaderField(
                        headerField
                    )
                ) {

                    addZoneField(
                        encodedHeader,
                        headerField!!.begin!!,
                        headerField.end!!,
                        headerZone,
                        headerField as Field
                    )
                }
            }
            return headerZone
        }

        fun get2DDocMessageZone(encodedMessage: String, documentType: DocumentType): DocumentStaticFieldsResult {
            val messageZone = DocumentStaticFieldsResult()
            messageZone.fields = arrayListOf()
            messageZone.type = documentType.type
            messageZone.label = documentType.label
            val documentTypeFieldMap: Map<String, DocumentTypeField> =
                getDocumentTypeFieldMap(documentType)
            try {
                var index = 0
                while (index + ID_FIELD_SIZE <= encodedMessage.length) {
                    val key = encodedMessage.substring(index, index + ID_FIELD_SIZE)
                    index += ID_FIELD_SIZE
                    val documentTypeField: DocumentTypeField? =
                        getDocumentTypeField(key, documentTypeFieldMap)
                    if (documentTypeField != null) {
                        index = if (Objects.equals(documentTypeField.min, documentTypeField.max)) {
                            addZoneField(
                                encodedMessage,
                                index,
                                index + documentTypeField.min!!,
                                messageZone,
                                documentTypeField
                            )
                            index + documentTypeField.min
                        } else {
                            getIndexForFieldOfVariableLength(
                                encodedMessage,
                                messageZone,
                                index,
                                documentTypeField
                            )
                        }
                    } else {
                        messageZone.error = ErrorBarcodeEnum.CANNOT_READ_FULL_MESSAGE
                        break
                    }
                }
            } catch (e: Exception) {
                messageZone.error = ErrorBarcodeEnum.CANNOT_READ_FULL_MESSAGE
            }
            return messageZone
        }

        private fun getDocumentTypeFieldMap(documentType: DocumentType): Map<String, DocumentTypeField> {
            val map: Map<String, DocumentTypeField> = HashMap()

            documentType.fields?.forEach { strField ->
                val field = BarcodeLoader.findDocumentTypeFieldByName(strField)
                if (field != null
                    && DocumentTypeField.isValidDocumentField(
                        field
                    )
                ) {
                    map.plus(Pair(field.name, field))
                }
            }

            return map
        }

        private fun addZoneField(
            encodedZone: String,
            begin: Int,
            end: Int,
            zone: DocumentStaticFieldsResult,
            field: Field
        ) {
            var value = encodedZone.substring(begin, end)
            value = transformFieldValue(value, field.operations)
            zone.fields?.add(DocumentFieldResult(field.name, field.label, value))
        }

        private fun getIndexForFieldOfVariableLength(
            encodedMessage: String,
            messageZone: DocumentStaticFieldsResult,
            index: Int,
            documentTypeField: DocumentTypeField
        ): Int {
            var end = index
            while (end < encodedMessage.length && end - index <= documentTypeField.max!! && CharacterEnum.isNotSpecialCharacter(
                    encodedMessage[end]
                )
            ) {
                end++
            }
            addZoneField(encodedMessage, index, end, messageZone, documentTypeField)
            return end + 1
        }

        private fun getDocumentTypeField(
            type: String,
            documentTypeFieldMap: Map<String, DocumentTypeField>
        ): DocumentTypeField? {
            return if (documentTypeFieldMap.containsKey(type)) {
                documentTypeFieldMap[type]
            } else {
                BarcodeLoader.findDocumentTypeFieldByName(type)
            }
        }

        private fun transformFieldValue(input: String, operations: List<OperationEnum>?): String {
            var value = input
            if (operations != null && operations.isNotEmpty()) {
                for (operation in operations) {
                    value = OperationService.transformFieldValue(value, operation)
                }
            }
            return value
        }
    }
}