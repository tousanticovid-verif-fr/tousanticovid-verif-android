package com.ingroupe.verify.anticovid.common.model

import com.ingroupe.verify.anticovid.service.document.model.DocumentEngineResult

class ResultData {
    var label: String = ""
    var value: String = ""
    var subValue: String = ""
    var engineResultDatas: List<DocumentEngineResult>? = null
    var position: Int = 0

    var withBackground: Boolean = false
    var isGroupHeader: Boolean = false
    var format: String = ""

    var name: String = ""
    var isStatic = false
    var isValidity = false

    var isSignatureValid: Boolean? = null
}