package com.ingroupe.verify.anticovid.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import com.ingroupe.verify.anticovid.BuildConfig
import java.io.*
import java.util.*

object FileLogger {
    fun write(text:String,context: Context){
        if (BuildConfig.LOG_ENABLED) {
            try {
                val textToWrite = "\n" + text
                val outputStream = FileOutputStream(context.cacheDir.path + "/inlog",true)
                outputStream.write(textToWrite.encodeToByteArray())
                outputStream.flush()
                outputStream.close()
            } catch (e: IOException) {
                Log.e("Exception", "File write failed: $e")
            }
        }
    }

    private fun read(context: Context): String {
        var ret = ""
        if (BuildConfig.LOG_ENABLED) {
            try {
                val inputStream: InputStream = FileInputStream(context.cacheDir.path + "/inlog")
                val inputStreamReader = InputStreamReader(inputStream)
                val bufferedReader = BufferedReader(inputStreamReader)
                ret = bufferedReader.readText()
            } catch (e: FileNotFoundException) {
                Log.e("FileLogger", "File not found: $e")
            } catch (e: IOException) {
                Log.e("FileLogger", "Can not read file: $e")
            }
        }
        return ret
    }

    fun send(activity : Activity){
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type="text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, read(activity))
        activity.startActivity(Intent.createChooser(shareIntent,"Partage"))
    }
    fun clean(context: Context){

        File(context.cacheDir.path + "/inlog").delete()
    }
}