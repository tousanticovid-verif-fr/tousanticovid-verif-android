package com.ingroupe.verify.anticovid.service.document.enums

enum class ErrorDocEnum(val message: String) {
    INF01("Document with type {0} and criteria {1} found."),
    ERR01("Document with type {0} and criteria {1} not found."),
    ERR02("Cannot get barcode 2ddoc {0} from 2D-DOC API."),
    ERR03("Could not decode DCC"),
    ERR04("Could not decode 2DDOC"),
}