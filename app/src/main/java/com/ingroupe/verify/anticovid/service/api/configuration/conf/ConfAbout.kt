package com.ingroupe.verify.anticovid.service.api.configuration.conf

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConfAbout(
    @SerializedName("url")
    var url: String? = null,
    @SerializedName("cgu")
    var cgu: String? = null,
    @SerializedName("privacyPolicy")
    var privacyPolicy: String? = null,
    @SerializedName("lastCGUVersion")
    var lastCGUVersion: String? = null,
    @SerializedName("lastPrivacyPolicyVersion")
    var lastPrivacyPolicyVersion: String? = null,
    @SerializedName("androidConfiguration")
    var androidConfiguration: ConfVersions? = null
) : Serializable