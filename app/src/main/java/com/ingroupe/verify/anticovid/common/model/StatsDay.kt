package com.ingroupe.verify.anticovid.common.model

class StatsDay {
    var nbR: Int = 0
    var nbG: Int = 0
    var nbB: Int = 0
    var nbTotal: Int = 0
    var nbDuplicate: Int = 0
}