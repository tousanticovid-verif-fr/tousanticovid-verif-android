package com.ingroupe.verify.anticovid.data.local.blacklist

import com.ingroupe.verify.anticovid.service.api.configuration.blacklist.BlacklistElement
import com.ingroupe.verify.anticovid.synchronization.elements.Blacklist

fun BlacklistElement.toBlacklistLocal(blacklistType: Blacklist.BlacklistType): BlacklistLocal = BlacklistLocal(
    id = this.id,
    type = blacklistType.text,
    hash = this.hash,
    active = this.active?: true
)

fun List<BlacklistElement>.toBlacklistLocals(blacklistType: Blacklist.BlacklistType): List<BlacklistLocal> {
    val blacklistLocals = mutableListOf<BlacklistLocal>()
    forEach {
        blacklistLocals.add(it.toBlacklistLocal(blacklistType))
    }
    return blacklistLocals
}