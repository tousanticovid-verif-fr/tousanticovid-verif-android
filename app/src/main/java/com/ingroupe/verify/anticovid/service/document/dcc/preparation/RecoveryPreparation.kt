package com.ingroupe.verify.anticovid.service.document.dcc.preparation

import com.ingroupe.verify.anticovid.service.document.dcc.DccModeInterface
import com.ingroupe.verify.anticovid.service.document.model.InfoTestForStat
import dgca.verifier.app.decoder.model.RecoveryStatement
import java.time.LocalDate

class RecoveryPreparation(
    val PositiveTestDate: LocalDate,
    val currentDate: LocalDate
) {
    companion object {

        fun getPreparation(
            statement: RecoveryStatement,
            infoTestForStat: InfoTestForStat
        ) : RecoveryPreparation? {
            val date = DccModeInterface.getLocalDateFromDcc(statement.dateOfFirstPositiveTest)
            val currentDate = LocalDate.now()

            infoTestForStat.updateInfo(statement.certificateIssuer)

            return if(date == null) {
                null
            } else {
                RecoveryPreparation(date, currentDate)
            }
        }

    }
}
