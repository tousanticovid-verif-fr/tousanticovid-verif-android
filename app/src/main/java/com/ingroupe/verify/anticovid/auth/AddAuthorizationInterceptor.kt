package com.ingroupe.verify.anticovid.auth

import com.ingroupe.verify.anticovid.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class AddAuthorizationInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()

        builder.addHeader("Authorization", "Bearer " + BuildConfig.PERSISTENT_TOKEN)

        return chain.proceed(builder.build())
    }
}
