package com.ingroupe.verify.anticovid.ui.scan

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.lifecycle.ViewModelProvider
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.SettingsChildFragment
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.camera.ScanViewModel
import com.ingroupe.verify.anticovid.common.*
import com.ingroupe.verify.anticovid.databinding.PopupPremiumModeActivatedBinding
import com.ingroupe.verify.anticovid.databinding.ProgressOverlayBinding
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentResult
import com.ingroupe.verify.anticovid.synchronization.ConfServiceUtils
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils
import com.ingroupe.verify.anticovid.ui.actionchoice.ActionChoiceChildFragment
import com.ingroupe.verify.anticovid.ui.result.ResultScanChildFragment
import com.ingroupe.verify.anticovid.ui.tutorialscan2ddoc.TutorialScanActivity
import com.ingroupe.verify.anticovid.ui.tutorialresult.lite.TutorialResultLiteChildFragment
import com.ingroupe.verify.anticovid.ui.tutorialresult.premium.TutorialResultPremiumChildFragment
import com.scandit.datacapture.barcode.data.Barcode
import com.scandit.datacapture.barcode.data.Symbology
import com.scandit.datacapture.barcode.ui.overlay.BarcodeCaptureOverlay
import com.scandit.datacapture.core.common.geometry.Anchor
import com.scandit.datacapture.core.ui.DataCaptureView
import com.scandit.datacapture.core.ui.control.TorchSwitchControl
import com.scandit.datacapture.core.ui.style.Brush
import com.scandit.datacapture.core.ui.viewfinder.RectangularViewfinder
import com.scandit.datacapture.core.ui.viewfinder.RectangularViewfinderStyle
import org.greenrobot.eventbus.EventBus
import java.util.*


class ScanChildFragment : FeatureChildFragment(), ScanView, ScanViewModel.ResultListener {

    override fun getTitle(): String = "Scan du 2D-Doc"
    override fun getTitleId(): Int = R.string.title_scan

    companion object {
        const val TAG = "Scan"
        fun newInstance() = ScanChildFragment()
    }

    private var presenter: ScanPresenter? = null
    private var viewModel: ScanViewModel? = null

    private lateinit var model: SharedViewModel
    private var isCallingWS = false
    private var isShowingPopup = false

    private var dialogLoader : AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ScanViewModel::class.java)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (presenter == null) {
            presenter = ScanPresenterImpl(this)
        }
        // To visualize the on-going barcode capturing process on screen,
        // setup a data capture view that renders the camera preview.
        // The view must be connected to the data capture context.
        // To visualize the on-going barcode capturing process on screen,
        // setup a data capture view that renders the camera preview.
        // The view must be connected to the data capture context.
        val view = DataCaptureView.newInstance(requireContext(), viewModel?.dataCaptureContext)
        view.logoAnchor = Anchor.BOTTOM_LEFT

        // ajout du bouton flash
        val torchSwitchControl = TorchSwitchControl(requireContext())
        torchSwitchControl.setTorchOnImage(R.drawable.button_flash_on)
        torchSwitchControl.setTorchOffImage(R.drawable.button_flash_off)
        view.addControl(torchSwitchControl)

        // Add a barcode capture overlay to the data capture view to render the tracked
        // barcodes on top of the video preview.
        // This is optional, but recommended for better visual feedback.
        val overlay = BarcodeCaptureOverlay.newInstance(viewModel?.barcodeCapture!!, view)

        // ajout de la mire
        val viewfinder = RectangularViewfinder(RectangularViewfinderStyle.SQUARE)
        viewfinder.color = requireActivity().getColor(R.color.in_gold)
        overlay.viewfinder = viewfinder

        // Adjust the overlay's barcode highlighting to match the new viewfinder styles and improve
        // the visibility of feedback. With 6.10 we will introduce this visual treatment as a new
        // style for the overlay.
        val brush = Brush(Color.TRANSPARENT, Color.WHITE, 3f)
        overlay.brush = brush

        return view
    }

    @Suppress("DEPRECATION")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCallingWS = false
        val bindingOverlay =
            ProgressOverlayBinding.inflate(LayoutInflater.from(context))

        dialogLoader = AlertDialog.Builder(
            ContextThemeWrapper(
                activity,
                R.style.AlertDialogCustom
            )
        )
        .setView(bindingOverlay.root)
        .create()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_scan_2ddoc, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun showNavigation(): MainActivity.NavigationIcon {
        return MainActivity.NavigationIcon.BACK
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume")
        isCallingWS = false

        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        model.currentResponse = null
        model.currentResponseDate = null

        viewModel?.setListener(this)
        // Switch camera on to start streaming frames.
        // The camera is started asynchronously and will take some time to completely turn on.
        viewModel?.startFrameSource()
        if (!isCallingWS) {
            viewModel?.resumeScanning()
        }
    }

    override fun onPause() {
        super.onPause()
        viewModel?.setListener(null)
        // Switch camera off to stop streaming frames.
        // The camera is stopped asynchronously and will take some time to completely turn off.
        // Until it is completely stopped, it is still possible to receive further results, hence
        // it's a good idea to first disable barcode tracking as well.
        viewModel?.pauseScanning()
        viewModel?.stopFrameSource()
        hideLoaderOverlay()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_help -> {
                goToHelp()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun showResult(response: DocumentResult) {
        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        model.currentResponse = response
        model.currentResponseDate = Date()
        model.resultAlreadyViewed = false

        goToResult()
    }

    override fun onDccLoaded(documentResult: DocumentResult?) {
        hideLoaderOverlay()

        context?.let { c ->
            if (documentResult != null) {
                presenter?.checkExpirationAndDo(model, c) {
                    showResult(documentResult)
                }
            } else {
                isCallingWS = false
                Utils.showToast(activity as Activity, getString(R.string.scan_not_2ddoc))
                viewModel?.resumeScanningDelayed()
            }
        }
    }

    private fun goToResult() {

        val settingsPref =
            App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
        val showTuto = settingsPref?.getBoolean(Constants.SavedItems.SHOW_RESULT_TUTO.text, true)
        if (showTuto == true) {

            if(JWTUtils.isDisplayPremium()) {
                featureFragment?.replaceFragment(TutorialResultPremiumChildFragment.TAG)
            } else {
                featureFragment?.replaceFragment(TutorialResultLiteChildFragment.TAG)
            }
        } else {
            featureFragment?.replaceFragment(ResultScanChildFragment.TAG)
        }
    }

    private fun goToHelp() {
        val i = Intent(activity, TutorialScanActivity::class.java)
        activity?.startActivity(i)
    }

    private fun goToActionChoice() {
        featureFragment?.popToTag(ActionChoiceChildFragment.TAG)
    }

    override fun showErrorMessage(title: Int, message: Int) {
        Log.e(TAG, "showErrorMessage")
        isCallingWS = false

        this.context?.let { context ->

            val dialog = AlertDialog.Builder(context).setTitle(getString(title)).setMessage(
                getString(
                    message
                )
            )
                .setNegativeButton("OK") { dialog, _ -> dialog.dismiss()
                    goToActionChoice()
                }.create()

            dialog.show()
        }
    }

    private fun showLoaderOverlay() {
        context?.let {
            dialogLoader?.show()
            dialogLoader?.window?.setLayout(Utils.dpsToPxs(150, it), Utils.dpsToPxs(150, it))
            dialogLoader?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    private fun hideLoaderOverlay() {
        dialogLoader?.dismiss()
    }

    override fun onCodeScanned(barcodeResult: Barcode?) {

        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        context?.let { c ->
            presenter?.checkExpirationAndDo(model, c) {

                if (barcodeResult != null && !isCallingWS && !isShowingPopup) {

                    if (barcodeResult.symbology != Symbology.DATA_MATRIX) {
                        isCallingWS = true

                        if (barcodeResult.symbology == Symbology.QR && barcodeResult.data != null) {

                            when {
                                barcodeResult.data!!.contains(Regex("^${Constants.DccFormat.DCC_EU.prefix}")) -> {
                                    showLoaderOverlay()
                                    presenter?.checkDcc(
                                        barcodeResult.data!!,
                                        c,
                                        model.travelConfiguration
                                    )
                                }
                                barcodeResult.data!!.contains(Regex("^${Constants.DccFormat.DCC_EXEMPTION.prefix}")) -> {
                                    showLoaderOverlay()
                                    presenter?.checkDccExemption(
                                        barcodeResult.data!!,
                                        c,
                                        model.travelConfiguration
                                    )
                                }
                                barcodeResult.data!!.contains(Regex("^${Constants.DccFormat.DCC_ACTIVITY.prefix}")) -> {
                                    showLoaderOverlay()
                                    presenter?.checkDccActivity(
                                        barcodeResult.data!!,
                                        c,
                                        model.travelConfiguration
                                    )
                                }
                                else -> checkUnknownDcc(barcodeResult.data!!)
                            }
                        }

                    } else if (barcodeResult.data!!.length >= 23
                        && barcodeResult.data!!.subSequence(20, 22) != "B2"
                        && barcodeResult.data!!.subSequence(20, 22) != "L1"
                    ) {

                        isCallingWS = false
                        Utils.showToast(
                            activity as Activity,
                            getString(R.string.scan_not_2ddoc_B2_L1)
                        )
                        viewModel?.resumeScanning()

                    } else {

                        isCallingWS = true

                        val isDecoded = presenter?.on2dDocDetected(
                            c,
                            barcodeResult.data!!,
                            model.travelConfiguration
                        )

                        if (isDecoded != true) {
                            isCallingWS = false
                            Utils.showToast(
                                activity as Activity,
                                getString(R.string.scan_not_2ddoc_B2_L1)
                            )
                            viewModel?.resumeScanning()
                        }
                    }
                }
            }
        }
    }

    private fun checkUnknownDcc(qrCode: String) {
        context?.let { c ->
            isCallingWS = false
            Utils.showToast(activity as Activity, getString(R.string.scan_not_2ddoc))
            viewModel?.resumeScanningDelayed()
        }
    }
}