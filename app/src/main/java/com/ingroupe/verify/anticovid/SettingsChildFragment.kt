package com.ingroupe.verify.anticovid

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri.fromParts
import android.os.Bundle
import android.provider.Settings
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.lifecycle.ViewModelProvider
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.common.*
import com.ingroupe.verify.anticovid.data.EngineManager
import com.ingroupe.verify.anticovid.databinding.PopupDeactivatePremiumModeBinding
import com.ingroupe.verify.anticovid.databinding.SettingsMainBinding
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncResult
import com.ingroupe.verify.anticovid.synchronization.ConfServiceUtils
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils
import org.greenrobot.eventbus.EventBus


class SettingsChildFragment : FeatureChildFragment(), SynchronisationUtils.SyncListener, ConfServiceUtils.ConfServiceListener {
    override fun getTitle(): String = "Paramètres"
    override fun getTitleId(): Int = R.string.title_settings

    companion object {
        const val TAG = "settings"
        fun newInstance() = SettingsChildFragment()
    }
    private var _binding: SettingsMainBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var model: SharedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = SettingsMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        val settingsPref =
            App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
        val showTutoResult = settingsPref?.getBoolean(Constants.SavedItems.SHOW_RESULT_TUTO.text, true) ?: true
        val token = settingsPref?.getString(Constants.SavedItems.CURRENT_TOKEN.text, null)
        val showTutoScan = settingsPref?.getBoolean(Constants.SavedItems.SHOW_SCAN_TUTO.text, true) ?: true

        binding.switchTutoScan.isChecked = showTutoScan
        binding.switchTutoResult.isChecked = showTutoResult

        binding.switchTutoScan.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener,
            CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
            }

            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                settingsPref?.let {
                    with(it.edit()) {
                        putBoolean(
                            Constants.SavedItems.SHOW_SCAN_TUTO.text,
                            isChecked
                        )
                        apply()
                    }
                    Utils.showToast(activity as Activity, getString(R.string.settings_change_saved))
                }
            }
        })

        binding.switchTutoResult.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener,
            CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
            }

            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                settingsPref?.let {
                    with(it.edit()) {
                        putBoolean(
                            Constants.SavedItems.SHOW_RESULT_TUTO.text,
                            isChecked
                        )
                        apply()
                    }
                    Utils.showToast(activity as Activity, getString(R.string.settings_change_saved))
                }
            }
        })

        val content = SpannableString(binding.textViewConfTitle.text)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        binding.textViewConfTitle.text = content

        binding.constraintLayoutConfAccess.setOnClickListener {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = fromParts("package", activity?.packageName, null)
            intent.data = uri
            context?.startActivity(intent)
        }

        if(token != null) {
            binding.constraintLayoutToken.visibility = View.VISIBLE
            binding.textViewTokenText3.text = getString(R.string.settings_ot_label_3, JWTUtils.daysBeforeExpiration())
            binding.switchPremium.isChecked = true

            binding.switchPremium.setOnCheckedChangeListener { _, checked ->

                if(!checked) {

                    context?.let { c ->
                        val bindingPremiumModeDialog =
                            PopupDeactivatePremiumModeBinding.inflate(LayoutInflater.from(c))

                        val dialogDeactivatePremiumMode = AlertDialog.Builder(
                            ContextThemeWrapper(
                                activity,
                                R.style.AlertDialogCustom
                            )
                        )
                            .setView(bindingPremiumModeDialog.root)
                            .create()

                        dialogDeactivatePremiumMode.show()

                        dialogDeactivatePremiumMode.setOnDismissListener {
                            binding.switchPremium.isChecked = JWTUtils.isPremium()
                        }

                        bindingPremiumModeDialog.buttonAccept.setOnClickListener {
                            JWTUtils.resetPrefsForToken()
                            model.configuration = Utils.loadFromAsset(Constants.CONF_BASIC)
                            binding.constraintLayoutToken.visibility = View.GONE
                            (c as MainActivity).changeMode(true)
                            binding.switchTutoResult.isChecked = true
                            Utils.showToast(
                                activity as Activity,
                                getString(R.string.settings_change_saved)
                            )
                            dialogDeactivatePremiumMode.dismiss()
                        }

                        bindingPremiumModeDialog.buttonRefuse.setOnClickListener {
                            dialogDeactivatePremiumMode.dismiss()
                        }
                    }
                }
            }
        }

        binding.textViewSyncLabel.text = getString(
            R.string.settings_sync_label,
            SynchronisationUtils.getDateLastSynchronization()
        )

        binding.buttonDoSync.setOnClickListener {
            ConfServiceUtils.callSynchronization(this, TAG, false)
        }
    }

    override fun showNavigation(): MainActivity.NavigationIcon {
        return MainActivity.NavigationIcon.BACK
    }

    override fun onSynchronizationDone() {
        showLoading(false)
        binding.textViewSyncLabel.text = getString(
            R.string.settings_sync_label,
            SynchronisationUtils.getDateLastSynchronization()
        )
        EngineManager.blacklistRepository.loadBlacklistPageWithoutCondition()
        Utils.showToast(activity as Activity, getString(R.string.settings_sync_done))
    }

    override fun onSynchronizationError() {
        showLoading(false)
        activity?.let {
            Utils.showToast(it as Activity, getString(R.string.settings_sync_error), Toast.LENGTH_LONG)
        }
    }

    override fun saveResult(syncResult: SyncResult?) {
        context?.let {
            SynchronisationUtils.saveSynchronization(
                syncResult,
                it,
                this,
                this,
                TAG,
                false)
        }
    }

    override fun showErrorMessage(title: Int, message: Int) {
        Log.e(TAG, "showErrorMessage")

        this.context?.let { context ->

            val dialog = AlertDialog.Builder(context).setTitle(getString(title)).setMessage(getString(message))
                .setNegativeButton("OK") { dialog, _ -> dialog.dismiss() }.create()
            dialog.show()
        }
    }

    override fun showLoading(show: Boolean) {
        Log.d(TAG, "show loading $show")
        EventBus.getDefault().post(ShowLoadingAnimEvent(activity, show))
    }
}