package com.ingroupe.verify.anticovid.synchronization.elements

import android.content.Context
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.BuildConfig
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfAbout
import com.ingroupe.verify.anticovid.synchronization.ConfServiceUtils

object Config {

    private const val LAST_CGU_VERSION = "LAST_CGU_VERSION"
    private const val LAST_PRIVACY_POLICY_VERSION = "LAST_PRIVACY_POLICY_VERSION"
    private const val INFORM_NEW_CGU_OR_POLICY = "INFORM_NEW_CGU_OR_POLICY"

    private const val LAST_APP_VERSION_SYNCHRONIZED = "LAST_APP_VERSION_SYNCHRONIZED"
    private const val INFORM_NEW_MINOR_APP_VERSION = "INFORM_NEW_MINOR_APP_VERSION"
    private const val INFORM_NEW_MAJOR_APP_VERSION = "INFORM_NEW_MAJOR_APP_VERSION"

    fun getConfig(listener: ConfServiceUtils.ConfServiceListener, tag: String, backgroundWork: Boolean) {
        ConfServiceUtils.callConfiguration(
            listener,
            tag,
            backgroundWork
        )
    }

    fun updateConfig(confAbout: ConfAbout?) {
        val configPref =
            App.getContext().getSharedPreferences(Constants.CONFIG_KEY, Context.MODE_PRIVATE)
        val currentAppVersion = BuildConfig.VERSION_CODE

        confAbout?.let { newConf ->

            var informNewCguPolVersion = false
            val lastCguVersion = configPref.getInt(LAST_CGU_VERSION, 2)
            val lastPrivacyPolicyVersion = configPref.getInt(LAST_PRIVACY_POLICY_VERSION, 2)
            if(newConf.lastCGUVersion?.toIntOrNull() ?:0 > lastCguVersion) { informNewCguPolVersion = true }
            if(newConf.lastPrivacyPolicyVersion?.toIntOrNull() ?:0 > lastPrivacyPolicyVersion) { informNewCguPolVersion = true }

            val informNewMinorAppVersion = newConf.androidConfiguration?.lastMinorVersion?.toIntOrNull() ?:0 > currentAppVersion
            val informNewMajorAppVersion = newConf.androidConfiguration?.lastMajorVersion?.toIntOrNull() ?:0 > currentAppVersion

            with(configPref.edit()) {

                putBoolean(INFORM_NEW_CGU_OR_POLICY, informNewCguPolVersion)
                putInt(LAST_CGU_VERSION, newConf.lastCGUVersion?.toInt() ?: lastCguVersion)
                putInt(LAST_PRIVACY_POLICY_VERSION, newConf.lastPrivacyPolicyVersion?.toInt() ?: lastPrivacyPolicyVersion)

                putInt(LAST_APP_VERSION_SYNCHRONIZED, currentAppVersion)
                putBoolean(INFORM_NEW_MINOR_APP_VERSION, informNewMinorAppVersion)
                putBoolean(INFORM_NEW_MAJOR_APP_VERSION, informNewMajorAppVersion)

                apply()
            }
        }
    }

    fun isNewAppVersionAvailable(): Boolean {
        val configPref =
            App.getContext().getSharedPreferences(Constants.CONFIG_KEY, Context.MODE_PRIVATE)

        return configPref.getBoolean(INFORM_NEW_MINOR_APP_VERSION, false) || configPref.getBoolean(INFORM_NEW_MAJOR_APP_VERSION, false)
    }

    fun isNewAppVersionMajor(): Boolean {
        val configPref =
            App.getContext().getSharedPreferences(Constants.CONFIG_KEY, Context.MODE_PRIVATE)

        return configPref.getBoolean(INFORM_NEW_MAJOR_APP_VERSION, false)
    }

    fun checkIfAppUpdated() {
        val configPref =
            App.getContext().getSharedPreferences(Constants.CONFIG_KEY, Context.MODE_PRIVATE)
        val currentAppVersion = BuildConfig.VERSION_CODE
        val lastAppVersionSynchronized = configPref.getInt(LAST_APP_VERSION_SYNCHRONIZED, 0)

        if(currentAppVersion > lastAppVersionSynchronized) {
            with(configPref.edit()) {
                putBoolean(INFORM_NEW_MINOR_APP_VERSION, false)
                putBoolean(INFORM_NEW_MAJOR_APP_VERSION, false)
                apply()
            }
        }
    }
}