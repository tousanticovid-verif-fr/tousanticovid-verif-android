package com.ingroupe.verify.anticovid.synchronization.elements

import android.content.Context
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncExpDate
import com.ingroupe.verify.anticovid.service.document.model.DocumentStaticDccResult

object Expiration {


    enum class TypeForExp(val text: String, val default: Boolean) {
        TEST("TEST", false),
        VACCINE("VACCINE", false),
        RECOVERY("RECOVERY", false),
        EXEMPTION("EXEMPTION", false)
    }


    fun saveExpirationActivation(
        syncExpDate: SyncExpDate?
        ) {
        val tutoPref =
            App.getContext().getSharedPreferences(Constants.EXPIRATION_KEY, Context.MODE_PRIVATE)

        with(tutoPref.edit()) {
            syncExpDate?.forTest?.let { putBoolean(TypeForExp.TEST.text, it) }
            syncExpDate?.forVaccine?.let { putBoolean(TypeForExp.VACCINE.text, it) }
            syncExpDate?.forRecovery?.let { putBoolean(TypeForExp.RECOVERY.text, it) }
            syncExpDate?.forExemption?.let { putBoolean(TypeForExp.EXEMPTION.text, it) }
            apply()
        }
    }

    fun useExpirationDate(dccType: DocumentStaticDccResult.DccType): Boolean {
        return when(dccType) {
            DocumentStaticDccResult.DccType.DCC_TEST ->  useExpirationDateForType(TypeForExp.TEST)
            DocumentStaticDccResult.DccType.DCC_VACCINATION ->  useExpirationDateForType(TypeForExp.VACCINE)
            DocumentStaticDccResult.DccType.DCC_RECOVERY ->  useExpirationDateForType(TypeForExp.RECOVERY)
            DocumentStaticDccResult.DccType.DCC_EXEMPTION ->  useExpirationDateForType(TypeForExp.EXEMPTION)
            DocumentStaticDccResult.DccType.DCC_ACTIVITY -> true
        }
    }

    private fun useExpirationDateForType(type: TypeForExp) : Boolean {
        val expPref = App.getContext().getSharedPreferences(Constants.EXPIRATION_KEY, Context.MODE_PRIVATE)
        return expPref?.getBoolean(type.text, type.default) ?: type.default
    }
}