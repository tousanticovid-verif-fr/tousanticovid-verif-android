package com.ingroupe.verify.anticovid.ui.actionchoice.countrypicker

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.model.Country
import io.reactivex.rxjava3.subjects.PublishSubject

class CountryRecyclerAdapter(
    private val context: Context,
    private var countries: List<Country>
) : RecyclerView.Adapter<CountryRecyclerAdapter.CountryHolder>() {
    val clickSubject: PublishSubject<Country> = PublishSubject.create()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryHolder {
        return CountryHolder(
            LayoutInflater.from(context).inflate(R.layout.country_cell, parent, false)
        )
    }

    override fun getItemCount() = countries.size

    override fun onBindViewHolder(holder: CountryHolder, position: Int) {
        val currentCountry = countries[position]

        holder.textViewName.text = currentCountry.name
        holder.imageViewFlag.setImageResource(currentCountry.flagId)
        setFavVisibility(currentCountry, holder)
    }

    private fun setFavVisibility(
        currentCountry: Country,
        holder: CountryHolder
    ) {
        if (currentCountry.isFavorite) {
            holder.imageViewFav.visibility = View.VISIBLE
        } else {
            holder.imageViewFav.visibility = View.GONE
        }
    }

    fun updateList(filteredList: List<Country>) {
        countries = filteredList
        notifyDataSetChanged()
    }

    inner class CountryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                clickSubject.onNext(countries[layoutPosition])
            }
        }

        val textViewName: TextView = itemView.findViewById<View>(R.id.textviewName) as TextView
        val imageViewFlag: ImageView = itemView.findViewById<View>(R.id.imageviewFlag) as ImageView
        val imageViewFav: ImageView = itemView.findViewById<View>(R.id.imageViewFav) as ImageView
    }

}