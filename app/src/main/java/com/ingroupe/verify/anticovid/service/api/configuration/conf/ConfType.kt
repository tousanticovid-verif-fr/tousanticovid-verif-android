package com.ingroupe.verify.anticovid.service.api.configuration.conf

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConfType(
    @SerializedName("resourceType")
    var resourceType: String? = null,
    @SerializedName("groups")
    var groups: ArrayList<ConfGroup>? = null,
    @SerializedName("information")
    var information: ConfInfo? = null
) : Serializable