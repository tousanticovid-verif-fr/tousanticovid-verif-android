package com.ingroupe.verify.anticovid.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.DialogFragment
import com.ingroupe.verify.anticovid.R


/**
 * Shows OK/Cancel confirmation dialog about camera permission.
 */
class CameraDeniedDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val parent = parentFragment

        return AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
            .setMessage(R.string.request_permission_camera_refused)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val activity = parent!!.activity
                activity?.finish()
            }
            .create()
    }
}