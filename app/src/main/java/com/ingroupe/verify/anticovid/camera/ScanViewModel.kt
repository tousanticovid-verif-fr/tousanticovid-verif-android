/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ingroupe.verify.anticovid.camera

import android.os.Handler
import android.os.Looper
import androidx.annotation.MainThread
import androidx.lifecycle.ViewModel
import com.scandit.datacapture.barcode.capture.BarcodeCapture
import com.scandit.datacapture.barcode.capture.BarcodeCaptureListener
import com.scandit.datacapture.barcode.capture.BarcodeCaptureSession
import com.scandit.datacapture.barcode.capture.BarcodeCaptureSettings
import com.scandit.datacapture.barcode.data.Barcode
import com.scandit.datacapture.barcode.data.Symbology
import com.scandit.datacapture.core.capture.DataCaptureContext
import com.scandit.datacapture.core.data.FrameData
import com.scandit.datacapture.core.source.FrameSourceState

class ScanViewModel : ViewModel(), BarcodeCaptureListener {
    private val dataCaptureManager: DataCaptureManager = DataCaptureManager.CURRENT
    private var listener: ResultListener? = null

    // The barcode capturing process is configured through barcode capture settings
    // which are then applied to the barcode capture instance that manages barcode recognition.
    private val barcodeCaptureSettings: BarcodeCaptureSettings
        get() {
            // The barcode capturing process is configured through barcode capture settings
            // which are then applied to the barcode capture instance that manages barcode recognition.
            val barcodeCaptureSettings = BarcodeCaptureSettings()

            // The settings instance initially has all types of barcodes (symbologies) disabled.
            // For the purpose of this sample we enable a very generous set of symbologies.
            // In your own app ensure that you only enable the symbologies that your app requires
            // as every additional enabled symbology has an impact on processing times.
            barcodeCaptureSettings.enableSymbology(Symbology.QR, true)
            barcodeCaptureSettings.enableSymbology(Symbology.DATA_MATRIX, true)
            return barcodeCaptureSettings
        }

    val dataCaptureContext: DataCaptureContext
        get() = dataCaptureManager.dataCaptureContext
    val barcodeCapture: BarcodeCapture
        get() = dataCaptureManager.barcodeCapture

    fun setListener(listener: ResultListener?) {
        this.listener = listener
    }

    fun resumeScanning() {
        dataCaptureManager.barcodeCapture.isEnabled = true
    }

    fun resumeScanningDelayed() {
        Handler(Looper.getMainLooper()).postDelayed({
            dataCaptureManager.barcodeCapture.isEnabled = true
        }, 1500)
    }

    fun pauseScanning() {
        dataCaptureManager.barcodeCapture.isEnabled = false
    }

    fun startFrameSource() {
        dataCaptureManager.camera?.switchToDesiredState(FrameSourceState.ON)
    }

    fun stopFrameSource() {
        dataCaptureManager.camera?.switchToDesiredState(FrameSourceState.OFF)
    }

    override fun onBarcodeScanned(
        barcodeCapture: BarcodeCapture,
        session: BarcodeCaptureSession,
        data: FrameData
    ) {
        if (listener != null && session.newlyRecognizedBarcodes.isNotEmpty()) {
            val firstBarcode = session.newlyRecognizedBarcodes[0]
            // Stop recognizing barcodes for as long as we are displaying the result.
            // There won't be any new results until the capture mode is enabled again.
            // Note that disabling the capture mode does not stop the camera, the camera
            // continues to stream frames until it is turned off.
            pauseScanning()

            // This method is invoked on a non-UI thread, so in order to perform UI work,
            // we have to switch to the main thread.
            Handler(Looper.getMainLooper()).post {
                listener?.onCodeScanned(firstBarcode)
            }
        }
    }

    override fun onSessionUpdated(
        barcodeCapture: BarcodeCapture,
        session: BarcodeCaptureSession,
        data: FrameData
    ) {
    }

    override fun onObservationStarted(barcodeCapture: BarcodeCapture) {}
    override fun onObservationStopped(barcodeCapture: BarcodeCapture) {}
    interface ResultListener {
        @MainThread
        fun onCodeScanned(barcodeResult: Barcode?)
    }

    init {
        dataCaptureManager.barcodeCapture.applySettings(barcodeCaptureSettings)

        // Register self as a listener to get informed whenever a new barcode got recognized.
        dataCaptureManager.barcodeCapture.addListener(this)
    }
}