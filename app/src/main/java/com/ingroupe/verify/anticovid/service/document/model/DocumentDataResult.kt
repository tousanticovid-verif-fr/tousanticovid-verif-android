package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DocumentDataResult(
    @SerializedName("static")
    var static: DocumentStaticResult? = null,
    @SerializedName("dynamic")
    var dynamic: ArrayList<Map<String, String>>? = null
) : Serializable