package com.ingroupe.verify.anticovid.ui.actionchoice.countrypicker

import android.content.Context
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.CountryUtils
import com.ingroupe.verify.anticovid.common.model.Country
import com.scandit.datacapture.core.internal.sdk.extensions.toArrayList
import kotlin.collections.ArrayList

class CountryPickerPresenterImpl(
    val view: CountryPickerView
) : CountryPickerPresenter {

    companion object {
        const val TAG = "com.ingroupe.verify.anticovid.ui.actionchoice.countrypicker.CountryPickerPresenterImpl"
    }

    private var listFavorites : ArrayList<Country> = arrayListOf()

    override fun loadCountries(pickerType: Int) : List<Country>? {
        val sharedPref = App.getContext().getSharedPreferences(Constants.COUNTRIES_KEY, Context.MODE_PRIVATE)
        val controlZone = sharedPref.getString(Constants.CountriesConfiguration.CONTROL_ZONE.text, Constants.DEFAULT_COUNTRY_CODE)

        var countries : List<Country>? = CountryUtils.getCountries()

        countries = when (pickerType) {
            CountryPickerActivity.CODE_FAVORITES_DEPARTURE -> {
                countries?.filter { it.isDeparture == true }
            }
            CountryPickerActivity.CODE_FAVORITES_ARRIVAL -> {
                countries?.filter { it.isArrival == true }
            }
            else -> {
                countries?.filter { it.isNational }
            }
        }
        //Suppression du pays de contrôle de la liste
        countries = countries?.filter { it.nameCode != controlZone }

        return countries
    }

    override fun loadFavorites(countries: List<Country>, pickerType: Int) {
        if (pickerType != CountryPickerActivity.CODE_FAVORITES_DEPARTURE && pickerType != CountryPickerActivity.CODE_FAVORITES_ARRIVAL) {
            return
        }

        val sharedPref = App.getContext().getSharedPreferences(Constants.COUNTRIES_KEY, Context.MODE_PRIVATE)
        val key = if (pickerType == CountryPickerActivity.CODE_FAVORITES_DEPARTURE)
            Constants.CountriesConfiguration.DEPARTURE_FAVORITES.text
        else
            Constants.CountriesConfiguration.ARRIVAL_FAVORITES.text

        val setFavCountryCodes = sharedPref.getStringSet(key, setOf()) ?: setOf()

        //Be careful with the case management
        listFavorites = countries.filter { setFavCountryCodes.toArrayList().contains(it.nameCode) }.toArrayList()
        view.onFavoritesUpdated(listFavorites)
    }

    override fun onCountrySelected(country: Country, pickerType: Int) {
        if (pickerType == -1) { //Should never happen
            return
        }

        if (pickerType == CountryPickerActivity.CODE_FAVORITES_DEPARTURE || pickerType == CountryPickerActivity.CODE_FAVORITES_ARRIVAL) {
            when {
                listFavorites.contains(country) -> {
                    listFavorites.remove(country)
                    saveFavoritesInPrefs(pickerType)
                    view.onFavoritesUpdated(listFavorites)
                }
                listFavorites.size < Constants.MAX_FAVORITES -> {
                    listFavorites.add(country)
                    saveFavoritesInPrefs(pickerType)
                    view.onFavoritesUpdated(listFavorites)
                }
                else -> view.onMaxFavorites()
            }
        } else {
            saveControlZone(country.nameCode)
            view.onItemPicked()
        }
    }


    private fun saveControlZone(countryCode: String?) {
        val sharedPref = App.getContext().getSharedPreferences(Constants.COUNTRIES_KEY, Context.MODE_PRIVATE)
        sharedPref?.let {
            with(it.edit()) {
                putString(
                    Constants.CountriesConfiguration.CONTROL_ZONE.text,
                    countryCode
                )
                apply()
            }
        }
    }

    private fun saveFavoritesInPrefs(pickerType: Int) {
        val sharedPref = App.getContext().getSharedPreferences(Constants.COUNTRIES_KEY, Context.MODE_PRIVATE)
        val key = if (pickerType == CountryPickerActivity.CODE_FAVORITES_DEPARTURE)
            Constants.CountriesConfiguration.DEPARTURE_FAVORITES.text
        else
            Constants.CountriesConfiguration.ARRIVAL_FAVORITES.text

        sharedPref?.let {
            with(it.edit()) {
                putStringSet(
                    key,
                    getFavoritesAsSet()
                )
                apply()
            }
        }
    }

    private fun getFavoritesAsSet() : Set<String> {
        val setString = mutableSetOf<String>()
        listFavorites.map {
            setString.add(it.nameCode)
        }

        return setString
    }
}