package com.ingroupe.verify.anticovid.service.document.datamatrix.preparation

import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.service.document.datamatrix.DatamatrixModeInterface
import com.ingroupe.verify.anticovid.service.document.model.DocumentStatic2ddocResult
import java.time.LocalDate
import java.time.format.DateTimeFormatter

data class VaccinationPreparation(
    val valueLA: String,
    val vacDate: LocalDate
) {
    companion object {

        const val VACCINE_CYCLE_TE = "TE"

        fun getPreparation(
            static2ddoc: DocumentStatic2ddocResult,
            mappedDynamicData: MutableMap<String, String>
        ): VaccinationPreparation? {

            // Etat du cycle de vaccination
            val valueLA = DatamatrixModeInterface.getField(static2ddoc.message?.fields, "LA")

            val property = "@string/dynamic_L1LA.${valueLA}"
            val packageName: String = App.getContext().packageName
            val resId: Int = App.getContext().resources.getIdentifier(property, "string", packageName)
            if(resId != 0) {
                mappedDynamicData["LA"] = App.getContext().getString(resId)
            }

            // Date du dernier état du cycle de vaccination
            val valueL9 = DatamatrixModeInterface.getField(static2ddoc.message?.fields, "L9")

            return if(valueLA == null || valueL9 == null) {
                null
            } else {
                val vacDate = LocalDate.parse(valueL9, DateTimeFormatter.ofPattern("dd/MM/yyyy"))

                VaccinationPreparation(
                    valueLA,
                    vacDate
                )
            }
        }
    }
}