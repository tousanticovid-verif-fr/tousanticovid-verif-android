package com.ingroupe.verify.anticovid.dialog

import android.Manifest
import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.DialogFragment
import com.ingroupe.verify.anticovid.R


/**
 * Shows OK/Cancel confirmation dialog about camera permission.
 */
class CameraRequestDialog(
    private val requestPermissionLauncher: ActivityResultLauncher<String>
    ) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val parent = parentFragment
        return AlertDialog.Builder(ContextThemeWrapper(activity, R.style.AlertDialogCustom))
            .setMessage(R.string.request_permission_camera)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                requestPermissionLauncher.launch(Manifest.permission.CAMERA)
            }
            .setNegativeButton(
                android.R.string.cancel
            ) { _, _ ->
                val activity = parent!!.activity
                activity?.finish()
            }
            .create()
    }
}