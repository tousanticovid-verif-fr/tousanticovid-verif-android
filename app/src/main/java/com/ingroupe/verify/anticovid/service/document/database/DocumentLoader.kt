package com.ingroupe.verify.anticovid.service.document.database

import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.service.document.database.entity.ConfigBarcode2DDoc

object DocumentLoader {

    private var initialized = false

    private var configBarcode2DDoc: List<ConfigBarcode2DDoc>? = null

    fun loadAssets() {
        if(!initialized) {
            configBarcode2DDoc = Utils.loadFromAsset("document/config-barcode-2ddoc.json")
            initialized = true
        }
    }

    fun findConfigBarcodeByType(type: String?): ConfigBarcode2DDoc? {
        if(type == null) {
            return null
        }
        return configBarcode2DDoc?.find { it.documentTypeCode == type }
    }
}