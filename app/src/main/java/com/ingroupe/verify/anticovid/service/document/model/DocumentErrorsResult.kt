package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DocumentErrorsResult(
    @SerializedName("documentNumber")
    var documentNumber: String? = null,
    @SerializedName("error")
    var error: String? = null,
    @SerializedName("message")
    var message: String? = null
) : Serializable