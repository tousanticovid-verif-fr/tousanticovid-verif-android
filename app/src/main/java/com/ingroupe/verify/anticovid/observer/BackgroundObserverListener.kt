package com.ingroupe.verify.anticovid.observer

interface BackgroundObserverListener {

    fun hasComeBackFromBackground()
}