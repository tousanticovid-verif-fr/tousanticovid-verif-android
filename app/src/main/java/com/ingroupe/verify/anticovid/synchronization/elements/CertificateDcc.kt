package com.ingroupe.verify.anticovid.synchronization.elements

import android.content.Context
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.data.EngineManager
import dgca.verifier.app.decoder.base64ToX509Certificate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import java.security.cert.X509Certificate

object CertificateDcc {

    private val certificateDccMap: MutableMap<String, X509Certificate> = HashMap()

    fun saveDccCertificates(
        certificatesDCC: Map<String, String>?
    ) {
        // sécurité : si le back n'envoie pas assez de certificats -> on ne vide pas nos données
        certificatesDCC?.let { listCertifs ->
            if(listCertifs.size > 50) {
                runBlocking (Dispatchers.IO) {
                    EngineManager.certificatesRepository.saveCertificates(listCertifs)
                }
                certificateDccMap.clear()
            }
        }

    }

    fun getDccCertificate(keyCertificateId: String): X509Certificate? {

        val certificate = certificateDccMap[keyCertificateId]
        if(certificate != null) {
            return certificate
        }

        var cert = EngineManager.certificatesRepository.getCertificateWithKey(keyCertificateId)?.value

        //TODO Remove after a few releases (current : 2.0.0)
        val certPref = App.getContext().getSharedPreferences(Constants.CERTIFICATE_FILE_KEY, Context.MODE_PRIVATE)
        //Case where the user just installed the new version and didn't sync yet
        if (cert == null) {
            cert = certPref.getString(keyCertificateId, null)
        } else { //Case where the user just installed the new version and did a sync
            with(certPref.edit()) {
                clear()
                apply()
            }
        }

        cert?.let {
            try {
                val newCertificate = it.base64ToX509Certificate()
                checkNotNull(newCertificate) { return null }
                certificateDccMap[keyCertificateId] = newCertificate
                return newCertificate
            } catch (e: Exception) {
                return null
            }
        }
        return null
    }
}