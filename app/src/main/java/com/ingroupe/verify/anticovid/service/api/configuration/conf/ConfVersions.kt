package com.ingroupe.verify.anticovid.service.api.configuration.conf

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ConfVersions (
    @SerializedName("lastMajorVersion")
    var lastMajorVersion: String?,
    @SerializedName("lastMinorVersion")
    var lastMinorVersion: String?,
): Serializable
