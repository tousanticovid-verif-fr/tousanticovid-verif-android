package com.ingroupe.verify.anticovid.service.document

import android.content.Context
import android.util.Base64
import android.util.Log
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.service.document.enums.ErrorDocEnum
import com.ingroupe.verify.anticovid.service.barcode.Barcode2DDocService
import com.ingroupe.verify.anticovid.service.document.model.*
import com.ingroupe.verify.anticovid.synchronization.elements.CertificateDcc
import dgca.verifier.app.decoder.base45.DefaultBase45Service
import dgca.verifier.app.decoder.cbor.DefaultCborService
import dgca.verifier.app.decoder.cbor.GreenCertificateData
import dgca.verifier.app.decoder.compression.DefaultCompressorService
import dgca.verifier.app.decoder.cose.DefaultCoseService
import dgca.verifier.app.decoder.cose.VerificationCryptoService
import dgca.verifier.app.decoder.getJsonDataFromAsset
import dgca.verifier.app.decoder.model.ActivityDCC
import dgca.verifier.app.decoder.model.GreenCertificate
import dgca.verifier.app.decoder.model.VerificationResult
import dgca.verifier.app.decoder.prefixvalidation.DefaultPrefixValidationService
import dgca.verifier.app.decoder.schema.DefaultSchemaValidator
import dgca.verifier.app.decoder.services.X509
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime


class StaticDataService {

    companion object {
        const val TAG = "StaticDataService"

        private val headersUsed = arrayOf("01", "02", "07", "08")

        fun getBarcode2DDoc(encoded2DDoc: String): DocumentStatic2ddocResult? {
            try {
                val barcode2DDoc: DocumentStatic2ddocResult = Barcode2DDocService.getBarcode2DDoc(encoded2DDoc)
                if (DocumentStatic2ddocResult.barcode2DDocNotNull(barcode2DDoc)) {
                    addDocumentTypeToHeaderFields(barcode2DDoc)
                    removeHeaderFields(barcode2DDoc)
                    addSignatureTo2DDoc(barcode2DDoc)
                    return barcode2DDoc
                }
            } catch (e: Exception) {
                Log.e(TAG, ErrorDocEnum.ERR02.message, e)
            }
            return null
        }

        private fun addDocumentTypeToHeaderFields(barcode2DDoc: DocumentStatic2ddocResult) {
            val field = DocumentFieldResult()
            field.name = "00"
            field.label = "Type de document"
            field.value = barcode2DDoc.message!!.label
            barcode2DDoc.header!!.fields?.add(0, field)
        }
        private fun removeHeaderFields(barcode2DDoc: DocumentStatic2ddocResult) {
            barcode2DDoc.header?. let { zoneDto ->
                val newFields = arrayListOf<DocumentFieldResult>()
                zoneDto.fields?.forEach { field ->
                    if(field.name !in headersUsed) {
                        newFields.add(field)
                    }
                }
                zoneDto.fields = newFields
            }
        }

        private fun addSignatureTo2DDoc(barcode2DDocDto: DocumentStaticResult) {
            val signature = DocumentSignatureResult()
            signature.isValid = barcode2DDocDto.hasValidSignature
            signature.status = App.getContext().getString(if (barcode2DDocDto.hasValidSignature) R.string.result_valid else R.string.result_invalid)
            barcode2DDocDto.signature = signature
        }

        @ExperimentalUnsignedTypes
        fun getDcc(
            encodedDcc: String,
            context: Context,
            mappedDynamicData: MutableMap<String, String>,
            dccFormat: Constants.DccFormat,
            controlTimeOT: ZonedDateTime?
        ): DocumentStaticDccResult {

            val verificationResult = VerificationResult()
            val plainInput = DefaultPrefixValidationService(dccFormat.prefix).decode(encodedDcc, verificationResult)
            val compressedCose = DefaultBase45Service().decode(plainInput, verificationResult)
            val cose = checkNotNull(DefaultCompressorService().decode(compressedCose, verificationResult))
            val coseData = checkNotNull(DefaultCoseService().decode(cose, verificationResult))

            val jsonSchema = getJsonDataFromAsset(context, dccFormat.jsonSchemaAssetFileName) ?: ""

            DefaultSchemaValidator(jsonSchema).validate(coseData.cbor, verificationResult)
            if (!verificationResult.isSchemaValid) {
                mappedDynamicData["anomaly"] = context.getString(R.string.result_anomaly_dcc_schema)
            }

            val kid = checkNotNull(coseData.kid)
            val base64EncodedKid = Base64.encodeToString(kid, Base64.NO_WRAP)
            val greenCertificateData: GreenCertificateData = checkNotNull(DefaultCborService().decodeData(coseData.cbor, verificationResult))
            val greenCertificate: GreenCertificate = checkNotNull(greenCertificateData.greenCertificate)

            val certificate = CertificateDcc.getDccCertificate(base64EncodedKid)
            val signature = DocumentSignatureResult()
            if (certificate == null) {
                signature.isValid = false
                signature.status = context.getString(R.string.result_invalid)
            } else {
                val utcZoneId: ZoneId = ZoneId.ofOffset("", ZoneOffset.UTC).normalized()
                val expirationTime: ZonedDateTime? =
                    certificate.notAfter.toInstant().atZone(utcZoneId)
                val controlTime: ZonedDateTime = controlTimeOT?: ZonedDateTime.now().withZoneSameInstant(utcZoneId)
                if (expirationTime != null && controlTime.isAfter(expirationTime)) {
                    signature.isValid = false
                    signature.status = context.getString(R.string.result_anomaly_certificate_expired)
                } else {
                    VerificationCryptoService(X509()).validate(cose, certificate, verificationResult)
                    signature.isValid = verificationResult.coseVerified
                    signature.status = context.getString(if (verificationResult.coseVerified) R.string.result_valid else R.string.result_invalid)
                }
            }

            //Specific France for the Activity DCC
            if (DocumentStaticDccResult.getResourceType(greenCertificate) == DocumentStaticDccResult.DccType.DCC_ACTIVITY) {
                greenCertificate.activity = ActivityDCC(greenCertificateData.expirationTime, greenCertificateData.issuedAt)
            }

            val result = DocumentStaticDccResult(greenCertificateData, signature, base64EncodedKid, greenCertificateData.issuedAt, greenCertificateData.expirationTime)
            result.hasValidSignature = verificationResult.coseVerified
            return result
        }
    }
}