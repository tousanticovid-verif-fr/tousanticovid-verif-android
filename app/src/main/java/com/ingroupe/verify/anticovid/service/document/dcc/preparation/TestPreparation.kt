package com.ingroupe.verify.anticovid.service.document.dcc.preparation

import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.service.document.BarcodeService
import com.ingroupe.verify.anticovid.service.document.dcc.DccModeInterface
import com.ingroupe.verify.anticovid.service.document.model.InfoTestForStat
import dgca.verifier.app.decoder.model.Test
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

data class TestPreparation(
    val testDate: ZonedDateTime,
    val currentDate: OffsetDateTime,
    val manufacturerFound: Boolean,
    val isPCR: Boolean,
    val isAntigenic: Boolean
) {
    companion object {
        const val RESULT_TEST_NEGATIVE = "260415000"
        const val RESULT_TEST_POSITIVE = "260373001"

        fun getPreparation(
            test: Test,
            mappedDynamicData: MutableMap<String, String>,
            infoTestForStat: InfoTestForStat
        ): TestPreparation? {
            val date = DccModeInterface
                .getOffsetDateTimeFromDcc(test.dateTimeOfCollection)
                ?.atZoneSameInstant(ZoneId.systemDefault())
            val currentDate = OffsetDateTime.now()

            val manufacturerFound =
                test.testNameAndManufacturer != null
                        && BarcodeService.isValuePresent(
                    "testNameAndManufacturer",
                    test.testNameAndManufacturer!!,
                    Constants.SpecificList.TEST_MANUFACTURER,
                    mappedDynamicData
                )

            val isPCR = BarcodeService.isValuePresent(
                "typeOfTest",
                test.typeOfTest.trim(),
                Constants.SpecificList.TEST_PCR,
                mappedDynamicData
            )

            val isAntigenic = BarcodeService.isValuePresent(
                "typeOfTest",
                test.typeOfTest.trim(),
                Constants.SpecificList.TEST_ANTIGENIC,
                mappedDynamicData
            )

            infoTestForStat.isTestPositive = test.testResult.trim() == RESULT_TEST_NEGATIVE
            infoTestForStat.updateInfo(test.testingCentre)

            return if(date == null) {
                null
            } else {
                TestPreparation(
                    date,
                    currentDate,
                    manufacturerFound,
                    isPCR,
                    isAntigenic
                )
            }
        }
    }
}
