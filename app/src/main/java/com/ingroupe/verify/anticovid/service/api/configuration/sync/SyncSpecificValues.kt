package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SyncSpecificValues (
    @SerializedName("validity")
    var validity: SyncValidity? = null,
    @SerializedName("labels")
    var labels: SyncLabels? = null,
    @SerializedName("testPcrList")
    var testPcrList: Map<String, String>? = null,
    @SerializedName("testAntigenicList")
    var testAntigenicList: Map<String, String>? = null,
    @SerializedName("testManufacturerList")
    var testManufacturerList: Map<String, String>? = null,
    @SerializedName("vaccineMedicalProductList")
    var vaccineMedicalProductList: Map<String, String>? = null,
    @SerializedName("vaccineProphylaxisList")
    var vaccineProphylaxisList:  Map<String, String>? = null,
    @SerializedName("vaccineManufacturerList")
    var vaccineManufacturerList: Map<String, String>? = null,
    @SerializedName("blacklist")
    var blacklist: SyncBlacklist? = null,
    @SerializedName("useExpirationDate")
    var useExpirationDate: SyncExpDate? = null,
    @SerializedName("notGreenCountries")
    var notGreenCountries: List<String>? = null,
    @SerializedName("ruleEngine")
    var ruleEngine: SyncRuleEngine? = null
): Serializable