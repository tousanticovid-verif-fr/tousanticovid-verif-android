package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DocumentStaticFieldsResult(
    @SerializedName("fields")
    var fields: ArrayList<DocumentFieldResult>? = null,
    @SerializedName("error")
    var error: String? = null
) : Serializable {


    var label: String? = null
    var version: String? = null
    var type: String? = null
}