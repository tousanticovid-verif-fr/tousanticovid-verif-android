package com.ingroupe.verify.anticovid.data.local.blacklist

import com.ingroupe.verify.anticovid.service.api.configuration.blacklist.BlacklistElement
import com.ingroupe.verify.anticovid.synchronization.elements.Blacklist

class BlacklistDataSource(private val blacklistDao: BlacklistDao) {

    fun isHashBlacklisted(blacklistType: Blacklist.BlacklistType, hash: String): Boolean {
        val blacklistedHashes = blacklistDao.getHash(blacklistType.text, hash)
        return blacklistedHashes.maxByOrNull { it.id }?.active ?: false
    }

    fun addBlacklist(blacklistType: Blacklist.BlacklistType, blacklistElements: List<BlacklistElement>) {
        val blacklist: List<BlacklistLocal> = blacklistElements.toBlacklistLocals(blacklistType)
        blacklistDao.insertBlacklist(*blacklist.toTypedArray())
    }

    fun getLastBlacklist(blacklistType: Blacklist.BlacklistType): List<BlacklistLocal> {
        return blacklistDao.getLastBlacklist(blacklistType.text)
    }

    fun deleteFromId(blacklistType: Blacklist.BlacklistType, id: Int) {
        blacklistDao.deleteFromId(blacklistType.text, id)
    }
}