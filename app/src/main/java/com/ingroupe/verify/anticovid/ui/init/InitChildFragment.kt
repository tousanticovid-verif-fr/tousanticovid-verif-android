package com.ingroupe.verify.anticovid.ui.init

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.FeatureChildFragment
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.databinding.InitMainBinding
import com.ingroupe.verify.anticovid.databinding.PopupCameraRequestBinding
import com.ingroupe.verify.anticovid.databinding.PopupUserPolicyAndCguBinding
import com.ingroupe.verify.anticovid.ui.actionchoice.ActionChoiceChildFragment


class InitChildFragment : FeatureChildFragment(), InitView {
    override fun getTitle(): String = "Initialisation"
    override fun getTitleId(): Int = R.string.title_init

    private var presenter : InitPresenter? = null

    companion object {
        const val TAG = "init"
        fun newInstance() = InitChildFragment()
        const val TIMEOUT = 500L //Time to launch the next fragment
    }
    private var _binding: InitMainBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    private lateinit var model: SharedViewModel
    private var requestPermissionLauncher: ActivityResultLauncher<String> =
        registerForActivityResult(ActivityResultContracts.RequestPermission()
    ){ isGranted: Boolean ->
        if (isGranted) {
            goToNextPage()
        } else {
            requestCameraPermission()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = InitMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //si on se déconnecte, la configuration est enlevée
        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        model.configuration = null

        if (presenter == null) {
            presenter = context?.let { InitPresenterImpl(this) }
        }

        presenter?.loadAssets(model)
        presenter?.initEngineIfNeeded()
    }

    private fun requestCameraPermission() {
        Utils.requestCameraPermission(
            requireActivity(),
            childFragmentManager,
            "dialogInit",
            requestPermissionLauncher
        )
    }

    private fun checkCameraPermission() {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            val bindingDialog = PopupCameraRequestBinding.inflate(LayoutInflater.from(context))
            val dialog = AlertDialog.Builder(ContextThemeWrapper(activity,
                R.style.AlertDialogCustom
            ))
                .setView(bindingDialog.root)
                .create()

            dialog.setOnDismissListener {
                Utils.requestCameraPermission(requireActivity(), childFragmentManager, "dialogInit", requestPermissionLauncher)
            }
            dialog.show()

            bindingDialog.buttonAccept.setOnClickListener {
                dialog.dismiss()
            }

        } else {
            goToNextPage()
        }
    }

    private fun showUserPolicyAndCguPopup() {
        val settingsPref =
            App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
        val alreadyAsked = settingsPref?.getBoolean(Constants.SavedItems.INFO_CGU_POLICY_SHOWN_V2.text, false) ?: false

        if (settingsPref != null && !alreadyAsked) {

            val bindingDialog = PopupUserPolicyAndCguBinding.inflate(LayoutInflater.from(context))

            val dialog = AlertDialog.Builder(ContextThemeWrapper(activity,
                    R.style.AlertDialogCustom
            ))
                    .setView(bindingDialog.root)
                    .create()

            dialog.setOnDismissListener {
                checkCameraPermission()
            }
            dialog.show()

            context?.let { c ->
                Utils.addLinktoTextView(bindingDialog.textViewLegal2, R.string.info_legal_2, R.string.info_legal_2_link, c)
                Utils.addLinktoTextView(bindingDialog.textViewLegal4, R.string.info_legal_4, R.string.info_legal_4_link, c)
            }

            bindingDialog.buttonAccept.setOnClickListener {
                closeUserPolicyAndCguAskDialog(settingsPref, dialog)
            }

            model.configuration?.confAbout.let { conf ->
                bindingDialog.textViewConfidentialityLinkPolicy.setOnClickListener {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(conf?.privacyPolicy))
                    startActivity(browserIntent)
                }
                bindingDialog.textViewConfidentialityLinkCgu.setOnClickListener {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(conf?.cgu))
                    startActivity(browserIntent)
                }
            }
            val contentPolConf = SpannableString(bindingDialog.textViewConfidentialityLinkPolicy.text)
            contentPolConf.setSpan(UnderlineSpan(), 0, contentPolConf.length, 0)
            bindingDialog.textViewConfidentialityLinkPolicy.text = contentPolConf

            val contentCgu = SpannableString(bindingDialog.textViewConfidentialityLinkCgu.text)
            contentCgu.setSpan(UnderlineSpan(), 0, contentCgu.length, 0)
            bindingDialog.textViewConfidentialityLinkCgu.text = contentCgu

        } else {
            checkCameraPermission()
        }
    }

    private fun closeUserPolicyAndCguAskDialog(sharedPref: SharedPreferences, dialog: AlertDialog?) {
        with(sharedPref.edit()) {
            putBoolean(
                Constants.SavedItems.INFO_CGU_POLICY_SHOWN_V2.text,
                true
            )
            apply()
        }
        dialog?.dismiss()
    }

    private fun goToNextPage() {
        Handler(Looper.getMainLooper()).postDelayed({
            goToActionChoice()
        }, TIMEOUT)
    }


    private fun goToActionChoice() {
        featureFragment?.replaceFragment(ActionChoiceChildFragment.TAG)
    }


    override fun showNavigation(): MainActivity.NavigationIcon {
        return MainActivity.NavigationIcon.NO_MENU
    }

    override fun setupFinished() {
        context?.let { c ->
            (c as MainActivity).onEngineInitDone()
        }
        showUserPolicyAndCguPopup()
    }

    override fun onPause() {
        super.onPause()
        presenter?.tearDown()
    }
}