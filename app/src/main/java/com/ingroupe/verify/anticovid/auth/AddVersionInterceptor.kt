package com.ingroupe.verify.anticovid.auth

import android.os.Build
import com.ingroupe.verify.anticovid.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class AddVersionInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()

        builder.addHeader("app_version", "and_" + BuildConfig.VERSION_CODE)
        builder.addHeader("os_version", "and_" + Build.VERSION.SDK_INT)
        builder.addHeader("app_mode", if(JWTUtils.isPremium()) "psot" else "public")

        return chain.proceed(builder.build())
    }
}