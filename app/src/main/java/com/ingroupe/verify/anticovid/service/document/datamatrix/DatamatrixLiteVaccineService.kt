package com.ingroupe.verify.anticovid.service.document.datamatrix

import android.content.Context
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.Constants.ValidityValues.*
import com.ingroupe.verify.anticovid.common.Constants.ValidityValuesString.VACCINE_BOOSTER_AGE_PERIOD
import com.ingroupe.verify.anticovid.service.document.BarcodeService
import com.ingroupe.verify.anticovid.service.document.datamatrix.preparation.VaccinationPreparation
import com.ingroupe.verify.anticovid.service.document.model.DocumentStatic2ddocResult
import java.time.LocalDate
import java.time.Period
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

object DatamatrixLiteVaccineService : DatamatrixModeInterface {

    override fun get2ddocTest(
        static2ddoc: DocumentStatic2ddocResult,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = Constants.GlobalValidity.KO.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = context.getString(R.string.result_invalid)
    }

    override fun get2ddocVaccin(
        static2ddoc: DocumentStatic2ddocResult,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        var validityGlobal: String = Constants.GlobalValidity.KO.text
        var validityGlobalStatus: String = context.getString(R.string.result_invalid)

        if (!static2ddoc.hasValidSignature) {
            validityGlobal = Constants.GlobalValidity.KO.text
            validityGlobalStatus = context.getString(R.string.invalid_2ddoc_signature)
        } else {
            VaccinationPreparation.getPreparation(static2ddoc, mappedDynamicData)?.let { prep ->

                // Date de naissance
                val valueL2 = DatamatrixModeInterface.getField(static2ddoc.message?.fields, "L2")
                // Nom du vaccin
                val valueL5 = DatamatrixModeInterface.getField(static2ddoc.message?.fields, "L5")
                // Nombre de doses attendues pour un cycle complet
                val valueL8 = DatamatrixModeInterface.getField(static2ddoc.message?.fields, "L8")

                if (valueL2 == null || valueL5 == null || valueL8 == null) {
                    validityGlobal = Constants.GlobalValidity.KO.text
                    validityGlobalStatus = context.getString(R.string.invalid_2ddoc_signature)
                } else {
                    val currentDate = LocalDate.now()

                    val dob = LocalDate.parse(valueL2, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                    val dobAfterPeriod = if (dob == null) {
                        currentDate.plusDays(1)
                    } else {
                        dob.plus(Period.parse(getSVS(VACCINE_BOOSTER_AGE_PERIOD)))
                    }

                    val nbWantedDose = valueL8.toInt()

                    if (prep.valueLA == VaccinationPreparation.VACCINE_CYCLE_TE
                        && (nbWantedDose == 1
                            && valueL5.uppercase().contains("JANSSEN")
                            && !currentDate.isBefore(
                                prep.vacDate.plusDays(getSV(VACCINE_DELAY_JANSSEN_1).toLong())
                            )
                            && !currentDate.isAfter(
                                prep.vacDate.plusDays(getSV(VACCINE_DELAY_MAX_JANSSEN_1).toLong())
                            )
                        )
                    ) {
                        validityGlobal = Constants.GlobalValidity.OK.text
                        validityGlobalStatus = context.getString(R.string.result_valid)

                    } else if (prep.valueLA == VaccinationPreparation.VACCINE_CYCLE_TE
                        && currentDate < dobAfterPeriod
                        && (
                            (nbWantedDose == 1
                                && !valueL5.uppercase().contains("JANSSEN")
                                && !currentDate.isBefore(
                                    prep.vacDate.plusDays(
                                        getSV(VACCINE_DELAY).toLong()
                                    )
                                )
                            )
                            || (nbWantedDose == 2
                                && !currentDate.isBefore(
                                    prep.vacDate.plusDays(
                                        getSV(VACCINE_DELAY).toLong()
                                    )
                                )
                            )
                            || (nbWantedDose >= 3
                                && !currentDate.isBefore(
                                    prep.vacDate.plusDays(
                                        getSV(VACCINE_BOOSTER_DELAY_UNDER_AGE_NEW).toLong()
                                    )
                                )
                            )
                        )
                    ) {
                        validityGlobal = Constants.GlobalValidity.OK.text
                        validityGlobalStatus = context.getString(R.string.result_valid)

                    } else if (prep.valueLA == VaccinationPreparation.VACCINE_CYCLE_TE
                        && currentDate >= dobAfterPeriod
                        && (
                            (nbWantedDose == 1
                                && !valueL5.uppercase().contains("JANSSEN")
                                && !currentDate.isBefore(
                                    prep.vacDate.plusDays(
                                        getSV(VACCINE_DELAY).toLong()
                                    )
                                )
                                && !currentDate.isAfter(
                                    prep.vacDate.plusDays(
                                        getSV(VACCINE_DELAY_MAX_RECOVERY).toLong()
                                    )
                                )
                            )
                            || (nbWantedDose == 2
                                && !currentDate.isBefore(
                                    prep.vacDate.plusDays(
                                        getSV(VACCINE_DELAY).toLong()
                                    )
                                )
                                && !currentDate.isAfter(
                                    prep.vacDate.plusDays(
                                        getSV(VACCINE_DELAY_MAX).toLong()
                                    )
                                )
                            )
                            || (nbWantedDose >= 3
                                && !currentDate.isBefore(
                                    prep.vacDate.plusDays(
                                        getSV(VACCINE_BOOSTER_DELAY_NEW).toLong()
                                    )
                                )
                                && !currentDate.isAfter(
                                    prep.vacDate.plusDays(
                                        getSV(VACCINE_BOOSTER_DELAY_MAX).toLong()
                                    )
                                )
                            )
                        )
                    ) {
                        validityGlobal = Constants.GlobalValidity.OK.text
                        validityGlobalStatus = context.getString(R.string.result_valid)

                    } else {
                        validityGlobal = Constants.GlobalValidity.KO.text
                        validityGlobalStatus = context.getString(R.string.result_invalid)
                    }
                }
            }
        }

        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }

    private fun getSV(svEnum : Constants.ValidityValues): Int {
        return BarcodeService.getVV(svEnum, Constants.ControlMode.VACCINE)
    }

    private fun getSVS(svEnum : Constants.ValidityValuesString): String {
        return BarcodeService.getVVS(svEnum, Constants.ControlMode.VACCINE)
    }
}