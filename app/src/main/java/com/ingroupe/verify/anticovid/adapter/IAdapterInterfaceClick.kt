package com.ingroupe.verify.anticovid.adapter

interface IAdapterInterfaceClick {
    fun onItemClick(position: Int)
}