package com.ingroupe.verify.anticovid.service.document.dcc

import android.content.Context
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.service.barcode.OperationService
import com.ingroupe.verify.anticovid.service.document.BarcodeService
import com.ingroupe.verify.anticovid.service.document.dcc.preparation.*
import com.ingroupe.verify.anticovid.service.document.model.DocumentStaticDccResult
import com.ingroupe.verify.anticovid.service.document.model.InfoTestForStat
import java.time.OffsetDateTime
import java.time.Period
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

object DccPremiumService : DccModeInterface {


    override fun getDccTest(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?,
        infoTestForStat: InfoTestForStat
    ) {
        var validityGlobal = Constants.GlobalValidity.KO
        var validityGlobalStatus = context.getString(R.string.result_invalid)

        staticDcc?.dccData?.greenCertificate?.tests?.get(0)?.let { test ->
            TestPreparation.getPreparation(test, mappedDynamicData, infoTestForStat)?.let { prep ->

                val zonedControlTime = controlTimeOT ?: prep.currentDate.toZonedDateTime()
                val offsetControlDateTime = controlTimeOT ?: prep.currentDate

                if (!staticDcc.hasValidSignature) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_signature)
                } else if (!DccModeInterface.checkIat(
                        staticDcc.issuedAt,
                        zonedControlTime
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_iat_future)
                } else if (!DccModeInterface.checkExp(
                        staticDcc.expirationTime,
                        DocumentStaticDccResult.DccType.DCC_TEST,
                        zonedControlTime
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_expired)
                } else if (!prep.isPCR && !prep.isAntigenic) {
                    validityGlobal = Constants.GlobalValidity.KO_RULE
                    validityGlobalStatus =
                        context.getString(R.string.result_test_type_not_valid)
                } else if (prep.isPCR) {

                    if (test.testResult.trim() == TestPreparation.RESULT_TEST_NEGATIVE) {
                        validityGlobal = Constants.GlobalValidity.INFO
                        validityGlobalStatus =
                            context.getString(R.string.result_test_pcr_negative_info)
                    } else if (test.testResult.trim() == TestPreparation.RESULT_TEST_POSITIVE
                    ) {
                        validityGlobal = Constants.GlobalValidity.INFO
                        validityGlobalStatus =
                            context.getString(R.string.result_test_pcr_positive_info)
                    }

                } else if (prep.isAntigenic) {

                    if (prep.manufacturerFound) {

                        if (test.testResult.trim() == TestPreparation.RESULT_TEST_NEGATIVE) {
                            validityGlobal = Constants.GlobalValidity.INFO
                            validityGlobalStatus =
                                context.getString(R.string.result_test_antigenic_negative_info)
                        } else if (test.testResult.trim() == TestPreparation.RESULT_TEST_POSITIVE) {
                            validityGlobal = Constants.GlobalValidity.INFO
                            validityGlobalStatus =
                                context.getString(R.string.result_test_antigenic_positive_info)
                        }
                    } else {
                        if (test.testResult.trim() == TestPreparation.RESULT_TEST_NEGATIVE) {
                            validityGlobal = Constants.GlobalValidity.INFO
                            validityGlobalStatus =
                                context.getString(R.string.result_test_antigenic_negative_fr_info)
                        } else if (test.testResult.trim() == TestPreparation.RESULT_TEST_POSITIVE) {
                            validityGlobal = Constants.GlobalValidity.INFO
                            validityGlobalStatus =
                                context.getString(R.string.result_test_antigenic_positive_fr_info)
                        }
                    }
                }

                var tempDateTime = OffsetDateTime.from(prep.testDate)
                val hours = tempDateTime.until(offsetControlDateTime, ChronoUnit.HOURS)
                tempDateTime = tempDateTime.plusHours(hours)
                val minutes =
                    tempDateTime.until(offsetControlDateTime, ChronoUnit.MINUTES)

                val samplingDuration =
                    if (ChronoUnit.HOURS.between(prep.testDate, offsetControlDateTime) <= 96
                    ) {
                        context.getString(
                            R.string.result_duration_hours_minutes,
                            hours,
                            minutes
                        )
                    } else {
                        context.getString(
                            R.string.result_duration_days_hours,
                            hours / 24,
                            hours % 24
                        )
                    }
                mappedDynamicData["samplingDuration"] = samplingDuration

                DccModeInterface.getString(
                    "testResult",
                    test.testResult.trim(),
                    context,
                    mappedDynamicData
                )
                DccModeInterface.getString(
                    "testType",
                    test.typeOfTest.trim(),
                    context,
                    mappedDynamicData
                )
                DccModeInterface.getString("disease", test.disease, context, mappedDynamicData)
                mappedDynamicData["countryOfVaccination"] =
                    OperationService.getCountry(test.countryOfVaccination)


            }
        }
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }


    override fun getDccVaccination(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        var validityGlobal = Constants.GlobalValidity.KO
        var validityGlobalStatus = context.getString(R.string.result_invalid)

        staticDcc?.dccData?.greenCertificate?.vaccinations?.get(0)?.let { vac ->
            VaccinationPreparation.getPreparation(vac, mappedDynamicData)?.let { prep ->

                val zonedControlTime = controlTimeOT ?: ZonedDateTime.now()
                val localControlDate = controlTimeOT?.toLocalDate() ?: prep.currentDate

                if (!staticDcc.hasValidSignature) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_signature)
                } else if (!DccModeInterface.checkIat(
                        staticDcc.issuedAt, zonedControlTime
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_iat_future)
                } else if (!DccModeInterface.checkExp(
                        staticDcc.expirationTime,
                        DocumentStaticDccResult.DccType.DCC_VACCINATION,
                        zonedControlTime
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_expired)
                } else if (vac.doseNumber >= vac.totalSeriesOfDoses
                    && prep.medicinalProductFound
                    && prep.manufacturerFound
                    && prep.vaccineFound
                ) {
                    validityGlobal = Constants.GlobalValidity.INFO
                    validityGlobalStatus =
                        context.getString(R.string.result_vaccine_cycle_completed_info)
                } else {
                    validityGlobal = Constants.GlobalValidity.KO_RULE
                    validityGlobalStatus =
                        context.getString(R.string.result_vaccine_cycle_improper)
                }

                val period = Period.between( prep.vacDate, localControlDate)
                val months = period.months + period.years * 12
                val duration = if (months > 0) {

                    context.getString(
                        R.string.result_duration_months_days,
                        months,
                        period.days
                    )
                } else {
                    context.getString(
                        R.string.result_duration_days,
                        period.days
                    )
                }
                mappedDynamicData["vaccinationDuration"] = duration

                mappedDynamicData["currentDoseCount"] =
                    "${vac.doseNumber} / ${vac.totalSeriesOfDoses}"

                DccModeInterface.getString("disease", vac.disease, context, mappedDynamicData)
                mappedDynamicData["countryOfVaccination"] =
                    OperationService.getCountry(vac.countryOfVaccination)
            }
        }

        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }

    override fun getDccRecovery(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?,
        infoTestForStat: InfoTestForStat
    ) {

        var validityGlobal = Constants.GlobalValidity.KO
        var validityGlobalStatus = context.getString(R.string.result_invalid)

        staticDcc?.dccData?.greenCertificate?.recoveryStatements?.get(0)?.let { statement ->
            RecoveryPreparation.getPreparation(statement, infoTestForStat)?.let { prep ->

                val zonedControlTime = controlTimeOT ?: ZonedDateTime.now()
                val localControlDate = controlTimeOT?.toLocalDate() ?: prep.currentDate

                if (!staticDcc.hasValidSignature) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_signature)
                } else if (!DccModeInterface.checkIat(
                        staticDcc.issuedAt,zonedControlTime
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_iat_future)
                } else if (!DccModeInterface.checkExp(
                        staticDcc.expirationTime,
                        DocumentStaticDccResult.DccType.DCC_RECOVERY,
                        zonedControlTime
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_expired)
                } else if (ChronoUnit.DAYS.between(
                        prep.PositiveTestDate, localControlDate
                    ) in BarcodeService.getVV(
                        Constants.ValidityValues.RECOVERY_START_DAY, Constants.ControlMode.PLUS
                    )..BarcodeService.getVV(Constants.ValidityValues.RECOVERY_END_DAY, Constants.ControlMode.PLUS)
                ) {
                    validityGlobal = Constants.GlobalValidity.INFO
                    validityGlobalStatus = context.getString(R.string.result_recovery_info)
                } else {
                    validityGlobal = Constants.GlobalValidity.KO_RULE
                    validityGlobalStatus = context.getString(R.string.result_recovery_invalid)
                }

                val period =
                    Period.between(prep.PositiveTestDate, localControlDate)
                val months = period.months + period.years * 12
                val duration = if (months > 0) {
                    context.getString(
                        R.string.result_duration_months_days,
                        months,
                        period.days
                    )
                } else {
                    context.getString(
                        R.string.result_duration_days,
                        period.days
                    )
                }
                mappedDynamicData["samplingDuration"] = duration

                DccModeInterface.getString(
                    "disease",
                    statement.disease,
                    context,
                    mappedDynamicData
                )
                mappedDynamicData["countryOfVaccination"] =
                    OperationService.getCountry(statement.countryOfVaccination)


            }
        }
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }

    override fun getDccExemption(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        var validityGlobal = Constants.GlobalValidity.KO
        var validityGlobalStatus = context.getString(R.string.result_invalid)

        staticDcc?.dccData?.greenCertificate?.exemption?.let { exemption ->
            ExemptionPreparation.getPreparation(exemption)?.let { _ ->

                val zonedControlTime = controlTimeOT ?: ZonedDateTime.now()

                if (!staticDcc.hasValidSignature) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_signature)
                } else if (!DccModeInterface.checkIat(
                        staticDcc.issuedAt, zonedControlTime
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_iat_future)
                } else if (!DccModeInterface.checkExp(
                        staticDcc.expirationTime,
                        DocumentStaticDccResult.DccType.DCC_EXEMPTION,
                        zonedControlTime
                    )
                ) {
                    validityGlobal = Constants.GlobalValidity.KO
                    validityGlobalStatus = context.getString(R.string.invalid_dcc_expired)
                } else {
                    validityGlobal = Constants.GlobalValidity.INFO
                    validityGlobalStatus = context.getString(R.string.result_exemption_fr_info)
                }
                DccModeInterface.getString(
                    "disease",
                    exemption.disease,
                    context,
                    mappedDynamicData
                )
                mappedDynamicData["countryOfVaccination"] =
                    OperationService.getCountry(exemption.countryOfVaccination)

                DccModeInterface.getString(
                    "exemptionStatus",
                    exemption.exemptionStatus,
                    context,
                    mappedDynamicData
                )


            }
        }
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = validityGlobal.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = validityGlobalStatus
    }

    override fun getDccActivity(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    ) {
        mappedDynamicData[Constants.VALIDITY_GLOBAL_FIELD] = Constants.GlobalValidity.KO.text
        mappedDynamicData[Constants.VALIDITY_STATUS_FIELD] = context.getString(R.string.result_invalid)
    }
}