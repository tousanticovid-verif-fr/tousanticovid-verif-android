package com.ingroupe.verify.anticovid.service.document.model

import com.google.gson.annotations.SerializedName

class DocumentStatic2ddocResult(
    @SerializedName("header")
    var header: DocumentStaticFieldsResult? = null,
    @SerializedName("message")
    var message: DocumentStaticFieldsResult? = null,
    @SerializedName("annexe")
    var annexe: DocumentStaticFieldsResult? = null,
    signature: DocumentSignatureResult? = null

) : DocumentStaticResult(signature) {

    companion object {
        fun barcode2DDocNotNull(barcode2DDoc: DocumentStatic2ddocResult?): Boolean {
            return barcode2DDoc?.header?.fields != null
                    && barcode2DDoc.message?.type != null
                    && barcode2DDoc.message?.label != null
                    && barcode2DDoc.message?.fields != null
        }
    }
}