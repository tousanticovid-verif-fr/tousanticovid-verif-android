package com.ingroupe.verify.anticovid.observer

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.ingroupe.verify.anticovid.MainActivity

class LocaleChangedBroadcastReceiver : BroadcastReceiver() {

    companion object {
        private const val TAG = "LocaleChangedBr"
    }

    var main: MainActivity? = null
    fun setMainActivityHandler(main: MainActivity?) {
        this.main = main
    }

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "new Locale")
        main?.reloadConfiguration()
    }
}