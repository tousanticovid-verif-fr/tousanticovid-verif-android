package com.ingroupe.verify.anticovid.service.document.model

class InfoTestForStat {
    var isTestPositive: Boolean? = null
    var info: String? = null

    fun updateInfo(field: String) {
        info = when {
            field.startsWith("PV") -> "PV"
            field.startsWith("TV") -> "TV"
            field.startsWith("NV") -> "NV"
            else -> null
        }
    }
}