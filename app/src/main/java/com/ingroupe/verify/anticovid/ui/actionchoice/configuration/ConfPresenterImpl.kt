package com.ingroupe.verify.anticovid.ui.actionchoice.configuration

import android.content.Context
import android.content.SharedPreferences
import androidx.annotation.VisibleForTesting
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.common.model.TravelConfiguration
import java.time.ZonedDateTime

class ConfPresenterImpl(private val model: SharedViewModel, private val view: ConfView) : ConfPresenter {

    private var sharedPrefs : SharedPreferences? = null
        get() {
            if (field == null) {
                field = App.getContext().getSharedPreferences(Constants.COUNTRIES_KEY, Context.MODE_PRIVATE)
            }
            return field
        }

    override fun initTravelType(type: Constants.TravelType) {
        val pickedCountry = getControlCountryAndUpdateModel()
        val pickedCountries = when(type) {
            Constants.TravelType.DEPARTURE -> getPickedFavoritesAndUpdateModel(Constants.CountriesConfiguration.DEPARTURE_FAVORITES.text)
            Constants.TravelType.ARRIVAL -> getPickedFavoritesAndUpdateModel(Constants.CountriesConfiguration.ARRIVAL_FAVORITES.text)
        }
        val updatedTravelConf = TravelConfiguration(type, pickedCountry, pickedCountries)

        //Reset de la date définie après retour au menu principal
        model.travelConfiguration?.let {
            if (type == it.travelType && it.isCustomDate) {
                updatedTravelConf.timeUTC = it.timeUTC
            }
        }
        model.travelConfiguration = updatedTravelConf
        saveTravelTypeInPrefs(type)
    }

    private fun saveTravelTypeInPrefs(type: Constants.TravelType) {
        sharedPrefs?.let {
            with(it.edit()) {
                putString(
                    Constants.CountriesConfiguration.TRAVEL_TYPE.text,
                    type.text
                )
                apply()
            }
        }
    }

    override fun updateTimePicked(time: ZonedDateTime?) {
        model.travelConfiguration?.let {
            it.timeUTC = time
        }
    }

    override fun getControlCountryAndUpdateModel() : String {
        val controlCountry = getControlCountry()

        model.travelConfiguration?.let {
            it.controlZone = controlCountry
        }

        return controlCountry
    }

    override fun getPickedFavoritesAndUpdateModel(keySharedPrefs: String) : Set<String> {
        val controlCountry = getControlCountry()

       val pickedFavorites = sharedPrefs?.getStringSet(
           keySharedPrefs,
            setOf()
        ) ?: setOf()

        val filteredFavorites = filterFavorites(pickedFavorites, controlCountry)
        if (filteredFavorites.size != pickedFavorites.size) {
            updateFavoritesInPrefs(keySharedPrefs, filteredFavorites)
        }

        model.travelConfiguration?.let {
            it.favoritesList = filteredFavorites
        }

        return filteredFavorites
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun updateFavoritesInPrefs(
        keySharedPrefs: String,
        filteredFavorites: Set<String>
    ) {
        sharedPrefs?.let { sp ->
            with(sp.edit()) {
                putStringSet(
                    keySharedPrefs,
                    filteredFavorites
                )
                apply()
            }
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun filterFavorites(
        pickedFavorites: Set<String>,
        controlCountry: String
    ): Set<String> {
        if (pickedFavorites.isEmpty()) {
            return pickedFavorites
        }

        val editableSet = pickedFavorites.toMutableSet()
        if (editableSet.removeIf { it == controlCountry }) {
            view.onListFiltered()
        }
        return editableSet
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun getControlCountry() = sharedPrefs?.getString(
        Constants.CountriesConfiguration.CONTROL_ZONE.text,
        Constants.DEFAULT_COUNTRY_CODE
    ) ?: Constants.DEFAULT_COUNTRY_CODE

    interface ConfView {
        fun onListFiltered()
    }
}