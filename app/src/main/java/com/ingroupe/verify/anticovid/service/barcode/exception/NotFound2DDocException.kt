package com.ingroupe.verify.anticovid.service.barcode.exception

class NotFound2DDocException(message: String, value: String?) :
    Exception(message.replace("{}", value ?: "")) {
    companion object {
        private const val serialVersionUID = 6647553645921475642L
    }
}