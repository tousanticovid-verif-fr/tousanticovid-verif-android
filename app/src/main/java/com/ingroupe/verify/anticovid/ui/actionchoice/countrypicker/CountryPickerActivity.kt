package com.ingroupe.verify.anticovid.ui.actionchoice.countrypicker

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.model.Country
import com.ingroupe.verify.anticovid.databinding.ActivityCountryPickerBinding
import io.reactivex.rxjava3.disposables.Disposable

class CountryPickerActivity : AppCompatActivity(), CountryPickerView {

    companion object {
        const val TAG = "CountryPickerActivity"

        const val KEY_CODE = "KEY_CODE"

        const val CODE_CONTROL_ZONE = 101
        const val CODE_FAVORITES_DEPARTURE = 102
        const val CODE_FAVORITES_ARRIVAL = 103
    }

    private lateinit var binding: ActivityCountryPickerBinding

    private lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var viewAdapter: CountryRecyclerAdapter
    private lateinit var viewManager: androidx.recyclerview.widget.RecyclerView.LayoutManager

    private var subscribe: Disposable? = null
    private lateinit var presenter: CountryPickerPresenter
    private var pickerType: Int = -1
    private var countries: List<Country>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pickerType = intent.getIntExtra(KEY_CODE, -1)
        binding = ActivityCountryPickerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        presenter = CountryPickerPresenterImpl(this)
        countries = presenter.loadCountries(pickerType)

        setupRecyclerView(pickerType)
        setTextWatcher()
    }

    private fun setupRecyclerView(pickerType: Int) {
        countries?.let {
            viewManager = LinearLayoutManager(this)
            viewAdapter = CountryRecyclerAdapter(this, it)

            recyclerView = binding.rvCountries.apply {
                setHasFixedSize(true)
                layoutManager = viewManager
                adapter = viewAdapter
            }

            presenter.loadFavorites(it, pickerType)
            subscribe = viewAdapter.clickSubject
                .subscribe { pickedCountry ->
                    presenter.onCountrySelected(pickedCountry, intent.getIntExtra(KEY_CODE, -1))
                }
        }
    }

    private fun setTextWatcher() {
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                filterAndUpdateList()
            }
        })
    }

    private fun filterAndUpdateList() {
        val filterValue = binding.etSearch.text
        val filteredList = countries?.filter { it.name.contains(filterValue.toString(), true) }
        filteredList?.let {
            viewAdapter.updateList(filteredList)
        }
    }

    override fun onItemPicked() {
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        supportActionBar?.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        binding.toolbar.setNavigationOnClickListener { onBackPressed() }
        binding.toolbar.title = getString(R.string.conf_country_picker_title)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onFavoritesUpdated(favorites: ArrayList<Country>) {
        countries?.filter { it.isFavorite }?.map { it.isFavorite = false }
        countries?.filter { favorites.contains(it) }?.map { it.isFavorite = true }
        countries = countries?.sortedWith(compareByDescending<Country> { it.isFavorite }.thenBy { it.nameForSort })
        filterAndUpdateList()
    }

    override fun onMaxFavorites() {
        Toast.makeText(this, getString(R.string.conf_max_favorites), Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        subscribe?.dispose()
    }
}