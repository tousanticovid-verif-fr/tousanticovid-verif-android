package com.ingroupe.verify.anticovid.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import com.ingroupe.verify.anticovid.databinding.DialogTextBinding

interface TextDialogListener {
    fun onTextOkClick(position: Int)
}

class TextDialog(
    context: Context,
    private val titleId: Int,
    private val textBoldId: Int,
    private val text: String,
    private val position: Int,
    private val listener: TextDialogListener,
    private val withCancel: Boolean
) : Dialog(context) {

    companion object {
        const val TAG = "TextDialog"
    }

    private lateinit var binding: DialogTextBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DialogTextBinding.inflate(layoutInflater)

        setContentView(binding.root)

        binding.textViewDialogTextTitle.text = context.getString(titleId)

        if (textBoldId == -1) {
            binding.textViewDialogTextBold.visibility = View.GONE
        } else {
            binding.textViewDialogTextBold.visibility = View.VISIBLE
            binding.textViewDialogTextBold.text = context.getString(textBoldId)
        }

        binding.textViewDialogText.text = text

        if (withCancel) {
            binding.buttonTextCancel.visibility = View.VISIBLE
            binding.buttonTextCancel.setOnClickListener { dismiss() }
        } else {
            binding.buttonTextCancel.visibility = View.INVISIBLE
        }
        binding.buttonTextOk.setOnClickListener {
            listener.onTextOkClick(position)
            dismiss()
        }
    }
}