package com.ingroupe.verify.anticovid.service.barcode

import com.ingroupe.verify.anticovid.service.barcode.database.BarcodeLoader
import com.ingroupe.verify.anticovid.service.barcode.database.entity.DocumentType
import com.ingroupe.verify.anticovid.service.barcode.database.entity.Header
import com.ingroupe.verify.anticovid.service.barcode.enums.CharacterEnum.GS
import com.ingroupe.verify.anticovid.service.barcode.enums.CharacterEnum.US
import com.ingroupe.verify.anticovid.service.barcode.enums.ErrorBarcodeEnum
import com.ingroupe.verify.anticovid.service.barcode.exception.Invalid2DDocException
import com.ingroupe.verify.anticovid.service.barcode.exception.NotFound2DDocException
import com.ingroupe.verify.anticovid.service.document.model.DocumentStatic2ddocResult


abstract class Barcode2DDocService {

    companion object {

        private const val VERSION_BEGIN = 2
        private const val VERSION_END = 4

        private const val TYPE_BEGIN = 20
        private const val TYPE_END = 22

        @Throws(Invalid2DDocException::class, NotFound2DDocException::class)
        fun getBarcode2DDoc(encoded2DDoc: String): DocumentStatic2ddocResult {
            if (encoded2DDoc.isBlank() || encoded2DDoc.length < TYPE_END) {
                throw Invalid2DDocException(ErrorBarcodeEnum.ERR01.message, encoded2DDoc)
            }
            val barcode2DDoc = DocumentStatic2ddocResult()
            val header: Header = getHeader(encoded2DDoc)
            val documentType: DocumentType = getDocumentType(encoded2DDoc)
            val encodedHeader = getEncodedHeader(encoded2DDoc, header.getSortedFields())
            val encodedMessage = getEncodedMessage(encoded2DDoc, encodedHeader)
            val encodedSignature = getEncodedSignature(encoded2DDoc)
            val encodedAnnexe = getEncodedAnnexe(encoded2DDoc, encodedSignature)
            barcode2DDoc.header = Zone2DDocService.get2DDocHeaderZone(encodedHeader, header)
            barcode2DDoc.message = Zone2DDocService.get2DDocMessageZone(encodedMessage, documentType)
            barcode2DDoc.annexe =
                if (encodedAnnexe.isEmpty()) null else Zone2DDocService.get2DDocMessageZone(
                    encodedAnnexe,
                    documentType
                )
            barcode2DDoc.hasValidSignature = Signature2DDocService.hasValidSignature(
                encodedHeader + encodedMessage,
                encodedSignature
            )
            return barcode2DDoc
        }

        @Throws(NotFound2DDocException::class)
        private fun getHeader(encoded2DDoc: String): Header {
            val version = encoded2DDoc.substring(VERSION_BEGIN, VERSION_END)
            val header = BarcodeLoader.findHeaderByVersion(version)
            header?.let { return it }
            throw NotFound2DDocException(ErrorBarcodeEnum.ERR02.message, version)
        }

        @Throws(NotFound2DDocException::class)
        private fun getDocumentType(encoded2DDoc: String): DocumentType {
            val type = encoded2DDoc.substring(TYPE_BEGIN, TYPE_END)
            val documentType = BarcodeLoader.findDocumentTYpeByType(type)
            documentType?.let { return it }
            throw NotFound2DDocException(ErrorBarcodeEnum.ERR03.message, type)
        }

        @Throws(Invalid2DDocException::class)
        private fun getEncodedHeader(encoded2DDoc: String, headerFields: List<String>): String {
            if (headerFields.isEmpty()) {
                return ""
            }
            val begin = BarcodeLoader.findHeaderFieldByName(headerFields[0])!!.begin!!
            val end = BarcodeLoader.findHeaderFieldByName(headerFields[headerFields.size - 1])!!.end!!

            if (encoded2DDoc.length < end) {
                throw Invalid2DDocException(ErrorBarcodeEnum.ERR01.message, encoded2DDoc)
            }
            return encoded2DDoc.substring(begin, end)
        }

        private fun getEncodedMessage(encoded2DDoc: String, encodedHeader: String): String {
            return if (encoded2DDoc.contains(US.unicode)) {
                encoded2DDoc.substringAfter(encodedHeader).substringBefore(US.unicode)
            } else encoded2DDoc.replace(encodedHeader, "")
        }

        private fun getEncodedSignature(encoded2DDoc: String): String {
            var signature = ""
            if (encoded2DDoc.contains(US.unicode)) {
                signature = encoded2DDoc.substringAfter(US.unicode)
            }
            if (signature.contains(GS.unicode)) {
                signature = signature.substring(0, signature.indexOf(GS.unicode))
            }
            return signature
        }

        private fun getEncodedAnnexe(encoded2DDoc: String, encodedSignature: String): String {
            var annexe = ""
            if (encoded2DDoc.contains(US.unicode)) {
                annexe = encoded2DDoc.substringAfter(encodedSignature)
            }
            if (annexe.isNotEmpty()) {
                annexe = annexe.substring(1)
            }
            return annexe
        }
    }

}