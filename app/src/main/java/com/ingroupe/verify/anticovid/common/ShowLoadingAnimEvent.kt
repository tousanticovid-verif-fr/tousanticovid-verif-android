package com.ingroupe.verify.anticovid.common

import android.app.Activity

class ShowLoadingAnimEvent(val activity: Activity?, val show: Boolean)