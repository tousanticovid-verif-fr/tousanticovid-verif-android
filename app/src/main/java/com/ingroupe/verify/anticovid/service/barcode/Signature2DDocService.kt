package com.ingroupe.verify.anticovid.service.barcode

import android.util.Log
import com.ingroupe.verify.anticovid.service.barcode.enums.EcdsaEnum
import com.ingroupe.verify.anticovid.service.barcode.enums.ErrorBarcodeEnum
import com.ingroupe.verify.anticovid.synchronization.elements.PublicKey2ddoc
import org.apache.commons.codec.binary.Base32
import org.bouncycastle.asn1.ASN1EncodableVector
import org.bouncycastle.asn1.ASN1Integer
import org.bouncycastle.asn1.DERSequence
import java.io.IOException
import java.math.BigInteger
import java.security.*


abstract class Signature2DDocService {

    companion object {
        const val TAG = "Signature2DDocService"

        private const val CERTIFICATE_BEGIN = 4
        private const val CERTIFICATE_END = 12

        fun hasValidSignature(signedData: String, encodedSignature: String): Boolean {
            val ecdsa: EcdsaEnum? = EcdsaEnum.getByLength(encodedSignature.length)
            if (ecdsa == null) {
                Log.i(TAG, ErrorBarcodeEnum.ERR07.message)
                return false
            }
            val certificateKey = signedData.substring(CERTIFICATE_BEGIN, CERTIFICATE_END)
            val publicKey: PublicKey? = PublicKey2ddoc.get2DDocPublicKey(certificateKey)
            if (publicKey != null) {
                try {
                    val derSequence: DERSequence =
                        getDERSequenceFromSignature(Base32().decode(encodedSignature))
                    val signature: Signature = Signature.getInstance(ecdsa.algorithm)
                    signature.initVerify(publicKey)
                    signature.update(signedData.toByteArray())
                    return verifySignature(signature, derSequence.encoded)
                } catch (e: IllegalArgumentException) {
                    Log.e(TAG, ErrorBarcodeEnum.ERR08.message, e)
                } catch (e: InvalidKeyException) {
                    Log.e(TAG, ErrorBarcodeEnum.ERR08.message, e)
                } catch (e: IOException) {
                    Log.e(TAG, ErrorBarcodeEnum.ERR08.message, e)
                } catch (e: NoSuchAlgorithmException) {
                    Log.e(TAG, ErrorBarcodeEnum.ERR08.message, e)
                } catch (e: SignatureException) {
                    Log.e(TAG, ErrorBarcodeEnum.ERR08.message, e)
                }
            } else {
                Log.i(TAG, ErrorBarcodeEnum.ERR06.message)
            }
            return false
        }

        @Throws(SignatureException::class)
        private fun verifySignature(
            signature: Signature,
            encodedDerSequence: ByteArray
        ): Boolean {
            Log.i(TAG, ErrorBarcodeEnum.INF01.message)
            val isValidSignature: Boolean = signature.verify(encodedDerSequence)
            if (!isValidSignature) {
                Log.i(TAG, ErrorBarcodeEnum.ERR09.message)
            }
            return isValidSignature
        }

        private fun getDERSequenceFromSignature(decodedSignature: ByteArray): DERSequence {
            val length = decodedSignature.size / 2
            val bytes = ByteArray(length)
            val asn1EncodableVector = ASN1EncodableVector()
            System.arraycopy(decodedSignature, 0, bytes, 0, length)
            asn1EncodableVector.add(ASN1Integer(BigInteger(1, bytes)))
            System.arraycopy(decodedSignature, length, bytes, 0, length)
            asn1EncodableVector.add(ASN1Integer(BigInteger(1, bytes)))
            return DERSequence(asn1EncodableVector)
        }
    }

}