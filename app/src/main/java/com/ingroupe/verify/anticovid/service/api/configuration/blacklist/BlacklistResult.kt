package com.ingroupe.verify.anticovid.service.api.configuration.blacklist

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BlacklistResult(
    @SerializedName("elements")
    var elements: List<BlacklistElement>? = null,
    @SerializedName("lastIndexBlacklist")
    var lastIndexBlacklist: Int? = null
): Serializable
