package com.ingroupe.verify.anticovid.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView


class TutoSliderAdapter(private val layouts: IntArray) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(viewType, parent, false)
        return TutoViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return layouts[position]
    }

    override fun getItemCount(): Int {
        return layouts.size
    }

    inner class TutoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView? = null
        var year: TextView? = null
        var genre: TextView? = null
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    }
}