package com.ingroupe.verify.anticovid.ui.scan

import com.ingroupe.verify.anticovid.service.document.model.DocumentResult


interface ScanView {

    fun showErrorMessage(title: Int, message: Int)

    fun showResult(response: DocumentResult)

    fun onDccLoaded(documentResult: DocumentResult?)

}