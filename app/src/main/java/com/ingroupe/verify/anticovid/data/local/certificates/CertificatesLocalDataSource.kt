package com.ingroupe.verify.anticovid.data.local.certificates

import android.util.Base64

class CertificatesDataSource(private val certificatesDao: CertificatesDao) {

    fun cleanAndInsertAll(certificatesMap: Map<String, String>) {
        certificatesDao.deleteAllAndInsert(*certificatesMap.toCertificateDccList().toTypedArray())
    }

    fun getCertificateWithKey(key: String): CertificateDcc? {
        return certificatesDao.getCertificateWithKey(key)
    }
}

fun Map<String, String>.toCertificateDccList(): List<CertificateDcc> =
    this.map {
        CertificateDcc(
            key = it.key,
            value = String(Base64.decode(it.value, Base64.NO_WRAP))
        )
    }