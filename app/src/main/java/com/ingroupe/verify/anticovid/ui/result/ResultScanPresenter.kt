package com.ingroupe.verify.anticovid.ui.result

import android.content.Context
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.common.model.ResultScan
import com.ingroupe.verify.anticovid.common.model.TravelConfiguration
import com.ingroupe.verify.anticovid.service.api.configuration.conf.ConfResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentDataResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentEngineDataResult
import com.ingroupe.verify.anticovid.service.document.model.DocumentErrorsResult

interface ResultScanPresenter {

    fun reloadConf(
        model: SharedViewModel
    )

    fun prepareResult(
        resultScan: ResultScan,
        resultType: String?,
        documentDataResult: DocumentDataResult,
        configuration: ConfResult?,
        travelConfiguration: TravelConfiguration?,
        context: Context,
        displayMode: Constants.DisplayMode,
        engineData: DocumentEngineDataResult?
    )

    fun prepareErrorResult(
        resultScan: ResultScan,
        errors: ArrayList<DocumentErrorsResult>,
        context: Context
    )
}