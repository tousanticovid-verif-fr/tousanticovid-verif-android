package com.ingroupe.verify.anticovid.ui.actionchoice.countrypicker

import com.ingroupe.verify.anticovid.common.model.Country

interface CountryPickerPresenter {

    fun loadCountries(pickerType: Int) : List<Country>?

    fun loadFavorites(countries: List<Country>, pickerType: Int)

    fun onCountrySelected(country: Country, pickerType: Int)

}