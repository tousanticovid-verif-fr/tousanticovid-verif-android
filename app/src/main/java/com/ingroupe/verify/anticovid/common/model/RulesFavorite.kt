package com.ingroupe.verify.anticovid.common.model

import dgca.verifier.app.engine.data.Rule

class RulesFavorite(
    var country: Country,
    var rules: List<Rule> = listOf(),
    var arrivalCountryCode: String,
    var showGenericMessage: Boolean = false,
    var checkColorRules: Boolean = false
)