package com.ingroupe.verify.anticovid.ui.scan

import android.content.Context
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.common.model.TravelConfiguration

interface ScanPresenter {

    fun on2dDocDetected(context: Context, result2DDoc: String, travelConfiguration: TravelConfiguration?): Boolean

    fun checkExpirationAndDo(model: SharedViewModel, context: Context, lmbd: () -> Unit)

    fun checkDcc(qrCode: String, context: Context, travelConfiguration: TravelConfiguration?)

    fun checkDccExemption(qrCode: String, context: Context, travelConfiguration: TravelConfiguration?)

    fun checkDccActivity(qrCode: String, context: Context, travelConfiguration: TravelConfiguration?)
}