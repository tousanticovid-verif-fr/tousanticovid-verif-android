package com.ingroupe.verify.anticovid.ui.actionchoice.countrypicker

import com.ingroupe.verify.anticovid.common.model.Country

interface CountryPickerView {

    fun onItemPicked()
    fun onFavoritesUpdated(favorites: ArrayList<Country>)
    fun onMaxFavorites()

}