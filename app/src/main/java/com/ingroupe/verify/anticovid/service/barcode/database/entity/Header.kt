package com.ingroupe.verify.anticovid.service.barcode.database.entity

import com.google.gson.annotations.SerializedName

data class Header (
    @SerializedName("version")
    val version: String,
    @SerializedName("fields")
    val fields: List<String>
) {

    fun getSortedFields(): List<String> {
        return fields.sorted()
    }
}