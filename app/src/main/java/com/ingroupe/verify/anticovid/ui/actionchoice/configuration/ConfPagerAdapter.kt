package com.ingroupe.verify.anticovid.ui.actionchoice.configuration

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class ConfPagerAdapter(parentFragment: Fragment): FragmentStateAdapter(parentFragment) {
    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when(position) {
            0 -> ConfDepartureChildFragment.newInstance()
            else -> ConfArrivalChildFragment.newInstance()
        }
    }

}