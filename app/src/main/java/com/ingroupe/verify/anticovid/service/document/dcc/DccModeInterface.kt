package com.ingroupe.verify.anticovid.service.document.dcc

import android.content.Context
import android.util.Log
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.service.document.DocOfflineService
import com.ingroupe.verify.anticovid.service.document.model.DocumentStaticDccResult
import com.ingroupe.verify.anticovid.service.document.model.InfoTestForStat
import com.ingroupe.verify.anticovid.synchronization.elements.Expiration
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

interface DccModeInterface {

    fun getDccTest(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?,
        infoTestForStat: InfoTestForStat
    )

    fun getDccVaccination(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    )

    fun getDccRecovery(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?,
        infoTestForStat: InfoTestForStat
    )

    fun getDccExemption(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    )

    fun getDccActivity(
        staticDcc: DocumentStaticDccResult?,
        mappedDynamicData: MutableMap<String, String>,
        context: Context,
        controlTimeOT: ZonedDateTime?
    )

    companion object {
        fun getLocalDateFromDcc(strDate: String): LocalDate? {
            return try {
                LocalDate.parse(strDate, DateTimeFormatter.ISO_DATE)
            } catch (e: Exception) {
                try {
                    LocalDate.parse(strDate, DateTimeFormatter.ISO_DATE_TIME)
                } catch (e: Exception) {
                    try {
                        val incompleteDate = Utils.splitDateFromString(strDate)
                        if(incompleteDate?.year != null) {
                            val localDateMonth = LocalDate.of(incompleteDate.year!!.toInt(), incompleteDate.month?.toInt()?:12, 1)
                            localDateMonth.plusDays(localDateMonth.lengthOfMonth().toLong() -1) // dernier jour du mois
                        } else {
                            null
                        }
                    } catch (e: Exception) {
                        null
                    }
                }
            }
        }

        fun getOffsetDateTimeFromDcc(strDate: String): OffsetDateTime? {
            return try {
                OffsetDateTime.parse(strDate, DateTimeFormatter.ISO_DATE_TIME)
            } catch (e: Exception) {
                null
            }
        }

        fun getString(
            field: String,
            value: String,
            context: Context,
            mappedDynamicData: MutableMap<String, String>
        ) {
            try {
                val updatedValue = value.replace("-", "_").replace("/", "_")
                val property = "@string/dynamic_$field.${updatedValue}"
                val packageName: String = context.packageName
                val resId: Int = context.resources.getIdentifier(property, "string", packageName)
                if (resId != 0) {
                    mappedDynamicData[field] = context.getString(resId)
                }
            } catch (e: Exception) {
                Log.e(DocOfflineService.TAG, "field not found")
            }
        }

        fun checkIat(
            issuedAt: ZonedDateTime,
            controlTime: ZonedDateTime
        ): Boolean {
            return issuedAt.isBefore(controlTime)
        }

        fun checkExp(
            expirationTime: ZonedDateTime,
            dccType: DocumentStaticDccResult.DccType,
            controlTime: ZonedDateTime
        ): Boolean {
            if(Expiration.useExpirationDate(dccType)
                && !expirationTime.isAfter(controlTime)) {
                return false
            }

            return true

        }
    }
}