package com.ingroupe.verify.anticovid.ui.actionchoice.configuration

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.CountryUtils
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.common.model.Country
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.*


abstract class ConfParentFragment : Fragment(), ConfPresenterImpl.ConfView {

    companion object {
        private const val DAYS_RANGE_DATE_PICKER = 7
    }

    lateinit var model: SharedViewModel
    lateinit var presenter: ConfPresenter
    var countries: List<Country>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        countries = CountryUtils.getCountries()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        presenter = ConfPresenterImpl(model, this)
    }

    fun showDatePickerDialog() {
        val c: Calendar = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)

        val minDate = Calendar.getInstance()
        minDate.add(Calendar.DAY_OF_MONTH, -DAYS_RANGE_DATE_PICKER)
        val maxDate = Calendar.getInstance()
        maxDate.add(Calendar.DAY_OF_MONTH, DAYS_RANGE_DATE_PICKER)

        val datePickerDialog = context?.let { ctx ->
            DatePickerDialog(
                ctx,
                { _, year, monthOfYear, dayOfMonth ->
                    val pickedDate = ZonedDateTime.of(
                        year,
                        monthOfYear + 1,
                        dayOfMonth,
                        0,
                        0,
                        0,
                        0,
                        ZoneId.systemDefault()
                    )
                    showTimePickerDialog(pickedDate)
                }, mYear, mMonth, mDay
            )
        }

        datePickerDialog?.datePicker?.minDate = minDate.timeInMillis
        datePickerDialog?.datePicker?.maxDate = maxDate.timeInMillis

        datePickerDialog?.show()
    }

    private fun showTimePickerDialog(pickedDate: ZonedDateTime) {
        val c = Calendar.getInstance()
        val mHour = c.get(Calendar.HOUR_OF_DAY)
        val mMinute = c.get(Calendar.MINUTE)

        val timePickerDialog = context?.let { ctx ->
            TimePickerDialog(
                ctx,
                { _, hours, minutes ->
                    val pickedDateTime = ZonedDateTime.of(
                        pickedDate.year,
                        pickedDate.monthValue,
                        pickedDate.dayOfMonth,
                        hours,
                        minutes,
                        0,
                        0,
                        ZoneId.systemDefault()
                    )
                    val utcZoneId: ZoneId = ZoneId.ofOffset("", ZoneOffset.UTC).normalized()

                    updateTimePicked(pickedDateTime.withZoneSameInstant(utcZoneId))
                }, mHour, mMinute, true
            )
        }

        timePickerDialog?.show()
    }

    fun getPickedControlZone(): Country? {
        val pickedCountryCode = presenter.getControlCountryAndUpdateModel()
        return countries?.find { it.nameCode.lowercase() == pickedCountryCode.lowercase() }
    }

    fun getPickedFavorites(keySharedPrefs: String): List<Country> {
        val pickedCountryCode = presenter.getPickedFavoritesAndUpdateModel(keySharedPrefs)
        return countries?.filter {
            pickedCountryCode.contains(it.nameCode) || pickedCountryCode.contains(it.nameCode.lowercase())
        }?.sortedBy { item -> item.nameForSort } ?: listOf()
    }

    fun showDialogResetTime() {
        val builder = AlertDialog.Builder(activity)
        builder.setMessage(getString(R.string.conf_dialog_datetime_text))
            .setPositiveButton(getString(R.string.conf_yes)) { _, _ ->
                updateTimePicked(null)
            }
            .setNegativeButton(
                getString(R.string.conf_no)
            ) { _, _ ->
                showDatePickerDialog()
            }
            .create()
            .show()
    }

    private fun updateTimePicked(newValue: ZonedDateTime?) {
        presenter.updateTimePicked(newValue)
        displayDate()
    }

    abstract fun displayControlZone()
    abstract fun displayFavorites()
    abstract fun displayDate()
}
