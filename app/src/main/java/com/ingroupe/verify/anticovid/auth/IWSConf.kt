package com.ingroupe.verify.anticovid.auth

import com.ingroupe.verify.anticovid.service.api.configuration.ConfigurationService
import okhttp3.OkHttpClient

interface IWSConf {

    fun getOkHttpClient(): OkHttpClient

    fun getConfigurationBaseUrl(): String

    fun getConfigurationService(): ConfigurationService

}