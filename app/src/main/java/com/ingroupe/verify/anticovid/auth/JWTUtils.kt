package com.ingroupe.verify.anticovid.auth

import android.content.Context
import android.util.Base64
import android.util.Log
import com.auth0.android.jwt.JWT
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.BuildConfig
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.Constants.DisplayMode
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.synchronization.SynchronisationUtils
import java.nio.charset.Charset
import java.security.KeyFactory
import java.security.PublicKey
import java.security.Signature
import java.security.spec.X509EncodedKeySpec
import java.util.*
import kotlin.collections.ArrayList


class JWTUtils {

    companion object {
        const val TAG = "jwtUtils"

        fun getSiren(): String {
            val settingsPref =
                App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
            return settingsPref.getString(Constants.SavedItems.CURRENT_SIREN.text, null) ?: Constants.DEFAULT_SIREN
        }

        fun loadConf(
            model: SharedViewModel,
            checkExpiration: Boolean
        ): Boolean {
            var isExpired = false

            val settingsPref =
                App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
            val dateExp = Date(settingsPref.getLong(Constants.SavedItems.CONF_DATE_EXP.text, 0))
            val token = settingsPref.getString(Constants.SavedItems.CURRENT_TOKEN.text, null)

            var confName = Constants.CONF_BASIC
            if(token != null) {
                if(checkExpiration && dateExp.before(Date())) {
                    resetPrefsForToken()
                    isExpired = true
                } else if (getDisplayMode().isPremium) {
                    confName = Constants.CONF_CONTROL
                }
            }

            model.configuration = Utils.loadFromAsset(confName)
            Utils.sortConf(model.configuration)

            return isExpired
        }

        fun resetPrefsForToken() {

            val settingsPref =
                App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)

            with(settingsPref.edit()) {
                putLong(
                    Constants.SavedItems.CONF_DATE_EXP.text,
                    0
                )
                remove(Constants.SavedItems.CURRENT_TOKEN.text)
                putString(
                    Constants.SavedItems.CURRENT_SIREN.text,
                    getDefaultSiren()
                )
                putBoolean(
                    Constants.SavedItems.SHOW_RESULT_TUTO.text,
                    true
                )
                putString(
                    Constants.SavedItems.DISPLAY_MODE.text,
                    DisplayMode.VACCINE.text
                )
                apply()
            }
        }

        private fun getDefaultSiren(): String {
            try {
                val jwt = JWT(BuildConfig.PERSISTENT_TOKEN)
                return jwt.getClaim("siren").asString() ?: Constants.DEFAULT_SIREN

            } catch (e: Exception) {
                Log.d(TAG,"Exception occurred: ", e)
            }
            return Constants.DEFAULT_SIREN
        }

        fun isPremium(): Boolean {
            val settingsPref =
                App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)

            return settingsPref.getString(Constants.SavedItems.CURRENT_TOKEN.text, null) != null
        }

        fun isDisplayPremium(): Boolean {
            return getDisplayMode().isPremium
        }

        fun getDisplayMode(): DisplayMode {
            val settingsPref =
                App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
            var displayModeText = settingsPref.getString(Constants.SavedItems.DISPLAY_MODE.text, DisplayMode.VACCINE.text) ?: DisplayMode.VACCINE.text

            // pour rétrocompatibilité
            if(displayModeText == "LITE") { displayModeText = DisplayMode.VACCINE.text }

            // Le pass vaccinal n'est pas activé tout de suite
            if(!SynchronisationUtils.isPassVaccineStarted()
                && displayModeText == DisplayMode.VACCINE.text) {
                displayModeText = DisplayMode.HEALTH.text
            }

            // Au démarrage du pass vaccinal, on passe du pass sanitaire au pass vaccinal
            val passVaccineStarted = settingsPref.getBoolean(Constants.SavedItems.PASS_VACCINE_STARTED.text, false)
            if(SynchronisationUtils.isPassVaccineStarted()
                && !passVaccineStarted) {
                    with(settingsPref.edit()) {
                        putBoolean(Constants.SavedItems.PASS_VACCINE_STARTED.text, true)
                        apply()
                    }
                if (displayModeText == DisplayMode.HEALTH.text) {
                    displayModeText = DisplayMode.VACCINE.text
                }
                // on affiche le tuto pour le mode LITE (et on sauvegarde le passage en pass vaccinal)
                if(displayModeText == DisplayMode.VACCINE.text) {
                    with(settingsPref.edit()) {
                        putString(Constants.SavedItems.DISPLAY_MODE.text, DisplayMode.VACCINE.text)
                        putBoolean(Constants.SavedItems.SHOW_RESULT_TUTO.text, true)
                        apply()
                    }
                }
            }

            return DisplayMode.valueOf(displayModeText)
        }

        fun changeDisplayMode(displayMode: DisplayMode,
                              model: SharedViewModel) {

            model.configuration = if (displayMode.isPremium) {
                Utils.loadFromAsset(Constants.CONF_CONTROL)
            } else {
                Utils.loadFromAsset(Constants.CONF_BASIC)
            }
            showTutoIfNeeded(displayMode)

            val settingsPref =
                App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)

            with(settingsPref.edit()) {
                putString(
                    Constants.SavedItems.DISPLAY_MODE.text,
                    displayMode.text
                )
                apply()
            }
        }

        fun showTutoIfNeeded(displayMode: DisplayMode) : Boolean {
            val settingsPref = App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
            var shouldResetTutos = false

            settingsPref.getString(Constants.SavedItems.DISPLAY_MODE.text, null)?.let {
                shouldResetTutos = ((it != DisplayMode.HEALTH.text && displayMode.text == DisplayMode.HEALTH.text) ||
                        (it != DisplayMode.VACCINE.text && displayMode.text == DisplayMode.VACCINE.text))
            }

            if (shouldResetTutos) {
                with(settingsPref.edit()) {
                    putBoolean(
                        Constants.SavedItems.SHOW_RESULT_TUTO.text,
                        true
                    )
                    apply()
                }
            }

            return shouldResetTutos
        }

        fun daysBeforeExpiration(): Int {
            val dateExp = getDateExpiration()
            val nbDaysBeforeExp = (dateExp - Date().time) / (1000*60*60*24)
            return if(nbDaysBeforeExp < 0) {
                0
            } else {
                nbDaysBeforeExp.toInt()
            }
        }

        fun hoursBeforeExpiration(): Int {
            val dateExp = getDateExpiration()
            val nbHoursBeforeExp = (dateExp - Date().time) / (1000*60*60)
            return if(nbHoursBeforeExp < 0) {
                0
            } else {
                nbHoursBeforeExp.toInt()
            }
        }

        private fun getDateExpiration() : Long {
            val settingsPref =
                App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
            return settingsPref.getLong(Constants.SavedItems.CONF_DATE_EXP.text, 0)
        }
    }
}