package com.ingroupe.verify.anticovid.ui.statistic

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.adapter.StatisticAdapter
import com.ingroupe.verify.anticovid.common.FeatureChildFragment
import com.ingroupe.verify.anticovid.common.model.StatsPeriod
import com.ingroupe.verify.anticovid.databinding.StatisticMainBinding

class StatisticChildFragment : FeatureChildFragment(), StatisticView {

    override fun getTitle(): String = "Statistiques"
    override fun getTitleId(): Int = R.string.title_statistic

    companion object {
        const val TAG = "Statistic"
        fun newInstance() = StatisticChildFragment()
    }
    private var _binding: StatisticMainBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var recyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var viewAdapter: androidx.recyclerview.widget.RecyclerView.Adapter<*>
    private lateinit var viewManager: androidx.recyclerview.widget.RecyclerView.LayoutManager

    private var presenter : StatisticPresenter? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = StatisticMainBinding.inflate(inflater, container, false)

        val view = binding.root

        if(presenter == null) {
            presenter = StatisticPresenterImpl(this)
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val statsPeriod = StatsPeriod.getStatsPeriod()

        presenter?.let {
            viewManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
            viewAdapter = StatisticAdapter(it.formatMap(statsPeriod), requireContext())

            recyclerView = binding.rvStats.apply {
                // use this setting to improve performance if you know that changes
                // in content do not change the tutorial_skip_view size of the RecyclerView
                setHasFixedSize(true)

                // use a linear tutorial_skip_view manager
                layoutManager = viewManager

                // specify an viewAdapter (see also next example)
                adapter = viewAdapter
            }
        }
    }
    
    override fun showNavigation(): MainActivity.NavigationIcon {
        return MainActivity.NavigationIcon.BACK
    }


}