package com.ingroupe.verify.anticovid.ui.tutorialresult.premium

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.adapter.IAdapterInterfaceClick
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.FeatureChildFragment
import com.ingroupe.verify.anticovid.common.SharedViewModel
import com.ingroupe.verify.anticovid.databinding.TutorialResultPremiumMainBinding
import com.ingroupe.verify.anticovid.ui.result.ResultScanChildFragment

class TutorialResultPremiumChildFragment : FeatureChildFragment(), TutorialResultPremiumView, IAdapterInterfaceClick {
    override fun getTitle(): String = "Résultat - Tutoriel"
    override fun getTitleId(): Int = R.string.title_tutorial_result

    companion object {
        const val TAG = "tutorialOT"
        fun newInstance() = TutorialResultPremiumChildFragment()
    }
    private var _binding: TutorialResultPremiumMainBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var model: SharedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        _binding = TutorialResultPremiumMainBinding.inflate(inflater, container, false)
        val view = binding.root

        model = activity?.run {
            ViewModelProvider(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")

        binding.buttonTutoToResult.setOnClickListener {

            val settingsPref =
                App.getContext().getSharedPreferences(Constants.SETTINGS_KEY, Context.MODE_PRIVATE)
            settingsPref?.let { sp ->
                with(sp.edit()) {
                    putBoolean(
                        Constants.SavedItems.SHOW_RESULT_TUTO.text,
                        !binding.checkBoxShowTutoResult.isChecked
                    )
                    apply()
                }
            }
            goToResult()
        }

        return view
    }

    override fun onResume() {
        Log.d(TAG, "on Resume")
        super.onResume()
    }

    private fun goToResult() {
        featureFragment?.replaceFragment(ResultScanChildFragment.TAG)
    }

    override fun showNavigation(): MainActivity.NavigationIcon {
        return MainActivity.NavigationIcon.BACK
    }

    override fun onItemClick(position: Int) {
        // rien à faire
    }
}