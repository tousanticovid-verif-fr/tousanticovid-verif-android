package com.ingroupe.verify.anticovid.synchronization.elements

import androidx.core.util.AtomicFile
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.synchronization.ConfServiceUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File

object Analytics {

    private const val FOLDER_NAME: String = "TacVerifAnalytics"
    private const val FILE_NAME_VERIF_EVENTS: String = "verif_events"


    suspend fun sendAnalytics(listener: ConfServiceUtils.ConfServiceListener, tag: String, backgroundWork: Boolean) {
        ConfServiceUtils.sendAnalytics(
            getVerifEvents(),
            listener,
            tag,
            backgroundWork
        )
    }

    fun deleteOldStats() {
        executeActionOnAtomicFile {
            AtomicFile(
                File(
                    File(App.getContext().filesDir, FOLDER_NAME),
                    FILE_NAME_VERIF_EVENTS
                )
            ).delete()
        }
    }

    @Synchronized
    fun reportVerifEvent(verifEvent: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val file = File(File(App.getContext().filesDir, FOLDER_NAME),
                FILE_NAME_VERIF_EVENTS
            )
            val list = getVerifEvents().toMutableList()
            list.add(verifEvent)
            writeStringToFile(file, list)
        }
    }

    private suspend fun getVerifEvents(): List<String> {
        val file = File(File(App.getContext().filesDir, FOLDER_NAME),
            FILE_NAME_VERIF_EVENTS
        )
        return getEventFromFile(file)
    }

    private fun writeStringToFile(file: File, verifEvents: List<String>) {
        executeActionOnAtomicFile {
            val atomicFile = AtomicFile(file)
            val fileOutputStream = atomicFile.startWrite()
            verifEvents.forEach { line ->
                fileOutputStream.write(line.toByteArray())
                fileOutputStream.write(System.getProperty("line.separator")!!.toByteArray())
            }
            atomicFile.finishWrite(fileOutputStream)
        }
    }


    @Suppress("BlockingMethodInNonBlockingContext")
    private suspend fun getEventFromFile(file: File): List<String> {
        return withContext(Dispatchers.IO) {
            executeActionOnAtomicFile {
                if (file.exists()) {
                    val atomicFile = AtomicFile(file)
                    atomicFile.openRead().use { inputStream ->
                        try {
                            inputStream.bufferedReader().use { br ->
                                br.readLines()
                            }
                        } catch (e: Exception) {
                            emptyList()
                        }
                    }
                } else {
                    emptyList()
                }
            }
        }
    }


    @Synchronized
    private fun <T> executeActionOnAtomicFile(action: () -> T): T {
        return action()
    }
}