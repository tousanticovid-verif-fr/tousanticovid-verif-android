package com.ingroupe.verify.anticovid.synchronization

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.VisibleForTesting
import androidx.preference.PreferenceManager
import androidx.work.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.MainActivity
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.auth.JWTUtils
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.Constants.*
import com.ingroupe.verify.anticovid.common.Constants.SpecificList.*
import com.ingroupe.verify.anticovid.common.Constants.ValidityValues.*
import com.ingroupe.verify.anticovid.common.Constants.ValidityValuesString.*
import com.ingroupe.verify.anticovid.common.CountryUtils
import com.ingroupe.verify.anticovid.common.FileLogger
import com.ingroupe.verify.anticovid.data.EngineManager
import com.ingroupe.verify.anticovid.databinding.PopupSynchronizationNeededBinding
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncResult
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncValidityMode
import com.ingroupe.verify.anticovid.synchronization.elements.*
import com.ingroupe.verify.anticovid.synchronization.elements.Labels
import kotlinx.coroutines.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit


object SynchronisationUtils {

    private const val TAG = "tacvSyncTag"

    private const val UNIQUE_WORK_NAME = "tacvSyncWorkName"

    private const val HIDE_SYNC_WARNING_DATE = "HIDE_SYNC_WARNING_DATE"

    private const val HIDE_SYNC_WARNING_MINUTES = 60

    private const val NOT_GREEN_COUNTRIES = "NOT_GREEN_COUNTRIES"

    private const val RULE_ENGINE_MESSAGE_OT = "RULE_ENGINE_MESSAGE_OT"

    fun checkStep(context: Context, showPopup: Boolean = false) {
        when (getCurrentStep()) {
            1 -> warnStep1(context)
            2,3 -> warnStep2(context, showPopup)
        }
    }

    private fun warnStep1(context: Context) {
        if (context is MainActivity) {
            context.showSnackBar(
                Constants.SNACKBAR_CERT,
                context.getString(R.string.snackbar_sync_needed_line_1),
                null,
                criticity = Criticity.INFO
            ) {
                showPopup(context)
            }
        }
    }

    private fun warnStep2(context: Context, showPopup: Boolean) {
        if (context is MainActivity) {
            context.showSnackBar(
                Constants.SNACKBAR_CERT,
                context.getString(R.string.snackbar_sync_needed_line_1),
                null,
                criticity = Criticity.CRITICAL
            ) {
                showPopup(context)
            }

            if (!popupWasPaused() && showPopup) {
                showPopup(context, isHideable = true)
            }
        }
    }

    @VisibleForTesting
    internal fun popupWasPaused() : Boolean {
        val prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext())
        val timePopupPaused = prefs.getLong(HIDE_SYNC_WARNING_DATE, Date(0L).time)

        val timeElapsed = TimeUnit.MILLISECONDS.toMinutes(Date().time - timePopupPaused)

        return timeElapsed <= HIDE_SYNC_WARNING_MINUTES
    }


    private fun showPopup(context: Context, isHideable: Boolean = false) {
        val bindingDialog = PopupSynchronizationNeededBinding.inflate(LayoutInflater.from(context))
        val dialog = AlertDialog.Builder(ContextThemeWrapper(context,
            R.style.AlertDialogCustom
        ))
            .setView(bindingDialog.root)
            .create()

        if(isHideable) {
            bindingDialog.checkBoxHideSynchonization.visibility = View.VISIBLE
        } else {
            bindingDialog.checkBoxHideSynchonization.visibility = View.GONE
        }

        dialog.show()
        bindingDialog.buttonAccept.setOnClickListener {
            if (bindingDialog.checkBoxHideSynchonization.isChecked) {
                with(PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit()) {
                    putLong(HIDE_SYNC_WARNING_DATE, Date().time)
                    apply()
                }
            }
            dialog.dismiss()
        }
    }

    private fun getStep(step: Step): Long {
        val certPref = PreferenceManager.getDefaultSharedPreferences(App.getContext())
        return certPref.getLong(step.text, step.default)
    }

    fun getCurrentStep(): Int {
        val period = getHoursSinceLastSynch()
        return when {
            period < getStep(Step.CERT_STEP_1) -> 0
            period < getStep(Step.CERT_STEP_2) -> 1
            period < getStep(Step.CERT_STEP_3) -> 2
            else -> 3
        }
    }

    fun isSyncNeeded(): Boolean {
        return getStep(Step.CERT_FREQUENCY) <= getHoursSinceLastSynch()
    }

    private fun getHoursSinceLastSynch(): Long {
        return TimeUnit.MILLISECONDS.toHours(Date().time - getDateLastSynchronization().time)
    }

    fun getDateLastSynchronization(): Date {
        val pref = PreferenceManager.getDefaultSharedPreferences(App.getContext())
        val time = pref.getLong(Constants.LAST_SYNC_DATE, 0)

        // on ne doit pas avoir une date de synchro dans le futur
        return if(time > Date().time) {
            with(pref.edit()) {
                putLong(Constants.LAST_SYNC_DATE, 0)
                apply()
            }
            Date(0)
        }  else {
            Date(time)
        }
    }

    fun getValidityValue(svEnum: ValidityValues, mode: ControlMode) : Int {
        val certPref = PreferenceManager.getDefaultSharedPreferences(App.getContext())
        return certPref.getInt(mode.prefix + svEnum.text, svEnum.default)
    }

    fun getValidityStringValue(svEnum: ValidityValuesString, mode: ControlMode) : String {
        val certPref = PreferenceManager.getDefaultSharedPreferences(App.getContext())
        return certPref.getString(mode.prefix + svEnum.text, svEnum.default) ?: svEnum.default
    }

    private fun getSpecificList(svEnum: SpecificList) : Map<String, String> {
        val certPref = PreferenceManager.getDefaultSharedPreferences(App.getContext())
        val jsonMap = certPref.getString(svEnum.text, "{}")

        val gson = Gson()
        val itemType = object : TypeToken<MutableMap<String, String>>() {}.type
        return gson.fromJson(jsonMap, itemType)
    }

    fun getLabelSpecificList(svEnum: SpecificList, key: String): String? {
        val map = getSpecificList(svEnum)
        val insensitiveMap = TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER)
        insensitiveMap.putAll(map)
        return insensitiveMap[key]
    }

    fun getRuleEngineMessageOT(): String? {
        val pref = PreferenceManager.getDefaultSharedPreferences(App.getContext())

        return pref.getString(RULE_ENGINE_MESSAGE_OT, "")
    }

    fun isCountryNotGreen(countryCode: String): Boolean {
        val pref = PreferenceManager.getDefaultSharedPreferences(App.getContext())

        val countriesNotGreen = pref.getStringSet(NOT_GREEN_COUNTRIES, setOf())

        return countriesNotGreen?.find { it.lowercase() == countryCode.lowercase() } != null
    }

    private fun getVaccinePassStartDate(): LocalDate {
        val certPref = PreferenceManager.getDefaultSharedPreferences(App.getContext())
        val vaccinePassStartDate =
            certPref.getString(VACCINE_PASS_START_DATE.text, VACCINE_PASS_START_DATE.default)
                ?: VACCINE_PASS_START_DATE.default
        return LocalDate.parse(vaccinePassStartDate, DateTimeFormatter.ISO_LOCAL_DATE)
    }

    fun isPassVaccineStarted(): Boolean {
        return getVaccinePassStartDate() <= LocalDate.now()
    }

    fun saveSynchronization(synchr: SyncResult?,
                            context: Context,
                            listener: SyncListener,
                            serviceListener: ConfServiceUtils.ConfServiceListener,
                            tag: String,
                            backgroundWork: Boolean) {
        saveSpecificValues(synchr)

        if (context is MainActivity) {
            context.hideSnackBar(Constants.SNACKBAR_CERT)
        }

        CoroutineScope(Dispatchers.Default).launch {
            Analytics.sendAnalytics(serviceListener, tag, backgroundWork)
        }
        CoroutineScope(Dispatchers.Default).launch {
            Config.getConfig(serviceListener, tag, backgroundWork)
        }
        CoroutineScope(Dispatchers.Default).launch {
            supervisorScope {
                try {
                    val saveDccCertificates =
                        async { CertificateDcc.saveDccCertificates(synchr?.certificatesDCC) }

                    if(JWTUtils.isPremium()) {
                        val loadRules = async { EngineManager.rulesRepository.loadRules("") }
                        val loadValuesSets = async { EngineManager.valueSetsRepository.preLoad("") }
                        val loadCountries =
                            async { EngineManager.countriesRepository.preLoadCountries("") }
                        loadRules.await()
                        loadValuesSets.await()
                        loadCountries.await()
                    }
                    saveDccCertificates.await()

                    resetPrefs(synchr)
                    EngineManager.blacklistRepository.loadBlacklistPageWithCondition()
                    CountryUtils.reloadCountries()

                    //Notify the caller (SyncWorker or SettingsFragment)
                    withContext(Dispatchers.Main) {
                        listener.onSynchronizationDone()
                        FileLogger.write(Date().toString() + " END \n\n", context)
                    }
                } catch (e: Exception) {
                    FileLogger.write(e.stackTraceToString(),context)

                    Log.i(
                        TAG,
                        "Error while loading the Rules, Sets or Countries : ${e.stackTraceToString()}"
                    )
                    listener.onSynchronizationError()
                }
            }
        }
    }

    fun saveSpecificValues(
        synchr: SyncResult?
    ) {
        PublicKey2ddoc.saveCertificates2DDoc(synchr?.certificates2DDoc)
        Blacklist.saveBlacklist(synchr?.specificValues?.blacklist)
        Expiration.saveExpirationActivation(synchr?.specificValues?.useExpirationDate)
        Labels.saveLabels(synchr?.specificValues?.labels)
    }

    fun resetPrefs(synchr: SyncResult?, lastSyncDate: Long = Date().time) {
        val pref = PreferenceManager.getDefaultSharedPreferences(App.getContext())
        val gson = Gson()

        with(pref.edit()) {
            clear()
            synchr?.period?.frequency?.let { putLong(Step.CERT_FREQUENCY.text, it) }
            synchr?.period?.step1?.let { putLong(Step.CERT_STEP_1.text, it) }
            synchr?.period?.step2?.let { putLong(Step.CERT_STEP_2.text, it) }
            synchr?.period?.step3?.let { putLong(Step.CERT_STEP_3.text, it) }
            synchr?.specificValues?.let { values ->
                values.validity?.vaccine?.let { editValidity(it, ControlMode.VACCINE.prefix, this) }
                values.validity?.health?.let { editValidity(it, ControlMode.HEALTH.prefix, this) }
                values.validity?.plus?.let { editValidity(it, ControlMode.PLUS.prefix, this) }

                values.validity?.vaccinePassStartDate?.let {
                    putString(VACCINE_PASS_START_DATE.text, it)
                }

                values.testAntigenicList?.let {
                    putString(TEST_ANTIGENIC.text, gson.toJson(it))
                }
                values.testPcrList?.let { putString(TEST_PCR.text, gson.toJson(it)) }
                values.testManufacturerList?.let {
                    putString(TEST_MANUFACTURER.text, gson.toJson(it))
                }
                values.vaccineManufacturerList?.let {
                    putString(VACCINE_MANUFACTURER.text, gson.toJson(it))
                }
                values.vaccineMedicalProductList?.let {
                    putString(VACCINE_MEDICAL_PRODUCT.text, gson.toJson(it))
                }
                values.vaccineProphylaxisList?.let {
                    putString(VACCINE_PROPHYLAXIS.text, gson.toJson(it))
                }

                values.notGreenCountries?.let { putStringSet(NOT_GREEN_COUNTRIES, it.toSet()) }

                values.ruleEngine?.messageOtModeFR?.let { putString(RULE_ENGINE_MESSAGE_OT, it) }
            }
            putLong(Constants.LAST_SYNC_DATE, lastSyncDate)
            apply()
        }

        //Cas: init local a échoué mais sync OK
        val prefsSync = App.getContext().getSharedPreferences(Constants.SYNC_KEY, Context.MODE_PRIVATE)
        with(prefsSync.edit()) {
            putBoolean(
                Constants.KEY_LAST_FORCED_UPDATE,
                true
            )
            apply()
        }
    }

    private fun editValidity(validity: SyncValidityMode, prefix: String, editor: SharedPreferences.Editor) {
        validity.recoveryEndDay?.let {
            editor.putInt(prefix + RECOVERY_END_DAY.text, it)
        }
        validity.recoveryStartDay?.let {
            editor.putInt(prefix + RECOVERY_START_DAY.text, it)
        }

        validity.recoveryDelayMaxTV?.let {
            editor.putInt(prefix + RECOVER_DELAY_MAX_TV.text, it)
        }
        validity.recoveryDelayMax?.let {
            editor.putInt(prefix + RECOVER_DELAY_MAX.text, it)
        }
        validity.recoveryDelayMaxUnderAge?.let {
            editor.putInt(prefix + RECOVER_DELAY_MAX_UNDER_AGE.text, it)
        }
        validity.recoveryAcceptanceAgePeriod?.let {
            editor.putString(prefix + RECOVERY_ACCEPTANCE_AGE_PERIOD.text, it)
        }

        validity.testNegativeAntigenicEndHour?.let {
            editor.putInt(prefix + TEST_NEGATIVE_ANTIGENIC_END_HOUR.text, it)
        }
        validity.testNegativePcrEndHour?.let {
            editor.putInt(prefix + TEST_NEGATIVE_PCR_END_HOUR.text, it)
        }
        validity.testPositiveAntigenicEndDay?.let {
            editor.putInt(prefix + TEST_POSITIVE_ANTIGENIC_END_DAY.text, it)
        }
        validity.testPositiveAntigenicStartDay?.let {
            editor.putInt(prefix + TEST_POSITIVE_ANTIGENIC_START_DAY.text, it)
        }
        validity.testPositivePcrEndDay?.let {
            editor.putInt(prefix + TEST_POSITIVE_PCR_END_DAY.text, it)
        }
        validity.testPositivePcrStartDay?.let {
            editor.putInt(prefix + TEST_POSITIVE_PCR_START_DAY.text, it)
        }

        validity.testAcceptanceAgePeriod?.let {
            editor.putString(prefix + TEST_ACCEPTANCE_AGE_PERIOD.text, it)
        }
        validity.vaccineDelay?.let { editor.putInt(prefix + VACCINE_DELAY.text, it) }
        validity.vaccineDelayMax?.let { editor.putInt(prefix + VACCINE_DELAY_MAX.text, it) }
        validity.vaccineDelayMaxRecovery?.let {
            editor.putInt(prefix + VACCINE_DELAY_MAX_RECOVERY.text, it)
        }
        validity.vaccineDelayJanssen1?.let { editor.putInt(prefix + VACCINE_DELAY_JANSSEN_1.text, it) }
        validity.vaccineDelayMaxJanssen1?.let {
            editor.putInt(prefix + VACCINE_DELAY_MAX_JANSSEN_1.text, it)
        }
        validity.vaccineBoosterDelayNew?.let {
            editor.putInt(prefix + VACCINE_BOOSTER_DELAY_NEW.text, it)
        }
        validity.vaccineBoosterDelayMax?.let {
            editor.putInt(prefix + VACCINE_BOOSTER_DELAY_MAX.text, it)
        }
        validity.vaccineBoosterDelayUnderAgeNew?.let {
            editor.putInt(prefix + VACCINE_BOOSTER_DELAY_UNDER_AGE_NEW.text, it)
        }
        validity.vaccineBoosterAgePeriod?.let {
            editor.putString(prefix + VACCINE_BOOSTER_AGE_PERIOD.text, it)
        }
        validity.vaccineDelayNovavax?.let {
            editor.putInt(prefix + VACCINE_DELAY_NOVAVAX.text, it)
        }
        validity.vaccineDelayMaxRecoveryNovavax?.let {
            editor.putInt(prefix + VACCINE_DELAY_MAX_RECOVERY_NOVAVAX.text, it)
        }
        validity.vaccineDelayMaxNovavax?.let {
            editor.putInt(prefix + VACCINE_DELAY_MAX_NOVAVAX.text, it)
        }
        validity.vaccineBoosterDelayMaxNovavax?.let {
            editor.putInt(prefix + VACCINE_BOOSTER_DELAY_MAX_NOVAVAX.text, it)
        }
        validity.vaccineDelayJanssen2?.let {
            editor.putInt(prefix + VACCINE_DELAY_JANSSEN_2.text, it)
        }
        validity.vaccineDelayMaxJanssen2?.let {
            editor.putInt(prefix + VACCINE_DELAY_MAX_JANSSEN_2.text, it)
        }
        validity.vaccineBoosterDelayMaxJanssen?.let {
            editor.putInt(prefix + VACCINE_BOOSTER_DELAY_MAX_JANSSEN.text, it)
        }
    }

    fun startWorker() {

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val syncRequest =
            PeriodicWorkRequestBuilder<SyncWorker>(15, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .setInitialDelay(15, TimeUnit.MINUTES)
                .addTag(TAG)
                .build()

        WorkManager
            .getInstance(App.getContext())
            .enqueueUniquePeriodicWork(
                UNIQUE_WORK_NAME,
                ExistingPeriodicWorkPolicy.REPLACE,
                syncRequest)
    }

    suspend fun initializeEngineDatas() : Boolean {
        val rulesInit = EngineManager.rulesRepository.initDatas()
        val vSetsInit = EngineManager.valueSetsRepository.initDatas()
        val countriesInit = EngineManager.countriesRepository.initDatas()
        val certifInit = EngineManager.certificatesRepository.initDatas()
        val blacklistInit = EngineManager.blacklistRepository.initDatas()

        return rulesInit && vSetsInit && countriesInit && certifInit && blacklistInit
    }

    interface SyncListener {
        fun onSynchronizationDone()
        fun onSynchronizationError()
    }
}