package com.ingroupe.verify.anticovid.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.recyclerview.widget.RecyclerView
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.common.model.ResultScan
import com.ingroupe.verify.anticovid.databinding.*
import com.ingroupe.verify.anticovid.synchronization.elements.Blacklist
import com.ingroupe.verify.anticovid.ui.result.ResultExpandableListAdapter


class ResultScanAdapter(
    private val myDataset: ResultScan,
    private val adapterInterfaceClick: IAdapterInterfaceClick
) :
    RecyclerView.Adapter<ResultScanAdapter.MyViewHolder>(), View.OnClickListener {


    override fun onClick(v: View?) {
        v?.let {
            adapterInterfaceClick.onItemClick(it.tag as Int)
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    open class MyViewHolder(open val binding:  androidx.viewbinding.ViewBinding) :
        RecyclerView.ViewHolder(binding.root)


    override fun getItemViewType(position: Int): Int {
        if (myDataset.datas[position].isGroupHeader) {
            return R.layout.result_header_view
        }
        return when (myDataset.datas[position].format) {
            Constants.CODE_VALIDITY_OK -> R.layout.result_validity_ok_view
            Constants.CODE_VALIDITY_KO -> R.layout.result_validity_ko_view
            Constants.CODE_VALIDITY_WARN -> R.layout.result_validity_warn_view
            Constants.CODE_VALIDITY_INFO -> R.layout.result_validity_info_view
            Constants.CODE_VALIDITY_INFO_DETAIL_OK -> R.layout.result_validity_info_detail_ok_view
            Constants.CODE_VALIDITY_INFO_DETAIL_KO -> R.layout.result_validity_info_detail_ko_view
            Constants.CODE_CHECK_1 -> R.layout.result_data_check_1_view
            Constants.CODE_CHECK_2 -> R.layout.result_data_check_2_view
            Constants.CODE_CHECK_3 -> R.layout.result_data_check_3_view
            Constants.CODE_ICONE_1 -> R.layout.result_data_icone_1_view
            Constants.CODE_FORMAT_1 -> R.layout.result_data_format_1_view
            Constants.CODE_FORMAT_2 -> R.layout.result_data_format_2_view
            Constants.CODE_FORMAT_3 -> R.layout.result_data_format_3_view
            Constants.CODE_HIDDEN -> R.layout.result_data_hidden_view
            Constants.CODE_WARN_TEXT_1 -> R.layout.result_warn_text_1_view
            Constants.CODE_WARN_WEBVIEW_1 -> R.layout.result_warn_webview_view
            Constants.CODE_COUNTRIES -> R.layout.result_scan_countries
            else -> R.layout.result_data_format_1_view
        }
    }

    // Create new views (invoked by the manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {

        return when(viewType) {
            R.layout.result_header_view -> MyViewHolder(ResultHeaderViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_validity_ok_view -> MyViewHolder(ResultValidityOkViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_validity_ko_view -> MyViewHolder(ResultValidityKoViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_validity_warn_view -> MyViewHolder(ResultValidityWarnViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_validity_info_view -> MyViewHolder(ResultValidityInfoViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_validity_info_detail_ok_view -> MyViewHolder(ResultValidityInfoDetailOkViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_validity_info_detail_ko_view -> MyViewHolder(ResultValidityInfoDetailKoViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_data_check_1_view -> MyViewHolder(ResultDataCheck1ViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_data_check_2_view -> MyViewHolder(ResultDataCheck2ViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_data_check_3_view -> MyViewHolder(ResultDataCheck3ViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_data_icone_1_view -> MyViewHolder(ResultDataIcone1ViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_data_format_1_view -> MyViewHolder(ResultDataFormat1ViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_data_format_2_view -> MyViewHolder(ResultDataFormat2ViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_data_format_3_view -> MyViewHolder(ResultDataFormat3ViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_data_hidden_view -> MyViewHolder(ResultDataHiddenViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_warn_text_1_view -> MyViewHolder(ResultWarnText1ViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_warn_webview_view -> MyViewHolder(ResultWarnWebviewViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            R.layout.result_scan_countries -> MyViewHolder(ResultScanCountriesBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> MyViewHolder(ResultDataFormat1ViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }
    }

    // Replace the contents of a view (invoked by the manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        val resultData = myDataset.datas[position]

        when (holder.binding) {
            is ResultHeaderViewBinding -> (holder.binding as ResultHeaderViewBinding).textViewGroupName.text = resultData.label
            is ResultValidityOkViewBinding -> (holder.binding as ResultValidityOkViewBinding).textViewValidity.text = resultData.value
            is ResultValidityKoViewBinding -> (holder.binding as ResultValidityKoViewBinding).textViewValidity.text = resultData.value
            is ResultValidityWarnViewBinding -> (holder.binding as ResultValidityWarnViewBinding).textViewValidity.text = resultData.value
            is ResultValidityInfoViewBinding -> (holder.binding as ResultValidityInfoViewBinding).textViewValidity.text = resultData.value
            is ResultValidityInfoDetailOkViewBinding -> {
                (holder.binding as ResultValidityInfoDetailOkViewBinding).textViewValidity.text = resultData.value
                (holder.binding as ResultValidityInfoDetailOkViewBinding).textViewValiditySubtitle.text = resultData.subValue
            }
            is ResultValidityInfoDetailKoViewBinding -> {
                (holder.binding as ResultValidityInfoDetailKoViewBinding).textViewValidity.text = resultData.value
                (holder.binding as ResultValidityInfoDetailKoViewBinding).textViewValiditySubtitle.text = resultData.subValue
            }
            is ResultDataCheck1ViewBinding -> {
                (holder.binding as ResultDataCheck1ViewBinding).textViewLabel.text = resultData.label
                (holder.binding as ResultDataCheck1ViewBinding).textViewValue.text = resultData.value
                isWithBackground(holder.binding.root, resultData.withBackground)
            }
            is ResultDataCheck2ViewBinding -> {
                (holder.binding as ResultDataCheck2ViewBinding).textViewLabel.text = resultData.label
                (holder.binding as ResultDataCheck2ViewBinding).textViewValue.text = resultData.value
                isWithBackground(holder.binding.root, resultData.withBackground)
            }
            is ResultDataCheck3ViewBinding -> {
                (holder.binding as ResultDataCheck3ViewBinding).textViewLabel.text = resultData.label
                (holder.binding as ResultDataCheck3ViewBinding).textViewValue.text = resultData.value
                isWithBackground(holder.binding.root, resultData.withBackground)
            }
            is ResultDataIcone1ViewBinding -> {
                (holder.binding as ResultDataIcone1ViewBinding).textViewLabel.text = resultData.label
                (holder.binding as ResultDataIcone1ViewBinding).textViewValue.text = resultData.value
                isWithBackground(holder.binding.root, resultData.withBackground)
            }
            is ResultDataFormat1ViewBinding ->{
                (holder.binding as ResultDataFormat1ViewBinding).textViewLabel.text = resultData.label
                (holder.binding as ResultDataFormat1ViewBinding).textViewValue.text = resultData.value
                isWithBackground(holder.binding.root, resultData.withBackground)
            }
            is ResultDataFormat2ViewBinding -> {
                (holder.binding as ResultDataFormat2ViewBinding).textViewLabel.text = resultData.label
                (holder.binding as ResultDataFormat2ViewBinding).textViewValue.text = resultData.value
                isWithBackground(holder.binding.root, resultData.withBackground)
            }
            is ResultDataFormat3ViewBinding -> {
                (holder.binding as ResultDataFormat3ViewBinding).textViewLabel.text = resultData.label
                (holder.binding as ResultDataFormat3ViewBinding).textViewValue.text = resultData.value
                isWithBackground(holder.binding.root, resultData.withBackground)
            }
            is ResultWarnText1ViewBinding -> {
                (holder.binding as ResultWarnText1ViewBinding).textViewValue.text = resultData.value
                isWithBackground(holder.binding.root, resultData.withBackground)
            }
            is ResultWarnWebviewViewBinding -> {
                val messageEnum = Blacklist.Message.valueOf(resultData.value)
                (holder.binding as ResultWarnWebviewViewBinding).textViewMessageTitle.text = Blacklist.getMessageTitle(messageEnum)
                (holder.binding as ResultWarnWebviewViewBinding).webViewMessageDetail.webViewClient = object : WebViewClient() {
                    override fun onPageFinished(view: WebView?, url: String?) {
                        super.onPageFinished(view, url)
                        (holder.binding as ResultWarnWebviewViewBinding).root.requestLayout()
                    }
                }
                (holder.binding as ResultWarnWebviewViewBinding).webViewMessageDetail.loadData(Blacklist.getMessageDetail(messageEnum), "text/html", "base64")
                (holder.binding as ResultWarnWebviewViewBinding).webViewMessageDetail.setBackgroundColor(Color.TRANSPARENT)
                isWithBackground(holder.binding.root, resultData.withBackground)

                (holder.binding as ResultWarnWebviewViewBinding).constraintLayoutMessage.setOnClickListener {
                    (holder.binding as ResultWarnWebviewViewBinding).webViewMessageDetail.visibility =
                        if((holder.binding as ResultWarnWebviewViewBinding).webViewMessageDetail.visibility == View.VISIBLE) View.GONE else View.VISIBLE
                }
            }
            is ResultScanCountriesBinding -> {
                resultData.engineResultDatas?.let { datas ->
                    val expandableListAdapter = ResultExpandableListAdapter(holder.binding.root.context, datas)
                    val view = (holder.binding as ResultScanCountriesBinding).explistViewCountries
                    view.setAdapter(expandableListAdapter)
                    Utils.setExpandableInitialHeight(view, holder.binding.root.context)

                    view.setOnGroupClickListener { _, _, groupPosition, _ ->
                        Utils.setExpandableViewHeight(view, groupPosition)
                        false
                    }
                }
            }
        }

        holder.binding.root.tag = position
        holder.binding.root.setOnClickListener(this)
    }

    private fun isWithBackground(view: View, withBackground: Boolean) {
        if (withBackground) {
            view.setBackgroundColor(Color.parseColor("#08000000"))
        } else {
            view.setBackgroundColor(Color.parseColor("#00000000"))
        }

    }

    override fun getItemCount() = myDataset.datas.size
}