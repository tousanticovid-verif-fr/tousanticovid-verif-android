package com.ingroupe.verify.anticovid.service.api.configuration.sync

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SyncExpDate (
    @SerializedName("forVaccine")
    var forVaccine: Boolean? = null,
    @SerializedName("forTest")
    var forTest: Boolean? = null,
    @SerializedName("forRecovery")
    var forRecovery: Boolean? = null,
    @SerializedName("forExemption")
    var forExemption: Boolean? = null

): Serializable
