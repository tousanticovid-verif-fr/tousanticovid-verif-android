package com.ingroupe.verify.anticovid.synchronization

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.ingroupe.verify.anticovid.common.FileLogger
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncResult
import com.ingroupe.verify.anticovid.synchronization.elements.Blacklist
import java.util.*

class SyncWorker(appContext: Context, workerParams: WorkerParameters):
    CoroutineWorker(appContext, workerParams), ConfServiceUtils.ConfServiceListener,
    SynchronisationUtils.SyncListener {

    companion object {
        const val TAG = "SyncWorkerTag"
    }


    override suspend fun doWork(): Result {
        if(SynchronisationUtils.isSyncNeeded()) {
            ConfServiceUtils.callSynchronization(
                this,
                "SyncWorkerTag",
                true
            )
        }

        Blacklist.cleanDuplicateMap()
        return Result.success()
    }

    override fun showErrorMessage(title: Int, message: Int) {
        Log.i(TAG, "Error during syncWork")
    }

    override fun saveResult(syncResult: SyncResult?) {
        SynchronisationUtils.saveSynchronization(
            syncResult,
            applicationContext,
            this,
            this,
            TAG,
            true
        )
    }

    override fun showLoading(show: Boolean) {
        // nothing to do
    }

    override fun onSynchronizationDone() {
        // nothing to do
    }

    override fun onSynchronizationError() {
        // nothing to do
    }
}
