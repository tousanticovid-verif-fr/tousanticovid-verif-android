package com.ingroupe.verify.anticovid.synchronization.elements

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ingroupe.verify.anticovid.App
import com.ingroupe.verify.anticovid.R
import com.ingroupe.verify.anticovid.common.Constants
import com.ingroupe.verify.anticovid.common.Utils
import com.ingroupe.verify.anticovid.data.EngineManager
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncBlacklist
import com.ingroupe.verify.anticovid.service.api.configuration.sync.SyncExclusionTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import java.time.*
import java.time.format.DateTimeFormatter
import java.util.*

object Blacklist {

    enum class BlacklistType(val text: String, val typeForUrl: String) {
        BLACKLIST_DCC("BLACKLIST_DCC", "dcc"),
        BLACKLIST_2DDOC("BLACKLIST_2DDOC", "2ddoc")
    }

    enum class Message(val keyTitle: Int, val keyDetail: Int) {
        BLACKLIST_LITE(R.string.result_anomaly_blacklist_lite_title_key, R.string.result_anomaly_blacklist_lite_detail_key),
        BLACKLIST_OT(R.string.result_anomaly_blacklist_ot_title_key, R.string.result_anomaly_blacklist_ot_detail_key),
        DUPLICATE(R.string.result_anomaly_duplicate_title_key, R.string.result_anomaly_duplicate_detail_key)
    }

    private const val DUPLICATE_ACTIVATE = "DUPLICATE_ACTIVATE"
    private const val DUPLICATE_RETENTION_PERIOD = "DUPLICATE_RETENTION_PERIOD"

    enum class BlacklistIntValue(val text: String, val default: Int) {
        LAST_INDEX_BLACKLIST_DCC("lastIndexBlacklistDcc", 0),
        LAST_INDEX_BLACKLIST_2DDOC("lastIndexBlacklist2ddoc", 0),
        EXCLUSION_QUANTITY_LIMIT("exclusionQuantityLimit", 15000),
        REFRESH_VERSION("refreshVersion", 0)
    }
    private const val EXCLUSION_TIME = "exclusionTime"

    private val mapHashDuplicate : MutableMap<String, LocalDateTime> = mutableMapOf()


    fun saveBlacklist(
        syncBlacklist: SyncBlacklist?
    ) {
        val blacklistPref =
            App.getContext().getSharedPreferences(Constants.BLACKLIST_KEY, Context.MODE_PRIVATE)

        val keyBlackListLiteTitleFR = Utils.getLocalizedResources(Locale.FRENCH).getString(Message.BLACKLIST_LITE.keyTitle)
        val keyBlackListLiteDetailFR = Utils.getLocalizedResources(Locale.FRENCH).getString(Message.BLACKLIST_LITE.keyDetail)
        val keyBlackListLiteTitleEN = Utils.getLocalizedResources(Locale.ENGLISH).getString(Message.BLACKLIST_LITE.keyTitle)
        val keyBlackListLiteDetailEN = Utils.getLocalizedResources(Locale.ENGLISH).getString(Message.BLACKLIST_LITE.keyDetail)

        val keyBlackListOtTitleFR = Utils.getLocalizedResources(Locale.FRENCH).getString(Message.BLACKLIST_OT.keyTitle)
        val keyBlackListOtDetailFR = Utils.getLocalizedResources(Locale.FRENCH).getString(Message.BLACKLIST_OT.keyDetail)
        val keyBlackListOtTitleEN = Utils.getLocalizedResources(Locale.ENGLISH).getString(Message.BLACKLIST_OT.keyTitle)
        val keyBlackListOtDetailEN = Utils.getLocalizedResources(Locale.ENGLISH).getString(Message.BLACKLIST_OT.keyDetail)

        val keyDuplicateTitleFR = Utils.getLocalizedResources(Locale.FRENCH).getString(Message.DUPLICATE.keyTitle)
        val keyDuplicateDetailFR = Utils.getLocalizedResources(Locale.FRENCH).getString(Message.DUPLICATE.keyDetail)
        val keyDuplicateTitleEN = Utils.getLocalizedResources(Locale.ENGLISH).getString(Message.DUPLICATE.keyTitle)
        val keyDuplicateDetailEN = Utils.getLocalizedResources(Locale.ENGLISH).getString(Message.DUPLICATE.keyDetail)

        val gson = Gson()

        val oldRefreshVersion = getBlacklistIntValue(BlacklistIntValue.REFRESH_VERSION)
        val refreshIndexDcc = syncBlacklist?.refreshIndex
        val refreshIndex2ddoc = syncBlacklist?.refreshIndex2ddoc

        runBlocking(Dispatchers.IO) {
            if(syncBlacklist?.refreshVersion ?: 0 > oldRefreshVersion && refreshIndexDcc != null) {
                EngineManager.blacklistRepository.deleteFromId(BlacklistType.BLACKLIST_DCC, refreshIndexDcc)
            }
            if(syncBlacklist?.refreshVersion ?: 0 > oldRefreshVersion && refreshIndex2ddoc != null) {
                EngineManager.blacklistRepository.deleteFromId(BlacklistType.BLACKLIST_2DDOC, refreshIndex2ddoc)
            }
        }

        with(blacklistPref.edit()) {
            clear()
            syncBlacklist?.messages?.blacklistLite?.titleFR?.let { putString(keyBlackListLiteTitleFR, it) }
            syncBlacklist?.messages?.blacklistLite?.detailFR?.let { putString(keyBlackListLiteDetailFR, it) }
            syncBlacklist?.messages?.blacklistLite?.titleEN?.let { putString(keyBlackListLiteTitleEN, it) }
            syncBlacklist?.messages?.blacklistLite?.detailEN?.let { putString(keyBlackListLiteDetailEN, it) }

            syncBlacklist?.messages?.blacklistOT?.titleFR?.let { putString(keyBlackListOtTitleFR, it) }
            syncBlacklist?.messages?.blacklistOT?.detailFR?.let { putString(keyBlackListOtDetailFR, it) }
            syncBlacklist?.messages?.blacklistOT?.titleEN?.let { putString(keyBlackListOtTitleEN, it) }
            syncBlacklist?.messages?.blacklistOT?.detailEN?.let { putString(keyBlackListOtDetailEN, it) }

            syncBlacklist?.messages?.duplicate?.titleFR?.let { putString(keyDuplicateTitleFR, it) }
            syncBlacklist?.messages?.duplicate?.detailFR?.let { putString(keyDuplicateDetailFR, it) }
            syncBlacklist?.messages?.duplicate?.titleEN?.let { putString(keyDuplicateTitleEN, it) }
            syncBlacklist?.messages?.duplicate?.detailEN?.let { putString(keyDuplicateDetailEN, it) }

            syncBlacklist?.duplicateActivate?.let { putBoolean(DUPLICATE_ACTIVATE, it)}
            syncBlacklist?.duplicateRetentionPeriod?.let { putLong(DUPLICATE_RETENTION_PERIOD, it)}

            syncBlacklist?.lastIndexBlacklist?.let { putInt(BlacklistIntValue.LAST_INDEX_BLACKLIST_DCC.text, it)}
            syncBlacklist?.lastIndexBlacklist2ddoc?.let { putInt(BlacklistIntValue.LAST_INDEX_BLACKLIST_2DDOC.text, it)}
            syncBlacklist?.exclusionTime?.let { putString(EXCLUSION_TIME, gson.toJson(it))}
            syncBlacklist?.exclusionQuantityLimit?.let { putInt(BlacklistIntValue.EXCLUSION_QUANTITY_LIMIT.text, it)}
            syncBlacklist?.refreshVersion?.let { putInt(BlacklistIntValue.REFRESH_VERSION.text, it)}

            apply()
        }
    }

    fun saveLastIndexBlacklist(blacklistType: BlacklistType, lastIndexBlacklist: Int?) {
        val blacklistPref =
            App.getContext().getSharedPreferences(Constants.BLACKLIST_KEY, Context.MODE_PRIVATE)

        with(blacklistPref.edit()) {
            when(blacklistType) {
                BlacklistType.BLACKLIST_DCC ->
                    lastIndexBlacklist?.let { putInt(BlacklistIntValue.LAST_INDEX_BLACKLIST_DCC.text, it)}
                BlacklistType.BLACKLIST_2DDOC ->
                    lastIndexBlacklist?.let { putInt(BlacklistIntValue.LAST_INDEX_BLACKLIST_2DDOC.text, it)}
            }
            apply()
        }
    }

    fun getMessageTitle(messageEnum: Message): String {
        val blacklistPref =
            App.getContext().getSharedPreferences(Constants.BLACKLIST_KEY, Context.MODE_PRIVATE)
        return blacklistPref.getString(App.getContext().getString(messageEnum.keyTitle), "") ?: ""
    }

    fun getMessageDetail(messageEnum: Message): String {
        val blacklistPref =
            App.getContext().getSharedPreferences(Constants.BLACKLIST_KEY, Context.MODE_PRIVATE)
        return blacklistPref.getString(App.getContext().getString(messageEnum.keyDetail), "") ?: ""
    }

    fun isBlacklistUpToDate(blacklistType: BlacklistType): Boolean {
        // verif lastIndexBlacklist VS currentLastIndex
        val lastBackIndex = when(blacklistType) {
            BlacklistType.BLACKLIST_DCC ->
                getBlacklistIntValue(BlacklistIntValue.LAST_INDEX_BLACKLIST_DCC)
            BlacklistType.BLACKLIST_2DDOC ->
                getBlacklistIntValue(BlacklistIntValue.LAST_INDEX_BLACKLIST_2DDOC)
        }

        val lastLocalIndex = EngineManager.blacklistRepository.getLastIndex(blacklistType)

        return lastBackIndex <= lastLocalIndex
    }

    fun isBlacklistSyncToDo(blacklistType: BlacklistType): Boolean {

        if(isBlacklistUpToDate(blacklistType)) {
            return false
        }

        val lastBackIndex = when(blacklistType) {
            BlacklistType.BLACKLIST_DCC ->
                getBlacklistIntValue(BlacklistIntValue.LAST_INDEX_BLACKLIST_DCC)
            BlacklistType.BLACKLIST_2DDOC ->
                getBlacklistIntValue(BlacklistIntValue.LAST_INDEX_BLACKLIST_2DDOC)
        }
        val lastLocalIndex = EngineManager.blacklistRepository.getLastIndex(blacklistType)
        // vérif heure VS exclusionTime OU (currentLastIndex - lastIndexBlacklist > exclusionQuantityLimit)
        if(isInExclusionTime() && (lastBackIndex-lastLocalIndex < getBlacklistIntValue(BlacklistIntValue.EXCLUSION_QUANTITY_LIMIT))) {
            return false
        }

        return true
    }

    fun getBlacklistIntValue(bivEnum: BlacklistIntValue) : Int {
        val blacklistPref =
            App.getContext().getSharedPreferences(Constants.BLACKLIST_KEY, Context.MODE_PRIVATE)
        return blacklistPref.getInt(bivEnum.text, bivEnum.default)
    }

    private fun isInExclusionTime(): Boolean {
        val blacklistPref =
            App.getContext().getSharedPreferences(Constants.BLACKLIST_KEY, Context.MODE_PRIVATE)
        val jsonMap = blacklistPref.getString(EXCLUSION_TIME, "[]")

        val gson = Gson()
        val itemType = object : TypeToken<List<SyncExclusionTime>>() {}.type
        val listExclusionTime: List<SyncExclusionTime> = gson.fromJson(jsonMap, itemType)

        listExclusionTime.forEach { set ->
            val start: OffsetTime = OffsetTime.parse(set.start, DateTimeFormatter.ISO_OFFSET_TIME)
            val end: OffsetTime = OffsetTime.parse(set.end, DateTimeFormatter.ISO_OFFSET_TIME)
            if(OffsetTime.now().isAfter(start) && OffsetTime.now().isBefore(end)) {
                return true
            }
        }
        return false
    }


    fun isBlacklisted(blacklistType: BlacklistType, hash: String): Boolean {

        return runBlocking (Dispatchers.IO) {
            EngineManager.blacklistRepository.isHashBlacklisted(blacklistType, hash)
        }
    }

    private fun isDuplicateActivated(): Boolean {
        val blacklistPref =
            App.getContext().getSharedPreferences(Constants.BLACKLIST_KEY, Context.MODE_PRIVATE)
        return blacklistPref.getBoolean(DUPLICATE_ACTIVATE, false)
    }

    private fun getDuplicateRetentionPeriod(): Long {
        val blacklistPref =
            App.getContext().getSharedPreferences(Constants.BLACKLIST_KEY, Context.MODE_PRIVATE)
        return blacklistPref.getLong(DUPLICATE_RETENTION_PERIOD, 5)
    }

    fun isHashDuplicate(hash: String) : Boolean {
        cleanDuplicateMap()

        if(!isDuplicateActivated()) {
            return false
        }

        var present = false
        if(mapHashDuplicate[hash] != null) {
            present = true
        }
        mapHashDuplicate[hash] = LocalDateTime.now()

        return present
    }

    fun cleanDuplicateMap() {
        val currentDateTime = LocalDateTime.now()
        val limitDateTime = currentDateTime.minusMinutes(getDuplicateRetentionPeriod())
        mapHashDuplicate.values.removeIf { it < limitDateTime }
    }
}