package com.ingroupe.verify.anticovid.ui.statistic

import com.ingroupe.verify.anticovid.common.model.StatsDay
import com.ingroupe.verify.anticovid.common.model.StatsPeriod
import java.time.LocalDate

class StatisticPresenterImpl(
    private val view: StatisticView
) : StatisticPresenter {

    companion object {
        const val TAG = "StatisticP"
    }

    override fun formatMap(statPeriod :StatsPeriod): Map<Long, StatsDay> {

        val newMap = mutableMapOf<Long, StatsDay>()

        for(i in 0L..14L) {
            val date = LocalDate.now().minusDays(i)
            newMap[date.toEpochDay()] = statPeriod.getStatsDay(date)
        }
        return newMap
    }

}