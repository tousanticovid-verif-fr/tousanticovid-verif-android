package com.ingroupe.verify.anticovid.ui.actionchoice.configuration

import com.ingroupe.verify.anticovid.common.Constants
import java.time.ZonedDateTime

interface ConfPresenter {
    fun initTravelType(type: Constants.TravelType)
    fun getControlCountryAndUpdateModel() : String
    fun getPickedFavoritesAndUpdateModel(keySharedPrefs: String) : Set<String>
    fun updateTimePicked(time: ZonedDateTime?)
}