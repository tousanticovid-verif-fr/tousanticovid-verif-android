![IN Groupe](https://res.cloudinary.com/ingroupe/image/upload/v1576850289/ingroupe-logo-mini.png)

Le code présenté ci-dessous est celui de l'application TAC-Verif pour les OS Android et iOS
=====================
Ce code est développé par IN Groupe et publié sous une licence propre à IN Groupe disponible ici : [LICENSE](LICENSE), que toute personne s'engage à consulter et respecter avant toute utilisation du code objet de la présente publication. 


Usage et fonctionnalité
-------------------

Le code ainsi publié est celui de l'application TAC Verif dont l'usage est strictement réservé aux personnes habilitées et services autorisée dans le cadre de la Loi de Sortie de l'Etat d'Urgence Sanitaire du 2 juin 2021, article 1 et de ces décrets d'applications [https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000043618403](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000043618403)


Cette application dispose de deux modes de fonctionnement :

    * Un mode lite destiné à des vérifications courantes, correspondant au pass Activités. Dans ce mode, sont affichés les noms, prénoms et date de naissance de la personne concernée par le justificatif, ainsi qu'un résultat positif ou négatif de détention d'un justificatif conforme. Le mode Lite est le mode d’usage par défaut de l’application.

    * Un mode étendu qui affiche des données complémentaires (cycle vaccinal, date du test) contenues dans le pass sanitaire (2D DOC ou QR européen) et restreint aux seules catégories de personnes dûment habilitées au titre de la réglementation en vigueur (opérateurs de transport agréés par le Ministère de la Santé). L’activation du mode étendu nécessite une procédure particulière afin d’en réserver l’usage aux seules personnes habilitées.

Le mode lite et le mode étendu fonctionnent en local sur le smartphone et ne nécessitent pas de connexion à Internet pour procéder au contrôle.


Modalité de publication
-------------------

Le code publié contient l'ensemble des règles de gestion des modes lite et mode détaillé.
Les éléments de code relatifs à l'activation du mode étendu sont volontairement omis de cette publication pour des raisons de sécurité.

Applications & roadmap
-------------------

Les applications Android et iOS rendent les mêmes fonctionnalités. Elles ont été developpées à partir de sources distinctes qui peut expliquer les différences entre les deux codes (au delà des aspects techniques)

Les applications vont évoluer avec, entre autres, un point déjà identifié : le remplacement de la Librairie MLKit


Licences
-------------------

___Apache 2.0___

*Component*: Retrofit
*License Text URL*: [https://github.com/square/retrofit/blob/master/LICENSE.txt](https://github.com/square/retrofit/blob/master/LICENSE.txt)
*Source Code*: [https://github.com/square/retrofit](https://github.com/square/retrofit)

*Component*: EventBus
*License Text URL*: [https://github.com/greenrobot/EventBus/blob/master/LICENSE](https://github.com/greenrobot/EventBus/blob/master/LICENSE)
*Source Code*: [https://github.com/greenrobot/EventBus](https://github.com/greenrobot/EventBus)

*Component*: Anko
*License Text URL*: [https://github.com/Kotlin/anko/blob/master/LICENSE](https://github.com/Kotlin/anko/blob/master/LICENSE)
*Source Code*: [https://github.com/Kotlin/anko](https://github.com/Kotlin/anko)

*Component*: Apache Commons Codec
*License Text URL*: [https://github.com/apache/commons-codec/blob/master/LICENSE.txt](https://github.com/apache/commons-codec/blob/master/LICENSE.txt)
*Source Code*: [https://github.com/apache/commons-codec](https://github.com/apache/commons-codec)

*Component*: EU Digital COVID Certificate App Core - Android
*License Text URL*: [URL: http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)
*Source Code*: [https://github.com/eu-digital-green-certificates/dgca-app-core-android](https://github.com/eu-digital-green-certificates/dgca-app-core-android)


___MIT___

*Component*: JWTDecode.Android
*License Text URL*: [https://github.com/auth0/JWTDecode.Android/blob/master/LICENSE](https://github.com/auth0/JWTDecode.Android/blob/master/LICENSE)
*Source Code*: [https://github.com/auth0/JWTDecode.Android](https://github.com/auth0/JWTDecode.Android)


*Component*: The Bouncy Castle Crypto Package For Java
*License Text URL*: [https://github.com/bcgit/bc-java/blob/master/LICENSE.html](https://github.com/bcgit/bc-java/blob/master/LICENSE.html)
*Source Code*: [https://github.com/bcgit/bc-java](https://github.com/bcgit/bc-java)


*Component*: World Country Data
*License Text URL*: [https://github.com/blongho/worldCountryData/blob/master/LICENSE.txt](https://github.com/blongho/worldCountryData/blob/master/LICENSE.txt)
*Source Code*: [https://github.com/blongho/worldCountryData](https://github.com/blongho/worldCountryData)


