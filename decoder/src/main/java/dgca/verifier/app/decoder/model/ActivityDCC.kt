package dgca.verifier.app.decoder.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.time.ZonedDateTime

/***
 * Data Class used the for Activity DCC which is specific for France
 *
 * @param expirationTime is extracted manually from the CWT Headers
 * @param issuedAt is extracted manually from the CWT Headers
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class ActivityDCC(
    @JsonProperty("expirationTime")
    var expirationTime: ZonedDateTime?,

    @JsonProperty("issuedAt")
    var issuedAt: ZonedDateTime?
    ) : Serializable