package dgca.verifier.app.decoder.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

data class Exemption(

    @JsonProperty("tg")
    val disease: String,

    @JsonProperty("es")
    val exemptionStatus: String,

    @JsonProperty("df")
    val dateValidFrom: String,

    @JsonProperty("du")
    val dateValidUntil: String,

    @JsonProperty("co")
    val countryOfVaccination: String,

    @JsonProperty("is")
    val certificateIssuer: String,

    @JsonProperty("ci")
    val certificateIdentifier: String) : Serializable {

}